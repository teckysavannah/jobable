import { Knex } from "knex";
import { Chance } from "chance";
import { hashPassword } from "../utils/hash";
import dotenv from "dotenv";
dotenv.config();
const totalImageFound = 37;
const chance = new Chance();
const domainName = process.env.DOMAIN_NAME;
export async function seed(knex: Knex): Promise<void> {
  //seed numbers of employer forms
  const employerFormNumbers = 60;
  // Deletes ALL existing entries
  await knex("admin_recommendations").del();
  await knex("other_tags").del();
  await knex("jobex_requirement").del();
  await knex("benefits").del();
  await knex("history").del();
  await knex("bookmarks").del();
  await knex("job_poster").del();
  await knex("user_profile").del();
  await knex("applied_job").del();
  await knex("jobs").del();
  await knex("employer_forms").del();
  await knex("users").del();
  await knex("web_users").del();

  // Inserts seed entries
  const user_list = [
    {
      user_name: "wendy@tecky.io",
      password: await hashPassword("12345678"),
    },
    {
      user_name: "wilson@tecky.io",
      password: await hashPassword("12345678"),
    },
    {
      user_name: "savannah@tecky.io",
      password: await hashPassword("12345678"),
    },
    {
      user_name: "jason@tecky.io",
      password: await hashPassword("12345678"),
    },
    {
      user_name: "alex@tecky.io",
      password: await hashPassword("12345678"),
    },
    {
      user_name: "gordon@tecky.io",
      password: await hashPassword("12345678"),
    },
    {
      user_name: "leo@tecky.io",
      password: await hashPassword("12345678"),
    },
    {
      user_name: "james@tecky.io",
      password: await hashPassword("12345678"),
    },
    {
      user_name: "beeno@tecky.io",
      password: await hashPassword("12345678"),
    },
    {
      user_name: "reemo@tecky.io",
      password: await hashPassword("12345678"),
    },
  ];
  const web_users = [
    {
      user_name: "superwilson@tecky.io",
      password: await hashPassword("12345678"),
      role: "admin",
    },
    {
      user_name: "lala@tecky.io",
      password: await hashPassword("12345678"),
    },
    {
      user_name: "superwendy@tecky.io",
      password: await hashPassword("12345678"),
      role: "admin",
    },
    {
      user_name: "superwayne@tecky.io",
      password: await hashPassword("12345678"),
      role: "admin",
    },
    {
      user_name: "superellie@tecky.io",
      password: await hashPassword("12345678"),
    },
    {
      user_name: "superlaalaa@tecky.io",
      password: await hashPassword("12345678"),
    },
  ];
  await knex.insert(user_list).into("users").returning("id");
  const userIdList = (await knex.select(knex.raw(`ARRAY_AGG (id) as id`)).from("users").first()).id;
  console.log({ userIdList });

  await knex.insert(web_users).into("web_users");
  const employerIdList: any = (
    await knex.select(knex.raw(`ARRAY_AGG (id) as id`)).from("web_users").where("role", "employer").first()
  ).id;
  console.log({ employerIdList });
  const adminIdList: any = (
    await knex.select(knex.raw(`ARRAY_AGG (id) as id`)).from("web_users").where("role", "admin").first()
  ).id;
  console.log({ adminIdList });

  let user_profile = [];
  for (let i = 0; i < userIdList.length; i++) {
    user_profile[i] = {
      first_name: chance.first(),
      last_name: chance.last(),
      birthday: chance.birthday({ string: true }),
      gender: chance.gender(),
      email: chance.email({ domain: "gmail.com" }),
      mobile: parseInt(chance.phone({ country: "uk", mobile: true, formatted: false })),
      address: chance.address(),
      occupation: GenOccupation(),
      image: GenImage(),
      descriptions: chance.paragraph(),
      // work_exp:"",
      education_level: "Yes",
      school_name: GenSchoolName(),
      program_name: GenProgramName(),
      grad_year: chance.year({ min: 1970, max: 2020 }),
      contact_time: "Whole Day",
      cv_filename: GenCvFileName(),
      coverletter_filename: GenCoverFilename(),
      users_id: userIdList[i],
    };
  }
  await knex.insert(user_profile).into("user_profile");

  const addressList = [
    { lng: "114.10776071317305", lat: "22.372692152959633", address: "One Midtown, 11 Hoi Shing Rd, Tsuen Wan" },
    { lng: "114.17172155162832", lat: "22.274746596614392", address: "Hopewell Centre, 183 Queen's Rd E, Wan Chai" },
    {
      lng: "114.13007948349684",
      lat: "22.26283347949813",
      address: "Cyberport 1, Unit 417-421, 100 Cyberport Rd, Pok Fu Lam",
    },
    { lng: "114.132230615032", lat: "22.25899584439116", address: "Cyberport 3, 9/F, Core F, Telegraph Bay" },
    { lng: "114.21297319506428", lat: "22.423991334966512", address: "Science and Technology W Ave, Science Park" },
    {
      lng: "114.21079524143119",
      lat: "22.42550871793431",
      address: "Biotech Centre 1, Science Park, Science and Technology W Ave, Sha Tin",
    },
    {
      lng: "114.26551914116878",
      lat: "22.33651887127191",
      address: "The Hong Kong University of Science and Technology, Clear Water Bay",
    },
    {
      lng: "114.20836912601763",
      lat: "22.320421270927277",
      address: "Enterprise Square V Tower 2, 38 Wang Chiu Rd, Kowloon Bay",
    },
    { lng: "114.22360407317379", lat: "22.308213021873634", address: "223 Wai Yip St, Kwun Tong" },
    { lng: "114.21599732924453", lat: "22.285024335804714", address: "2 Kornhill Rd, Quarry Bay" },
    { lng: "114.21217786362374", lat: "22.287704731617936", address: "Taikoo Place, 979 King's Rd, Quarry Bay" },
    { lng: "114.20754300647468", lat: "22.291695447929854", address: "Quarry Bay, King's Rd, No. 728, K11 ATELIER" },
  ];
  let employerForms = [];
  let jobsList = [];
  // const employerFormAddressList: any = (
  //   await knex.select(knex.raw(`ARRAY_AGG (address) as address`)).from("employer_forms").first()
  // ).address;
  // console.log({ employerFormAddressList });
  // console.log({ employerFormIdList });
  // function getLngLat(inputAddress: string, type: string) {
  //   if (!(type === "Lng" || type === "Lat")) {
  //     return;
  //   }
  //   for (let addressItem of addressList) {
  //     if (addressItem.address === inputAddress) {
  //       return type === "Lng" ? addressItem.lng : addressItem.lat;
  //     }
  //   }
  //   return;
  // }
  for (let i = 0; i < employerFormNumbers; i++) {
    const coordinateIndex = Math.floor(Math.random() * addressList.length);
    const company_name = chance.company()
    const job_title = GenJobtTitle()
    const address = addressList[coordinateIndex].address
    const lat = addressList[coordinateIndex].lat
    const lng = addressList[coordinateIndex].lng
    const salary = `${Math.floor(Math.random() * 20) + 20}k`
    
    const due_date = chance.date({ string: true, american: true, year: 2022 })
    const website = chance.url()
    const phone = parseInt(chance.phone({ country: "uk", mobile: true, formatted: false }))
    const email = chance.email({ domain: "gmail.com" })
    const form_status = i<employerFormNumbers/2?"Done":"Waiting"

    employerForms.push({
      company_name,
      job_title,
      address,
      salary,
      work_hours: "8",
      due_date,
      website,
      phone,
      email,
      form_status,
      contact_method: "Email",
      employer_users_id: employerIdList[Math.floor(Math.random() * employerIdList.length)],
      admin_users_id: adminIdList[Math.floor(Math.random() * adminIdList.length)],
    });
    const employerFormId = await knex.insert(employerForms[i]).into("employer_forms").returning('id');
    console.log('employerFormId',employerFormId)


    if (form_status == "Done" ){
      
      const { startDate, endDate } = GenDate();
      jobsList.push({
        title: job_title,
        comapany_name: company_name,
        phone_number: phone,
        address: address,
        salary_project: salary,
        descriptions: GenDescriptions(),
        job_duties: GenJobDuties(),
        start_time: startDate,
        end_time: endDate,
        working_hours: "8",
        annual_leave: Math.floor(Math.random() * 17) + 7,
        days_per_week: "5",
        working_period: chance.hour(),
        age_range: Math.ceil(Math.random() * 16),
        gender_range: Math.floor(Math.random() * 5),
        teamwork_range: Math.floor(Math.random() * 5),
        wfh_range: Math.floor(Math.random() * 3),
        client_range: Math.floor(Math.random() * 3),
        experience_range: Math.floor(Math.random() * 3),
        exp_requirement: GenExpRequirement(),
        benefits: GenBenefits(),
        apply_method: "Email",
        deadline: due_date,
        url: website,
        lng,
        lat,
        employer_forms_id:employerFormId[0],
      });

    }
  }
  
  const employerFormIdList: any = (await knex.select(knex.raw(`ARRAY_AGG (id) as id`)).from("employer_forms").first())
    .id;
  

  
  

  await knex.insert(jobsList).into("jobs");
  const jobIdList: any = (await knex.select(knex.raw(`ARRAY_AGG (id) as id`)).from("jobs").first()).id;
  console.log({ jobIdList });

  const bookmarksAmount = Math.floor(
    (Math.random() * jobIdList.length * userIdList.length) / 2 + (jobIdList.length * userIdList.length) / 2
  );
  const bookmarksMap = new Map();
  const bookmarksList = [];
  for (let i = 0; i < bookmarksAmount; i++) {
    const users_id = userIdList[Math.floor(Math.random() * userIdList.length)];
    const jobs_id = jobIdList[Math.floor(Math.random() * jobIdList.length)];
    const idPair = users_id.toString() + jobs_id.toString() + users_id.toString();
    if (!bookmarksMap.has(idPair)) {
      bookmarksMap.set(idPair, { users_id, jobs_id });
      bookmarksList.push({
        users_id,
        jobs_id,
      });
    }
  }
  await knex.insert(bookmarksList).into("bookmarks");

  const historyAmount = Math.floor(
    (Math.random() * jobIdList.length * userIdList.length) / 2 + (jobIdList.length * userIdList.length) / 2
  );

  const historyMap = new Map();
  const historyList = [];
  for (let i = 0; i < historyAmount; i++) {
    const users_id = userIdList[Math.floor(Math.random() * userIdList.length)];
    const jobs_id = jobIdList[Math.floor(Math.random() * jobIdList.length)];
    const idPair = users_id.toString() + jobs_id.toString() + users_id.toString();
    if (!historyMap.has(idPair)) {
      historyMap.set(idPair, { users_id, jobs_id });
      historyList.push({
        users_id,
        jobs_id,
      });
    }
  }
  await knex.insert(historyList).into("history");

  // const userAppliedJobsAmount = Math.floor(
  //   (Math.random() * jobIdList.length * userIdList.length) / 2 + (jobIdList.length * userIdList.length) / 2
  // );
  const userAppliedJobsAmount =  userIdList.length*3
  const appliedJobMap = new Map();
  const appliedJobList = [];
  for (let i = 0; i < userAppliedJobsAmount; i++) {
    const users_id = userIdList[Math.floor(Math.random() * userIdList.length)];
    const jobs_id = jobIdList[Math.floor(Math.random() * jobIdList.length)];
    const idPair = users_id.toString() + jobs_id.toString() + users_id.toString();
    if (!appliedJobMap.has(idPair)) {
      appliedJobMap.set(idPair, { users_id, jobs_id });
      appliedJobList.push({
        users_id,
        jobs_id,
        application_status: Math.random() > 0.95 ? "Rejected" : Math.random() > 0.25 ? "Received" : "Accepted",
      });
    }
  }
  await knex.insert(appliedJobList).into("applied_job");

  const jobPosterArr = [];
  for (let i = 0; i < jobIdList.length; i++) {
    const image_filename = GenImageURL(2);
    // console.log({image_filename})
    // const image_filename = [`${domainName}/images/poster01.jpg`, `${domainName}/images/poster02.jpg`];

    for (let image of image_filename) {
      jobPosterArr.push({ jobs_id: jobIdList[i], image_filename: image });
    }
  }
  await knex.insert(jobPosterArr).into("job_poster");

  let adminRecommendations = [];
  for (let i = 0; i < employerFormNumbers; i++) {
    adminRecommendations.push({
      admin_users_id: adminIdList[Math.floor(Math.random() * adminIdList.length)],
      employer_forms_id: employerFormIdList[i],
      users_id: userIdList[Math.floor(Math.random() * userIdList.length)],
    });
  }
  await knex.insert(adminRecommendations).into("admin_recommendations");
}
function GenDescriptions() {
  const descriptionsList: string[] = [
    "We are committed to carrying out pioneering research, attracting the brightest minds, nurturing the most sparkling ideas, bringing the best business solutions, and providing start up and spin off opportunities.",
    "We have a hardware team and a software team, with over 40 staff in the software team. Mainly on these projects, you are going to work on Robots and AI. You are going to work on the latest languages for Full Stack Development with the BA. Your responsibilities are Research, Designing, and Implementing software programs, Testing and Evaluating new programs. You will be working independently and with teams, with the freedom to shape your code, your R&D, and your growth trajectory.",
    "You will get plenty of chance to learn and build up your career in MRI field. You will also have the opportunities to travel and work with our overseas team and domestic (Hong Kong) expert clinical doctors in an innovative medical device corporation. The Company is now expanding and we invite talents at all levels to join our innovate team.  To find out more about our company, please visit to “www.time-medical.com”.",
    "An exciting opportunity to join a Marketing Innovation startup and develop a career as part of a highly specialized team. Our multi-national clients span multi-industrial and international brands. This role will be expected to reach the latest technologies and apply them in the market. As part of our expansion plan, we are looking for a high-calibre candidate to join our team to drive the development of our key account (s).",
    "We are providing a broad range of services and solutions in strategy, consulting, digital, technology and operations. We are one of the leaders in E-Commerce strategy and consulting company in Asia.",
    
  ];
  const index = Math.floor(Math.random() * descriptionsList.length);
  const jobDescriptions = descriptionsList[index];
  return jobDescriptions;
}
function GenBenefits() {
  const benefitsList: string[] = [
    "Double Pay.Discretionary Performance Bonus. Life Insurance. Medical insurance. Free Snacks and Drinks.",
    "Five days week. Friendly team environment",
    "Performance bonus. Medical Insurance. Overtime Pay",
    "5-day workweek, 14-21 days AL, education subsidies",
    "Competitive remuneration package. Additional leave (Family Care Leave, Exam Leave, Qualification Leave, etc.)",
    "23 Days Paid Leave. Flexible working hours. Free snack bar and drinks in office. Entitle medical insurance, Health check-up/ Dental allowance.",
    "Five-day work, great work life balance. Systematic onboard training and code review, for better career growth. Weekly technology sharing event.",
  ];
  const index = Math.floor(Math.random() * benefitsList.length);
  const jobBenefit = benefitsList[index];
  return jobBenefit;
}
function GenJobDuties() {
  const jobDutiesList: string[] = [
    "Building and maintaining web applications Assessing the efficiency and speed of current applicationsManaging hosting environments QA testing",
    "Software design and development of new products. Creating automated testing framework and test scripts. Maintaining and developing updates to existing software products.",
    "Design, implementation, deployment and testing of android applications (Including Mobile and other Android devices)",
    "Responsible for Software development and maintenance of our system. Provide technical advice to team members.",
    "Design, develop and test our next generation medical imaging device software and big data AI clinical platform following regulatory standards.",
    "Maintain thorough design and development documentations. Coordinate with other teams within and external to the product development group.",
    "Maintain the company website and development its new features. Provide various technical and programming supports to the project team.",
    "Assist in software development according to project requirements, as well as program testing and deployment of systems.",
  ];
  const index = Math.floor(Math.random() * jobDutiesList.length);
  const jobDuties = jobDutiesList[index];
  return jobDuties;
}
function GenExpRequirement() {
  const requirementList: string[] = [
    "2 - 5 years of experience with Java, JavaScript, Spring Framework, Angular/AngularJS and MS SQL",
    "2+ years experiences in Flutter/React Native application development",
    "1+ year relevant working experience in Web or Mobile App development",
    "At least 5 years experience in Java and Node.js programming language",
    "3+ yrs exp in building distributed systems in Java",
    "Higher Diploma or above in IT/ Computer Science or related discipline",
    "Experience in C# andPython development",
    "3 - 7 years of experience in application development, fresh graduate or less related job experience will be considered as Software Engineer.",
    "5+ years hands-on experience in application design and development on large scale/ mission critical systems using different technologies",
    "2+ year solid experience of desktop or web application development",
    "At least 3 years experiences in backend development",
    "Solid knowledge and experience in PHP / MySQL / Javascript",
    "2+ years exp in web application development",
    "Experience in cloud computing platforms such as Amazon Web Services, Microsoft Azure and Google Firebase will be an advantage",
    "Proficiency in relational databases - PostgreSQL, MySQL and Microsoft SQL server",
    "Good knowledge in NodeJS/ HTML5, CSS3, JavaScript/ ReactJS",
    "Experience in CI/CD pipelines and microservice architecture is STRONG PLUS",
    "Proficient understanding of web technology, including HTML, CSS, JavaScript frameworks, ES6 and toolchain.",
    "Fluent in JavaScript, React.js or other related tech stacks",
    "Knowledge of front-end technologies including CSS3, JavaScript, HTML5 and jQuery",
    "Knowledge of back-end integration such as database, JSON API integration",
    "Minimum 2 years of website FRONTEND development",
    "Solid experience on ReactJS or VueJS",
    "Degree holder of Computer Science or related disciplines",
    "Hands-on experience in SDLC and web development, including but not limited to these skill sets: Node.js, JS, HTML5, CSS3",
    "Experience in Azure Cloud Services, Azure DevOps",
    "At least 2 years of development experience Python web framework (Django)",
  ];
  const index = Math.floor(Math.random() * requirementList.length);
  const expRequirement = requirementList[index];
  return expRequirement;
}
function GenSchoolName() {
  const schoolList: string[] = [
    "The Chinese University of Hong Kong",
    "City University of Hong Kong",
    "The Education University of Hong Kong",
    "Hong Kong Baptist University",
    "The Hong Kong Polytechnic University",
    "The Hong Kong University of Science and Technology",
    "Lingnan University",
    "The University of Hong Kong",
    "The Open University of Hong Kong",
    "Hong Kong Shue Yan University",
    "Hang Seng University of Hong Kong",
    "Hong Kong Institute of Vocational Education",
    "Technological and Higher Education Institute of Hong Kong",
    "Vocational Training Council",
    "King's College",
    "Lok Sin Tong Leung Kau Kui College",
    "Sacred Heart Canossian College",
    "St. Clare's Girls' School",
    "St. Joseph's College",
    "St. Louis School",
    "St. Paul's Co-educational College",
    "St. Paul's College",
    "St. Stephen's Church College",
    "St. Stephen's Girls' College",
    "Ying Wa Girls' School",
    "Hon Wah College",
    "Hong Kong Chinese Women's Club College",
    "Hongkong Japanese School (Junior Secondary Section)",
    "Islamic Kasim Tuet Memorial College",
    "Kellett School",
    "Kiangsu-Chekiang College, North Point",
    "Lingnan Hang Yee Memorial Secondary School",
    "Lingnan Secondary School",
    "Man Kiu College",
    "Methodist Church Hong Kong Wesley College",
    "Munsang College (Hong Kong Island)",
    "Po Leung Kuk Yu Lee Mo Fan Memorial School",
    "Precious Blood Secondary School",
    "Pui Kiu Middle School",
    "Rotary Club of Hong Kong Island West Hong Chi Morninghope School",
    "Salesian English School (Secondary)",
    "Shau Kei Wan East Government Secondary School",
    "Shau Kei Wan Government Secondary School",
    "Sheng Kung Hui Li Fook Hing Secondary School",
    "St. Joan of Arc Secondary School",
    "St. Mark's School",
  ];
  const index = Math.floor(Math.random() * schoolList.length);
  const schoolName = schoolList[index];
  return schoolName;
}
function GenProgramName() {
  const programList: string[] = [
    "BBA Management",
    "BBA Business Economics",
    "BBA Business Analysis",
    "BA Creative Media",
    "Bachelor of Laws",
    "BSc Data Science",
    "BSc Data and Systems Engineering",
    "BSocSc Asian and International Studies",
    "BSocSc Criminology and Sociology",
    "BSc Computer Science",
    "BSc Computing Mathematics",
    "BSc Physics",
    "BEng Materials Science and Engineering",
    "Bachelor of Laws and Bachelor of Science in Computing Mathematics",
    "Bachelor of Veterinary Medicine",
    "Bachelor of Arts (Hons) in Religion, Philosophy and Ethics",
    "Bachelor of Business Administration (Hons) - Accounting Concentration",
    "Bachelor of Communication (Hons) in Film (Film and Television Concentration)",
    "Bachelor of Chinese Medicine and Bachelor of Science (Hons) in Biomedical Science",
    "Bachelor of Arts (Hons) in Visual Arts",
    "BSc (Hons) Scheme in Biotechnology, Food Safety and Chemical Technology",
    "BBA (Hons) Scheme in Aviation, Maritime and Supply Chain Management",
    "BEng (Hons) Scheme in Building Sciences and Engineering",
    "BSc (Hons) Scheme in Spatial Data Science and Smart Cities",
    "BEng (Hons) Scheme in Aviation Engineering",
    "BSc (Hons) Scheme in Optometry",
    "BSc (Hons) Scheme in Hotel and Tourism Management",
    "BA (Hons) Scheme in Fashion and Textiles",
    "BSc (Hons) Scheme in Logistics and Enterprise Engineering",
    "BSc (Hons) Scheme in Computing and AI",
    "Cultural Management",
    "Japanese Studies",
    "Philosophy",
  ];
  const index = Math.floor(Math.random() * programList.length);
  const programName = programList[index];
  return programName;
}
function GenOccupation() {
  const occupationList: string[] = [
    "Computer Scientist",
    "IT Professional",
    "UX Designer",
    "UI Developer",
    "SQL Developer",
    "Web Designer",
    "Web Developer",
    "Help Desk Worker",
    "Desktop Support",
    "Software Engineer",
    "Data Entry",
    "DevOps Engineer",
    "Computer Programmer",
    "Network Administrator",
    "Information Security Analyst",

    "Cloud Architect",
    "IT Manager",
    "Technical Specialist",
    "Application Developer",
    "Chief Technology Officer",
    "Chief Information Officer",
    "Sales Associate",
    "Sales Representative",
    "Sales Manager",
    "Retail Worker",
    "Store Manager",
    "Sales Representative",
    "Sales Manager",
    "Real Estate Broker",
    "Sales Associate",
    "Cashier",
    "Store Manager",
    "Account Executive",
    "Account Manager",
    "Area Sales Manager",
    "Direct Salesperson",
    "Director of Inside Sales",
    "Outside Sales Manager",
    "Sales Analyst",
    "Market Development Manager",
    "B2B Sales Specialist",
    "Sales Engineer",
    "Merchandising Associate",
  ];
  const index = Math.floor(Math.random() * occupationList.length);
  const occupation = occupationList[index];
  return occupation;
}
function GenImage() {
  const imageList: string[] = ["image01.jpg", "image02.jpg", "image03.jpg", "image04.jpg", "image05.jpg"];
  const index = Math.floor(Math.random() * imageList.length);
  const imageURL = imageList[index];
  return imageURL;
}
function GenCvFileName() {
  const cvFilenameList: string[] = ["cv01.pdf", "cv02.pdf", "cv03.pdf", "cv04.pdf", "cv05.pdf"];
  const index = Math.floor(Math.random() * cvFilenameList.length);
  const cvFilePath = cvFilenameList[index];
  return cvFilePath;
}
function GenCoverFilename() {
  const coverFilenameList: string[] = ["cover01.pdf", "cover02.pdf", "cover03.pdf", "cover04.pdf", "cover05.pdf"];
  const index = Math.floor(Math.random() * coverFilenameList.length);
  const coverLetterFilePath = coverFilenameList[index];
  return coverLetterFilePath;
}
function GenJobtTitle() {
  const jobTitleList: string[] = [
    "Computer Scientist",
    "IT Professional",
    "UX Designer",
    "UI Developer",
    "SQL Developer",
    "Web Designer",
    "Web Developer",
    "Help Desk Worker",
    "Desktop Support",
    "Software Engineer",
    "Data Entry",
    "DevOps Engineer",
    "Computer Programmer",
    "Network Administrator",
    "Information Security Analyst",
    "Cloud Architect",
    "IT Manager",
    "Technical Specialist",
    "Application Developer",
    "Chief Technology Officer",
  ];
  const index = Math.floor(Math.random() * jobTitleList.length);
  const jobTitle = jobTitleList[index];
  return jobTitle;
}
function GenDate() {
  let startDate: any;
  let endDate: any;
  startDate = chance.date({ string: true, american: false, year: 2022 });
  do {
    endDate = chance.date({ string: true, american: false, year: 2022 });
  } while (Date.parse(endDate) < Date.parse(startDate));
  return { startDate, endDate };
}
function GenImageURL(imageAmount: number): string[] {
  // const imageFilenamePath: string[] = [
  //   `${domainName}/images/poster01.jpg`,
  //   `${domainName}/images/poster02.jpg`,
  // ];
  let image_filename: string[] = [];
  for (let i = 0; i < imageAmount; i++) {
    // const index = Math.floor(Math.random() * imageFilenamePath.length);
    image_filename.push(`${domainName}/poster${Math.ceil(Math.random() * totalImageFound)}.jpg`);
  }
  return image_filename;
}
