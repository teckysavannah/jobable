import express from "express";
import { JobController } from "../controllers/jobController";
import { knex } from "../knexConfig";
import { JobService } from "../services/jobService";
import { isEmployer, isLoggedIn } from "../utils/guards";

const jobsService = new JobService(knex);
export const jobController = new JobController(jobsService);
export const jobRoutes = express.Router();

jobRoutes.get("/allJobs", jobController.getAllJobDetails);

//get all employerForms
jobRoutes.get("/allEmployerForm", isLoggedIn, isEmployer, jobController.selectAllEmployerForm);

//get single employerForm by formId
jobRoutes.post("/singleEmployerForm", isLoggedIn, isEmployer, jobController.selectSingleEmployerForm);

jobRoutes.post("/getAppliedJob", isLoggedIn, jobController.getAppliedJobStatus);

jobRoutes.post("/appliedJob", isLoggedIn, jobController.insertAppliedJobStatus);

jobRoutes.delete("/appliedJob", isLoggedIn, jobController.deleteAppliedJobStatus);
