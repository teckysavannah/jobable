import express from "express";
import { upload } from "../app";
import { AdminController } from "../controllers/adminController";
import { knex } from "../knexConfig";
import { AdminService } from "../services/adminService";
import { isAdmin, isEmployer, isLoggedIn } from "../utils/guards";

const adminService = new AdminService(knex);
export const adminController = new AdminController(adminService);
export const adminRoutes = express.Router();




adminRoutes.post("/adminPost", isLoggedIn, isAdmin, upload.single('image_filename'),adminController.insertAdminPost);

adminRoutes.put("/adminPost", isLoggedIn, isAdmin, upload.single('image_filename'),adminController.editAdminJobPost);

adminRoutes.put("/employerFormStatus", isLoggedIn, isAdmin, adminController.updateEmployerFormStatus);

//to be confirmed, (is employer or admin to approve applicants status)
adminRoutes.put("/updateApplicationStatus", isLoggedIn, isEmployer,adminController.updateApplicationStatus);



adminRoutes.put("/employerForm", isLoggedIn, isAdmin, adminController.editEmployerForm);

// not in use
// adminRoutes.delete("/employerForm", isLoggedIn, isAdmin, adminController.deleteEmployerForm);