import express from "express";
import { WebUserController } from "../controllers/webUserController";
import { knex } from "../knexConfig";
import { WebUserService } from "../services/webUserService";
export const webUserService = new WebUserService(knex);
export const webUserController = new WebUserController(webUserService);
export const webUserRoutes = express.Router();



webUserRoutes.post("/login", webUserController.webUserLogin);
webUserRoutes.post("/register", webUserController.webUserRegister);






















