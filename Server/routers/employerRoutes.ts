import express from "express";
import { EmployerController } from "../controllers/employerController";
import { knex } from "../knexConfig";
import { EmployerService } from "../services/employerService";
import { isEmployer, isLoggedIn } from "../utils/guards";

const employerService = new EmployerService(knex);
export const employerController = new EmployerController(employerService);
export const employerRoutes = express.Router();


employerRoutes.post("/getApplicants", isLoggedIn, isEmployer, employerController.getApplicants);
employerRoutes.post("/getJobIdByFormId", isLoggedIn, isEmployer, employerController.getJobIdByFormId);
employerRoutes.post("/employerForm", isLoggedIn, isEmployer,employerController.insertEmployerForm);
employerRoutes.get("/allEmployerForm", isLoggedIn, isEmployer, employerController.selectAllEmployerFormByEmployer);
