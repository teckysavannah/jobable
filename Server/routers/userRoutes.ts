import express from "express";
import { userController } from "../routes";

export const userRoutes = express.Router();

import { isLoggedIn, isUser} from "../utils/guards";

userRoutes.post("/register", userController.register);
userRoutes.post("/login", userController.login);
userRoutes.post("/favourite", isLoggedIn, userController.toggleBookmark);
userRoutes.delete("/favourite", isLoggedIn, userController.deleteBookmark);
userRoutes.post("/history", isLoggedIn, userController.addHistory);
userRoutes.get("/history", isLoggedIn, isUser, userController.checkHistory);

userRoutes.get("/userProfile", isLoggedIn, userController.getUserProfile);
/*  POST "/userprofile"
    INSERT into user_profile,
    get user id from req.user from token in bearer
    get "profileDetails" from req.body.profileDetails
    no return
*/
userRoutes.post("/userProfile", isLoggedIn, userController.updateUserProfile);

// userRoutes.post("/adminLogin", userController.adminLogin);
// userRoutes.post("/employerRegister", userController.employerRegister);
// userRoutes.post("/employerLogin", userController.employerLogin);
