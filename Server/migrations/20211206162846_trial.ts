import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("users", (table) => {
    table.increments();
    table.string("user_name").notNullable().unique();
    table.string("password").notNullable();
    table.timestamps(false, true);
  });
  await knex.schema.createTable("user_profile", (table) => {
    table.increments();
    table.string("first_name");
    table.string("last_name");
    table.date("birthday");
    table.enum("gender", ["Male", "Female", "Others"]);
    table.string("email");
    table.string("mobile");
    table.text("address");
    table.string("occupation");
    table.string("image");
    table.text("descriptions");
    table.string("work_exp");
    table.boolean("education_level").notNullable();
    table.string("school_name");
    table.string("program_name");
    table.string("grad_year");
    table.enum("contact_time", ["Whole Day", "Morning", "Afternoon"]).defaultTo("Whole Day");
    table.string("cv_filename");
    table.string("coverletter_filename");
    table.integer("users_id");
    table.foreign("users_id").references("users.id");
    table.timestamps(false, true);
  });
  await knex.schema.createTable("jobs", (table) => {
    table.increments();
    table.string("title");
    table.string("comapany_name");
    table.string("phone_number");
    table.text("address");
    table.text("salary_project");
    table.text("descriptions");
    table.text("job_duties");
    table.string("start_time");
    table.string("end_time");
    table.string("working_hours");
    table.string("annual_leave");
    table.string("days_per_week");
    table.string("working_period");
    table.string("age_range");
    table.string("gender_range");
    table.string("teamwork_range");
    table.string("wfh_range");
    table.string("client_range");
    table.string("experience_range");
    table.text("exp_requirement");
    table.text("benefits");
    table.text("apply_method");
    table.date("deadline");
    table.date("url");
    table.timestamps(false, true);
  });
  await knex.schema.createTable("applied_job", (table) => {
    table.increments();
    table.integer("users_id");
    table.foreign("users_id").references("users.id");
    table.integer("jobs_id");
    table.foreign("jobs_id").references("jobs.id");
    table.enum("application_status", ["Received", "Pending", "Accepted", "Rejected"]).defaultTo("Received");
    table.timestamps(false, true);
  });
  await knex.schema.createTable("bookmarks", (table) => {
    table.increments();
    table.integer("users_id");
    table.foreign("users_id").references("users.id");
    table.integer("jobs_id");
    table.foreign("jobs_id").references("jobs.id");
    table.timestamps(false, true);
  });
  await knex.schema.createTable("history", (table) => {
    table.increments();
    table.integer("users_id");
    table.foreign("users_id").references("users.id");
    table.integer("jobs_id");
    table.foreign("jobs_id").references("jobs.id");
    table.timestamps(false, true);
  });
  await knex.schema.createTable("benefits", (table) => {
    table.increments();
    table.string("tag");
    table.integer("jobs_id");
    table.foreign("jobs_id").references("jobs.id");
    table.timestamps(false, true);
  });
  await knex.schema.createTable("job_poster", (table) => {
    table.increments();
    table.text("image_filename");
    table.integer("jobs_id");
    table.foreign("jobs_id").references("jobs.id");
    table.timestamps(false, true);
  });
  await knex.schema.createTable("jobex_requirement", (table) => {
    table.increments();
    table.string("tag");
    table.integer("jobs_id");
    table.foreign("jobs_id").references("jobs.id");
    table.timestamps(false, true);
  });
  await knex.schema.createTable("other_tags", (table) => {
    table.increments();
    table.string("tag");
    table.integer("jobs_id");
    table.foreign("jobs_id").references("jobs.id");
    table.timestamps(false, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("other_tags");
  await knex.schema.dropTable("jobex_requirement");
  await knex.schema.dropTable("job_poster");
  await knex.schema.dropTable("benefits");
  await knex.schema.dropTable("history");
  await knex.schema.dropTable("bookmarks");
  await knex.schema.dropTable("applied_job");
  await knex.schema.dropTable("jobs");
  await knex.schema.dropTable("user_profile");
  await knex.schema.dropTable("users");
}
