import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("web_users", (table) => {
    table.increments();
    table.string("user_name").notNullable().unique();
    table.string("password").notNullable();
    table.enum("role", ["employer", "admin"]).defaultTo("employer");
    table.timestamps(false, true);
  });
  await knex.schema.createTable("employer_forms", (table) => {
    table.increments();
    table.string("company_name");
    table.string("job_title");
    table.string("address");
    table.string("salary");
    table.string("work_hours");
    table.string("due_date");
    table.string("website");
    table.string("phone");
    table.string("email");
    table.enum("form_status", ["Waiting", "Processing","Done","Rejected"]).defaultTo("Waiting");
    table.enum("contact_method", ["Phone", "Email"]).defaultTo("Email");
    table.integer("employer_users_id");
    table.foreign("employer_users_id").references("web_users.id");
    table.integer("admin_users_id");
    table.foreign("admin_users_id").references("web_users.id");
    table.timestamps(false, true);
  });
  await knex.schema.createTable("admin_recommendations", (table) => {
    table.increments();
    table.integer("admin_users_id");
    table.foreign("admin_users_id").references("web_users.id");
    table.integer("employer_forms_id");
    table.foreign("employer_forms_id").references("employer_forms.id");
    table.integer("users_id");
    table.foreign("users_id").references("users.id");
    table.timestamps(false, true);
  });
  await knex.schema.table("jobs", (table) => {
    table.enum("jobs_status", ["Waiting", "Processing","Done","Rejected"]).defaultTo("Waiting");
    table.integer("employer_forms_id");
    table.foreign("employer_forms_id").references("employer_forms.id");
  });
}
export async function down(knex: Knex): Promise<void> {
  
    await knex.schema.table("jobs",(table)=>{
        table.dropColumn("jobs_status");
        table.dropColumn("employer_forms_id");
    })
  await knex.schema.dropTable("admin_recommendations");
  await knex.schema.dropTable("employer_forms");
  await knex.schema.dropTable("web_users");
}
