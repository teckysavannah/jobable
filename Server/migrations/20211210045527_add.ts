import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.table("users", (table) => {
    table.boolean("is_active");
  });
  await knex.schema.alterTable("jobs", (table) => {
    table.string("url").alter();
  });
  await knex.schema.table("jobs", (table) => {
    table.string("lng");
    table.string("lat");
  });
  await knex.schema.table("user_profile", (table) => {
    table.string("latest_job_title");
  });
  await knex.schema.alterTable("user_profile", (table) => {
    table.boolean("work_exp").alter();
    table.string("education_level").alter();
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable("user_profile", (table) => {
    table.string("work_exp").alter();
    table.boolean("education_level").alter();
  });
  await knex.schema.table("user_profile", (table) => {
    table.dropColumn("latest_job_title");
  });
  await knex.schema.table("jobs", (table) => {
    table.dropColumn("lng");
    table.dropColumn("lat");
  });
  await knex.schema.alterTable("jobs", (table) => {
    table.string("url").alter();
  });
  await knex.schema.table("users", (table) => {
    table.dropColumn("is_active");
  });
}
