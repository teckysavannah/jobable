import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable("user_profile", (table) => {
        table.string("birthday").alter();
      });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable("user_profile", (table) => {
        table.string("birthday").alter();
      });
}

