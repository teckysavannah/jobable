const R =0 
const G =1 
const B =2 
const A =3
const FULL = 255

const win = window
win.batch = 120000
win.pixel = 2
// console.log(win)
let canvas = document.querySelector('canvas')
let currentBoard = 0

function init(){

    const pixel = win.pixel
    const rect = canvas.getBoundingClientRect()
    canvas.width = Math.floor(rect.width / pixel)
    canvas.height = Math.floor(rect.height/ pixel)
    // canvas.width = Math.floor(rect.width/pixel)
    const W = canvas.width 
    const H = canvas.height
    const ctx = canvas.getContext('2d')
    
    let imageData = ctx.getImageData(0,0,W,H)
    // console.log(imageData)
    for ( let i=0; i<imageData.data.length; i+=4){
        imageData.data[i + A] = FULL
        // imageData.data[i + R] = 0
        // imageData.data[i + G] = 0
        // imageData.data[i + B] = 0
    
    }
    // console.log(imageData.data)
    
    function xy2i(x,y){
        return ( x + y * W) *4
    }
    
    
    
    
    // function forEachPeer(x,y,eachFn){
    //     for (let dy of [-1,0,1]){
    //         let yPeer = y + dy;
    //         if(yPeer< 0 || yPeer>= H){
    //             continue
    //         }
    //         for( let dx of [-1,0,1]){
    //             if(dy==0 && dx == 0){
    //                 continue
    //             }
    //             let xPeer = x+ dx
    //             if( xPeer < 0 || xPeer>= W){
    //                 continue
    //             }
    //             eachFn(xPeer,yPeer)
    //         }
    //     }
    // }
    
    function tick(){
        let x = Math.floor(Math.random() * W)
        let y = Math.floor(Math.random() * H)
        let i = xy2i(x,y)
        let r = imageData.data[i+R]
        let b = imageData.data[i+B]

        function asda(y) {
            return 2*y**2 
        }

        let j = xy2i(asda(y),y)
        imageData.data[ j + B] = FULL
    
        // if ( b <= 0.1 * 255 && r < FULL){
        //     imageData.data[i+R] = Math.floor(r * 1.1) + 1
        //     return
            
        // }
    
        // if ( imageData.data[i + R] == 0){
        //     forEachPeer(x,y, (x,y)=>{
        //         let j = xy2i(x,y)
        //         if( imageData.data[ j + R] == 0){
        //             imageData.data[ i + B] = Math.floor(imageData.data[ i + B] * 0.9) 
                    
        //         }
        //     })
        //     return
        // }
        // forEachPeer(x,y,(x,y)=>{
        //     let j = xy2i(x,y)
        //     if( imageData.data[ j + B] >= 0.8 * FULL){
        //         imageData.data[i + R] = 0
        //         imageData.data[i + B] = FULL
        //         // console.log(imageData.data)
        //         // console.log(i)
        //         // console.log(j)
    
        //         return
        //     }
        // })
        
    }
    
    canvas.addEventListener('click',(e)=>{
        let x = Math.floor(e.clientX / pixel) 
        let y = Math.floor(e.clientY / pixel)
        let  i = xy2i(x,y)
        imageData.data[i+B] = FULL
        imageData.data[i+R] = 0
        // ctx.putImageData(imageData,0,0)
        // console.log("clicked")
        // render()
    })
    
    function render(){
        for (let i=0; i< win.batch; i++){
            tick()
        }
        ctx.putImageData(imageData,0,0)
        // console.log(imageData.data)
        // console.log("put")
        requestAnimationFrame(render)
    }
    requestAnimationFrame(render)
}
init()
// render()

window.onresize = init


