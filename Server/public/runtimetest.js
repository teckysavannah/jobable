const width =window.innerWidth
const height =window.innerHeight

const win = window
win.batch = 30000
win.pixel = 2
const pixel = win.pixel
const batch = win.batch

const canvas = document.querySelector('#test')
canvas.width = width
canvas.height =height
const rows = Math.floor(height/pixel) 
const columns = Math.floor(width/pixel) 
const ctx = canvas.getContext('2d')
let b =0;
let reverse = false
let pixelscount =0


// setup
let currentBoard = []
let nextBoard = []
for (let i = 0; i < rows; i++) {
    currentBoard[i] = []
    nextBoard[i] = []
}

//init
for (let i = 0; i < rows; i++) {
    for (let j = 0; j < columns; j++) {
        currentBoard[i][j] = [0,0,0];   //RGB color
        nextBoard[i][j] = [0,0,0];
        ctx.fillStyle = "(rgb(#000))"
        ctx.fillRect(j*pixel,i*pixel,pixel,pixel)
        
    }
}
let ifmousedown = false
let frMax = 255
let fgMax = 255
let fbMax = 255
let lrMax = 255
let lgMax = 255
let lbMax = 255

const ignitePoint = 0.1
const fuelPoint = 0.8
function thedraw() {
    // console.log("drawing")
    // background("#f22");
    let changedpixels = []
    // frameRate(20)
    if( !ifmousedown){
        // let changedpixels = generate();
        
        
        for (let k =0 ; k<batch; k++){
            let noigniteNeighbor = false
            let x = Math.floor(Math.random()*columns ) 
            let y = Math.floor(Math.random()*rows) 
            changedpixels.push([y,x])
                    
                    

            outerBlock :{
                for (let i of [-1, 0, 1]){
                    innerBlock:{
                        for (let j of [-1, 0, 1]) {
                            if (i == 0 && j == 0) {
                                // the cell itself is not its own neighbor
                                continue;
                            }
                            // The modulo operator is crucial for wrapping on the edge
                            if(currentBoard[y][x][0] == 0 && currentBoard[(y + i + rows) % rows][(x + j + columns) % columns][0]==0 ){
                                
                                nextBoard[y][x][2] = Math.floor(currentBoard[y][x][2] * 0.9) 
                                break outerBlock
                            }else if (currentBoard[(y + i + rows) % rows][(x + j + columns) % columns][2] > 255*fuelPoint ){
                                nextBoard[y][x][0] = 0
                                nextBoard[y][x][2] = lbMax
                                break outerBlock
                            }
                        }
                    }
                }
            }

            // avgBlue = sumOfBlue/8
            // avgRed = sumOfRed/8
            // console.log(sumOfBlue)
            if ((currentBoard[y][x][2] <= ignitePoint* fbMax) && noigniteNeighbor==false){
                noigniteNeighbor = true
                if (currentBoard[y][x][0] < frMax){
                    nextBoard[y][x][0] = Math.floor(currentBoard[y][x][0] * 1.2) + 1
                    // currentBoard[x][y][2] = 0
                }
             
            }          

        }
        // [currentBoard, nextBoard] = [nextBoard, currentBoard];
        currentBoard = nextBoard
    }
    for (let i of changedpixels){
        // console.log(currentBoard[ i[0] ][ i[1] ][0], currentBoard[ i[0] ][ i[1] ][1], currentBoard[ i[0] ][ i[1] ][2])
        ctx.fillStyle = `rgb(${currentBoard[ i[0] ][ i[1] ][0]},${currentBoard[ i[0] ][ i[1] ][1]},${ currentBoard[ i[0] ][ i[1] ][2]})` ;
        ctx.fillRect(i[1] * pixel, i[0] * pixel, pixel, pixel);
        // stroke("#00000000");
    }
    window.requestAnimationFrame(thedraw)
}
window.requestAnimationFrame(thedraw)

window.addEventListener('click',(e)=>{
    let mouseX = e.clientX
    let mouseY = e.clientY
    if ((mouseX > pixel * columns || mouseY > pixel * rows || mouseY<0)) {
        console.log("hihi")
        return;
    }
    const x = Math.floor(mouseX / pixel) 
    const y = Math.floor(mouseY / pixel) 
    nextBoard[y][x][0] = 0
    nextBoard[y][x][2] = 255

    ctx.fillStyle = `rgb(${currentBoard[y][x][0]},${currentBoard[y][x][1]},${currentBoard[y][x][2]})`
    ctx.fillRect(x * pixel, y * pixel, pixel, pixel)
    currentBoard = nextBoard
})

// function mousePressed() {
    
//     if ((mouseX > unitLength * columns || mouseY > unitLength * rows || mouseY<0)) {
//         console.log("hihi")
//         return;
//     }

//     const x = Math.floor(mouseX / unitLength) 
//     const y = Math.floor(mouseY / unitLength) 
//     noLoop()
  
//     nextBoard[x][y][0] = 0
//     nextBoard[x][y][2] = 255

//     fill(currentBoard[x][y][0], currentBoard[x][y][1], currentBoard[x][y][2]);
//     rect(x * unitLength, y * unitLength, unitLength, unitLength);
//     stroke("#00000000");
//     currentBoard = nextBoard
//     ifmousedown = true

// }
// function draw(){
    
    
//     for(let i =0; i<rows; i++){
//         for(let j =0; j<columns; j++){
    
//             // let rect = new Path2D
//             // rect.rect(i*pixel,j*pixel,pixel,pixel)
//             ctx.fillStyle= `rgb(${Math.floor(255- j* (255/(columns)))},${Math.floor(255- i* (255/(rows)))},${b})`
//             ctx.fillRect(j*pixel,i*pixel,pixel,pixel)
//             pixelscount++;
    
//         }
//     }
// }
// draw()
// console.log(pixelscount)