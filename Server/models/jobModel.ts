export interface JobDetails {
  title: string;
  comapany_name: string;
  phone_number: string;
  address: string;
  salary_project: string;
  descriptions: string;
  job_duties: string;
  start_time: string;
  end_time: string;
  working_hours: string;
  annual_leave: string;
  days_per_week: string;
  working_period: string;
  age_range: string;
  gender_range: string;
  teamwork_range: string;
  wfh_range: string;
  client_range: string;
  experience_range: string;
  exp_requirement: string;
  benefits: string;
  apply_method: string;
  deadline: Date;
  url: string;
  lng: string;
  lat: string;
}
