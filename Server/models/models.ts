export interface User {
  id: number;
  username?: string;
  role: string;
}
export interface WebUser {
  id: number;
  username?: string;
  role: string;
}

export enum Permissions {
  admin = "admin",
  employer = "employer",
  user = "user",
}
declare global {
  namespace Express {
    interface Request {
      user:User;
      webUser:WebUser;
    }
  }
}
