import { Knex } from "knex";
import { hashPassword } from "../utils/hash";

export class WebUserService {
  constructor(private knex: Knex) {}

  webUserLogin = async (username: string) => {
    const result = await this.knex.select().from("web_users").where("user_name", username).first();
    return result;
  };
  isRegistered = async (username: string) => {
    const result = await this.knex.select("*").from("web_users").where("user_name", username).first();
    return result;
  };
  register = async (username: string, password: string) => {
    const hashedPassword = await hashPassword(password);
    const id: string = await this.knex
      .insert({ user_name: username, password: hashedPassword })
      .into("web_users")
      .returning("id");
    return id;
  };
  
}
