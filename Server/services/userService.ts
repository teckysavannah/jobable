import { Knex } from "knex";
import { hashPassword } from "../utils/hash";

export class UserService {
  constructor(private knex: Knex) {}
  isRegistered = async (username: string) => {
    const result = await this.knex.select("*").from("users").where("user_name", username).first();
    return result;
  };
  register = async (username: string, password: string) => {
    const hashedPassword = await hashPassword(password);
    const id: string = await this.knex
      .insert({ user_name: username, is_active: true, password: hashedPassword })
      .into("users")
      .returning("id");
    return id;
  };
  getUserByUsername = async (username: string) => {
    const result = await this.knex("users").where("user_name", username).first();
    return result;
  };
  getUserById = async (id: number) => {
    const user = await this.knex.select("*").from("users").where({ id }).first();
    return user;
  };

  insertHistory = async (jobsId: number, usersId: number) => {
    await this.knex.insert({ users_id: usersId, jobs_id: jobsId }).into("history");
    return;
  };
  checkHistory = async (usersId: number) => {
    const result = await this.knex.select("*").from("history").where("users_id", usersId).orderBy("id", "desc");
    return result;
  };
  checkDuplicateHistory = async (jobsId: number, usersId: number) => {
    const result = await this.knex
      .select("*")
      .from("history")
      .where("users_id", usersId)
      .andWhere("jobs_id", jobsId)
      .first();
    return result;
  };
  deleteHistory = async (usersId: number) => {
    await this.knex.raw(
      /*sql*/
      `DELETE FROM history 
      WHERE id = (SELECT id FROM history WHERE users_id = ${usersId} ORDER BY id LIMIT 1)`
    );
    return;
  };
  deleteDuplicateHistory = async (jobsId: number, usersId: number) => {
    await this.knex.raw(
      /*sql*/
      `DELETE FROM history 
      WHERE jobs_id = ${jobsId} AND users_id = ${usersId}`
    );
    return;
  };
  insertBookmark = async (jobsId: number, usersId: number) => {
    await this.knex.insert({ users_id: usersId, jobs_id: jobsId }).into("bookmarks");
    return;
  };
  checkBookmark = async (jobsId: number, usersId: number) => {
    const result = await this.knex
      .select("*")
      .from("bookmarks")
      .where("jobs_id", jobsId)
      .andWhere("users_id", usersId)
      .first();
    return result;
  };
  deleteBookmarked = async (jobsId: number, usersId: number) => {
    await this.knex("bookmarks").where("jobs_id", jobsId).andWhere("users_id", usersId).del();
    return;
  };
  insertUserProfile = async (userProfileDetails: object) => {
    await this.knex.insert(userProfileDetails).into("user_profile");
    return;
  };
  getUserProfile = async (usersId: number) => {
    const result = await this.knex
      .select("*")
      .from("user_profile")
      .where("users_id", usersId)
      .orderBy("created_at", "desc")
      .first();
    return result;
  };
  getWebUser = async (userId: number) => {
    const result = await this.knex.select("*").from("web_users").where("id", userId).first();
    return result;
  };
}
