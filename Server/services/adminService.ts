import { Knex } from "knex";

export class AdminService {
  constructor(private knex: Knex) {}

  insertAdminPost = async (adminFormData: Object,image_filename:string) => {
    const jobsId = await this.knex.insert(adminFormData).into("jobs").returning('id');
    console.log(jobsId)
    await this.knex.insert({ jobs_id: jobsId[0], image_filename  }).into("job_poster");
    return jobsId;
  };

  editAdminJobPost= async (adminJobFormData:Object, formId: number,image_filename:string)=>{
    const jobsId = await this.knex("jobs").where("jobs.employer_forms_id", formId).update(adminJobFormData).returning('id');
    console.log(jobsId)
    if (image_filename != ''){

      await this.knex("job_poster").whereIn("jobs_id",jobsId).update({image_filename})
    }
    return;
  }

  updateEmployerForm = async (status: string, formId: number, adminId: number) => {
    await this.knex("employer_forms")
      .where("employer_forms.id", formId)
      .update({ form_status: status, admin_users_id: adminId });
  };
  editEmployerForm = async (employerFormData: Object, formId: number) => {
    await this.knex("employer_forms").where("employer_forms.id", formId).update(employerFormData);
    return;
  };
  updateApplicationStatus = async (userId: number, jobId: number, status: string) => {
    await this.knex("applied_job")
      .where("jobs_id", jobId)
      .andWhere("users_id", userId)
      .update({ application_status: status });
    return;
  };
  // deleteEmployerForm = async (formId: number) => {
  //   await this.knex("employer_forms").where("id", formId).del();
  //   return;
  // };
}
