import { Knex } from "knex";

export class JobService {
  constructor(private knex: Knex) {}
  allJobDetails = async (userId: number | null) => {
    const jobData = await this.knex
      .select(
        "jobs.id as job_id",
        "employer_forms.id as employer_forms_id",
        "web_users.id as employer_id",
        "employer_forms.*",
        "jobs.*",
        this.knex.raw(
          `(select json_AGG(id) from bookmarks where jobs.id=bookmarks.jobs_id and bookmarks.users_id=${userId}) as bookmarked`
        ),
        this.knex.raw(
          `(SELECT array_AGG(image_filename) from job_poster where job_poster.jobs_id = jobs.id) as imageURL`
        )
      )
      .from("users")
      .leftJoin("bookmarks", "users.id", "bookmarks.users_id")
      .rightJoin("jobs", "jobs.id", "bookmarks.jobs_id")
      .leftJoin("employer_forms", "jobs.employer_forms_id", "employer_forms.id")
      .leftJoin("web_users", "web_users.id", "employer_forms.employer_users_id")
      .groupBy("jobs.id", "jobs.*", "employer_forms.id", "employer_forms.*", "web_users.id")
      .orderBy("jobs.created_at", "DESC");
    return jobData;
  };

  selectAllEmployerForm = async () => {
    const result = await this.knex.select("*").from("employer_forms").orderBy("created_at", "asc");
    return result;
  };
  
  selectSingleEmployerForm = async (formId: number) => {
    const result = await this.knex.select("*").from("employer_forms").where("employer_forms.id", formId);
    return result;
  };

  getAllAppliedJobStatus = async (usersId: number) => {
    const result = await this.knex
      .select(
        "applied_job.users_id as user_id",
        "jobs.id as job_id",
        "jobs.title",
        "jobs.comapany_name as company_name",
        "applied_job.application_status as status"
      )
      .from("applied_job")
      .join("jobs", "jobs.id", "applied_job.jobs_id")
      .where("applied_job.users_id", usersId)
      .orderBy("applied_job.created_at", "desc");
    return result;
  };
  getAppliedJobStatus = async (usersId: number, jobsId: number) => {
    const result = await this.knex
      .select(
        "applied_job.users_id as user_id",
        "jobs.id as job_id",
        "jobs.title",
        "jobs.comapany_name as company_name",
        "applied_job.application_status as status"
      )
      .from("applied_job")
      .join("jobs", "jobs.id", "applied_job.jobs_id")
      .where("applied_job.jobs_id", jobsId)
      .andWhere("applied_job.users_id", usersId)
      .first();
    return result;
  };
  insertAppliedJobStatus = async (usersId: number, jobsId: number) => {
    await this.knex
      .insert({
        users_id: usersId,
        jobs_id: jobsId,
        application_status: "Received",
      })
      .into("applied_job");
    return;
  };
  deleteAppliedJobStatus = async (usersId: number, jobsId: number) => {
    await this.knex("applied_job").where("jobs_id", jobsId).andWhere("users_id", usersId).del();
    return;
  };
}
