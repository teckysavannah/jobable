import { Knex } from "knex";

export class EmployerService {
  constructor(private knex: Knex) {}

  insertEmployerForm = async (employerFormData: Object) => {
    await this.knex.insert(employerFormData).into("employer_forms");
    return;
  };
  getJobIdByFormId = async (formId: number) => {
    const jobId = await this.knex.select("id").from("jobs").where("employer_forms_id", formId);
    return jobId;
  };
  getApplicants = async (jobId: number) => {
    const applicantsData = await this.knex
      .select(
        this.knex.raw(
          /*sql*/ `DISTINCT ON (users.user_name)users.user_name, user_profile.*,applied_job.application_status`
        )
      )
      .from("applied_job")
      .join("users", "users.id", "applied_job.users_id")
      .leftJoin("user_profile", "users.id", "user_profile.users_id")
      .where("applied_job.jobs_id", jobId)
      .orderBy("users.user_name", "desc")
      .orderBy("user_profile.id", "desc")
      .orderBy("applied_job.created_at", "asc");
    return applicantsData;
  };
  selectAllEmployerFormByEmployer = async (userId: number) => {
    const result = await this.knex
      .select("*")
      .from("employer_forms")
      .where("employer_users_id", userId)
      .orderBy("created_at", "asc");
    return result;
  };
}
