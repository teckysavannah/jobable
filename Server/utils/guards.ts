import { Bearer } from "permit";
import jwtSimple from "jwt-simple";
import { Request, Response, NextFunction } from "express";
import jwt from "./jwt";
import { User, WebUser } from "../models/models";
import { userService } from "../routes";
import { Permissions } from "../models/models";

const permit = new Bearer({
  query: "access_token",
});
// Function () => Base on Token return Permission Type
export function checkRole(payload: User | WebUser) {
  switch (payload.role) {
    case "user":
      return Permissions.user;
    case "admin":
      return Permissions.admin;
    case "employer":
      return Permissions.employer;
    default:
      return;
  }
}

export async function isLoggedIn(req: Request, res: Response, next: NextFunction) {
  try {
    const token = permit.check(req);
    if (!token) {
      res.status(401).json({ msg: "Permission Denied 1" });
      return;
    }
    const payload:User|WebUser = jwtSimple.decode(token, jwt.jwtSecret);
    const userId= payload.id
    const role = checkRole(payload);
    console.log({ role , userId});
    let user;
    if (role === "user") {
      user = await userService.getUserById(payload.id);
    }
    if (role === "admin" || role === "employer") {
      user = await userService.getWebUser(payload.id);
    }
    if (!user || !role) {
      res.status(401).json({ msg: "no user,Permission Denied" });
      return;
    }
    const { id, user_name } = user;
    req.user = { id, username: user_name, role };
    next();
    return;
  } catch (e) {
    res.status(401).json({ msg: "Permission Denied 3"});
    return;
  }
}
export async function isUser(req: Request, res: Response, next: NextFunction) {
  if (req.user!.role === "user" || req.user!.role === "admin") {
    next();
    return;
  } else {
    res.status(401).json({ msg: "Permission Denied" });
    return;
  }
}
export async function isAdmin(req: Request, res: Response, next: NextFunction) {
  if (req.user!.role === "admin") {
    next();
    return;
  } else {
    res.status(401).json({ msg: "Permission Denied" });
    return;
  }
}
export async function isEmployer(req: Request, res: Response, next: NextFunction) {
  if (req.user!.role === "employer"|| req.user!.role === "admin") {
    next();
    return;
  } else {
    res.status(401).json({ msg: "Permission Denied" });
    return;
  }
}
