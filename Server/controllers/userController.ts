import { Request, Response } from "express";
import { UserService } from "../services/userService";
import { logger } from "../utils/logger";
import { Bearer } from "permit";
import jwtSimple from "jwt-simple";
import jwt from "../utils/jwt";
import { checkPassword } from "../utils/hash";

const permit = new Bearer({
  query: "access_token",
});
export class UserController {
  constructor(private userService: UserService) {}

  register = async (req: Request, res: Response) => {
    try {
      const { username, password } = req.body;
      if (!username || !password) {
        console.log(1);
        res.status(400).json({ message: "invalid input" });
        return;
      }
      const result = await this.userService.isRegistered(username);
      // res.status(200).json({ result });
      if (result) {
        console.log(2);
        res.status(400).json({ message: "email has been registered!!!!!!" });
        return;
      }
      // do register
      const userId = parseInt(await this.userService.register(username, password));
      // create payload

      await this.userService.insertUserProfile({users_id:userId})
      
      const payload = {
        id: userId,
        username: username,
        role: "user",
      };
      // create token
      const token = jwtSimple.encode(payload, jwt.jwtSecret);
      res.json(
        { 
          token, 
          role:payload.role 
        }
      );
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
  login = async (req: Request, res: Response) => {
    try {
      if (!req.body.username || !req.body.password) {
        res.status(401).json({ msg: "No Username/Password" });
        return;
      }
      const { username, password } = req.body;
      const user = await this.userService.getUserByUsername(username);
      if (!user) {
        res.status(401).json("getUser failed");
        return;
      }
      if (!user || !(await checkPassword(password, user.password))) {
        res.status(401).json({ msg: "Wrong Username/Password" });
        return;
      }
      const payload = {
        id: user.id,
        username: user.user_name,
        role: "user",
      };
      const token = jwtSimple.encode(payload, jwt.jwtSecret);
      res.json({
        token: token,
        role:payload.role
      });
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
  toggleBookmark = async (req: Request, res: Response) => {
    try {
      const jobsId = req.body.jobsId;
      const token = permit.check(req);
      const payload = jwtSimple.decode(token, jwt.jwtSecret);
      const userId = parseInt(payload.id);
      const isBookmarked = await this.userService.checkBookmark(jobsId, userId);
      if (!isBookmarked) {
        await this.userService.insertBookmark(jobsId, userId);
        res.json({ message: "success" });
      } else {
        await this.userService.deleteBookmarked(jobsId, userId);
        res.json({ message: "success" });
      }
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
  deleteBookmark = async (req: Request, res: Response) => {
    try {
      const jobsId = req.body.jobsId;
      const token = permit.check(req);
      const payload = jwtSimple.decode(token, jwt.jwtSecret);
      const userId = parseInt(payload.id);
      await this.userService.deleteBookmarked(jobsId, userId);
      await this.userService.checkBookmark(jobsId, userId);
      res.json({ message: "success" });
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
  addHistory = async (req: Request, res: Response) => {
    try {
      const jobsId = parseInt(req.body.jobsId);
      const token = permit.check(req);
      const payload = jwtSimple.decode(token, jwt.jwtSecret);
      const userId = parseInt(payload.id);

      const isDuplicate = await this.userService.checkDuplicateHistory(jobsId, userId);
      console.log(isDuplicate);
      if (isDuplicate) {
        await this.userService.deleteDuplicateHistory(jobsId, userId);
      }
      const result = await this.userService.checkHistory(userId);
      if (result.length > 4) {
        await this.userService.deleteHistory(userId);
      }
      await this.userService.insertHistory(jobsId, userId);

      const updatedResult = await this.userService.checkHistory(userId);
      res.json({ history: updatedResult });
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
  checkHistory = async (req: Request, res: Response) => {
    try {
      const token = permit.check(req);
      const payload = jwtSimple.decode(token, jwt.jwtSecret);
      const userId = parseInt(payload.id);
      const result = await this.userService.checkHistory(userId);
      res.json({ history: result });
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
  updateUserProfile = async (req: Request, res: Response) => {
    try {
      const userId = req.user.id;

      // const token = permit.check(req);
      // const payload = jwtSimple.decode(token, jwt.jwtSecret);
      // const userId = parseInt(payload.id);
      // console.log('updating')
      const userProfileDetails = req.body.userProfileDetails;

      userProfileDetails["users_id"] = userId;
      // console.log('details',userProfileDetails)
      await this.userService.insertUserProfile(userProfileDetails);
      res.json({ insertUserProfile: "success" });
      return;
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
  getUserProfile = async (req: Request, res: Response) => {
    try {
      const userId = req.user.id;
      const result = await this.userService.getUserProfile(userId);
      res.json({ result });
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
}
