import { Request, Response } from "express";
import { logger } from "../utils/logger";
import { EmployerService } from "../services/employerService";
import { Bearer } from "permit";
import jwtSimple from "jwt-simple";
import jwt from "../utils/jwt";

const permit = new Bearer({
  query: "access_token",
});

export class EmployerController {
  constructor(private employerService: EmployerService) {}
  getApplicants= async (req: Request, res: Response) => {
    try{
      const jobId =req.body.jobId
      const applicantsInfo = await this.employerService.getApplicants(jobId)
      res.json({applicantsInfo})
      return;
    }catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  }
  getJobIdByFormId= async (req: Request, res: Response) => {
    try{
      const formId =req.body.formId
      const jobId = await this.employerService.getJobIdByFormId(formId)
      res.json({jobId})
      return;
    }catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  }
  insertEmployerForm = async (req: Request, res: Response) => {
    try {
      const employerFormData = req.body.employerFormData;
      const token = permit.check(req);
      const payload = jwtSimple.decode(token, jwt.jwtSecret);
      const userId = payload.id;
      const employerId = userId//req.user.id;
      console.log(req.body)
      employerFormData["employer_users_id"] = employerId;
      console.log(employerFormData)
      await this.employerService.insertEmployerForm(employerFormData);
      res.json({ message: "insert form success" });
      return;
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  }
    selectAllEmployerFormByEmployer = async (req: Request, res: Response) => {
      try {
        const userId = req.user.id;
        const result = await this.employerService.selectAllEmployerFormByEmployer(userId);
        res.json({ result });
        return;
      } catch (err) {
        logger.error(err.message);
        res.status(500).json({ message: "internal server error" });
      
    };
  };





}