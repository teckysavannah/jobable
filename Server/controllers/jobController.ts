import { Request, Response } from "express";
import { Bearer } from "permit";
import jwtSimple from "jwt-simple";
import jwt from "../utils/jwt";

import { JobService } from "../services/jobService";
import { logger } from "../utils/logger";
const permit = new Bearer({
  query: "access_token",
});
export class JobController {
  constructor(private jobService: JobService) {}

  getAllJobDetails = async (req: Request, res: Response) => {
    try {
      let jobDetails;
      if (!permit.check(req)) {
        jobDetails = await this.jobService.allJobDetails(null);
        res.json(jobDetails);
        return;
      }
      const token = permit.check(req);
      const payload = jwtSimple.decode(token, jwt.jwtSecret);
      const userId = payload.id;
      jobDetails = await this.jobService.allJobDetails(userId);
      console.log({ userId });

      res.json(jobDetails);
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };

  selectAllEmployerForm = async (req: Request, res: Response) => {
    try {
      const result = await this.jobService.selectAllEmployerForm();
      res.json({ result });
      return;
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
  
  selectSingleEmployerForm = async (req: Request, res: Response) => {
    try {
      const formId = req.body.formId;
      const result = await this.jobService.selectSingleEmployerForm(formId);
      res.json({ result });
      return;
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };

  getAppliedJobStatus = async (req: Request, res: Response) => {
    try {
      const userId = req.user.id;
      const jobsId = req.body.jobsId;
      if (userId === null) {
        res.status(401).json({ msg: "Permission Denied" });
        return;
      }
      //get all jobs status service
      if (!jobsId) {
        const result = await this.jobService.getAllAppliedJobStatus(userId);
        res.json({ result });
        return;
      }
      //================================
      const result = await this.jobService.getAppliedJobStatus(userId, jobsId);
      res.json({ result });
      return;
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
  insertAppliedJobStatus = async (req: Request, res: Response) => {
    try {
      const userId = req.user.id;
      const jobsId = req.body.jobsId;
      if (userId === null) {
        res.status(401).json({ msg: "Permission Denied" });
        return;
      }
      const checkResult = await this.jobService.getAppliedJobStatus(userId, jobsId);
      console.log('checkResult',checkResult)
      if (checkResult) {
        res.status(401).json({ msg: "job is already applied" });
        return;
      }
      await this.jobService.insertAppliedJobStatus(userId, jobsId);
      res.json({ insertAppliedJobStatus: "Success" });
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
  deleteAppliedJobStatus = async (req: Request, res: Response) => {
    try {
      const userId = req.user.id;
      const jobsId = req.body.jobsId;
      if (userId === null) {
        res.status(401).json({ msg: "Permission Denied" });
        return;
      }
      await this.jobService.deleteAppliedJobStatus(userId, jobsId);
      res.json({ message: "deleted" });
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
}
