import { Request, Response } from "express";
import { logger } from "../utils/logger";
import { AdminService } from "../services/adminService";

export class AdminController {
  constructor(private adminService: AdminService) {}

  updateEmployerFormStatus = async (req: Request, res: Response) => {
    try {
      const adminId = req.user.id;
      const { status, formId } = req.body;
      await this.adminService.updateEmployerForm(status, formId, adminId);
      res.json({ message: "updated" });
      return;
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };

  updateApplicationStatus = async (req: Request, res: Response) => {
    try {
      const { userId, jobId, status } = req.body;
      await this.adminService.updateApplicationStatus(userId, jobId, status);
      res.json({ message: "update success" });
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };

  editEmployerForm = async (req: Request, res: Response) => {
    try {
      const employerFormData = req.body.employerFormData;
      const formId = req.body.formId;
      const webUserId = req.user.id;
      employerFormData.webUser_users_id = webUserId;
      await this.adminService.editEmployerForm(employerFormData, formId);
      res.json({ message: "update success" });
      return;
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
  insertAdminPost = async (req: Request, res: Response) => {
    try {
      const { adminFormData, formId } = req.body;
      const fadminFormData = JSON.parse(adminFormData);
      fadminFormData.employer_forms_id = parseInt(formId);
      const adminFormId = await this.adminService.insertAdminPost(fadminFormData, (req.file as any).location);
      res.json({ adminFormId });
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
  editAdminJobPost = async (req: Request, res: Response) => {
    try {
      const { adminFormData, formId } = req.body;
      const fadminFormData = JSON.parse(adminFormData);
      fadminFormData.employer_forms_id = parseInt(formId);
      let fileLocation = "";
      if (req.file) {
        fileLocation = (req.file as any).location;
      }
      await this.adminService.editAdminJobPost(fadminFormData, fadminFormData.employer_forms_id, fileLocation);
      res.json({ message: "update success" });
      return;
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
}
