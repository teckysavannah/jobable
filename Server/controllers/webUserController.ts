import { WebUserService } from "../services/webUserService";
import { Request, Response } from "express";
import { logger } from "../utils/logger";
import { checkPassword } from "../utils/hash";
import jwtSimple from "jwt-simple";

import jwt from "../utils/jwt";

export class WebUserController {
  constructor(private webUserService: WebUserService) {}
  
  webUserLogin = async (req: Request, res: Response) => {
    try {
      if (!req.body.username || !req.body.password) {
        res.status(401).json({ msg: "No Username/Password" });
        return;
      }
      const { username, password } = req.body;
      const user = await this.webUserService.webUserLogin(username);
      if (!user) {
        res.status(401).json("getUser failed");
        return;
      }
      if (!user || !(await checkPassword(password, user.password))) {
        res.status(401).json({ msg: "Wrong Username/Password" });
        return;
      }
      const payload = {
        id: user.id,
        username: user.user_name,
        role: user.role,
      };
      const token = jwtSimple.encode(payload, jwt.jwtSecret);
      res.json({
        token: token,
        role:user.role
      });
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
  webUserRegister = async (req: Request, res: Response) => {
    try {
      const { username, password } = req.body;
      if (!username || !password) {
        res.status(400).json({ message: "invalid input" });
        return;
      }
      const result = await this.webUserService.isRegistered(username);
      if (result) {
        res.status(400).json({ message: "email has been registered!!!!!!" });
        return;
      }
      // do register
      const userId = parseInt(await this.webUserService.register(username, password));
      // create payload
      const payload = {
        id: userId,
        username: username,
        role: "employer",
      };
      // create token
      const token = jwtSimple.encode(payload, jwt.jwtSecret);
      res.json({ token });
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
  
}
