import cors from "cors";
import express from "express";
import path from "path";
import dotenv from 'dotenv'

dotenv.config()

import { logger } from "./utils/logger";
import multer from "multer";
import aws  from 'aws-sdk'
import multerS3 from "multer-s3";

const s3 = new aws.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: 'ap-southeast-1'
});

// const storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, path.resolve('./uploads'));
//   },
//   filename: function (req, file, cb) {
//     cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
//   }
// })
// export const upload = multer({storage})


export const upload = multer({
  storage: multerS3({
      s3: s3,
      bucket: 'jobable.upload',
      metadata: (req,file,cb)=>{
          cb(null,{fieldName: file.fieldname});
      },
      key: (req,file,cb)=>{
          cb(null,`${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
      }
  })
})

const app = express();
// URLENCODED => JS
app.use(express.urlencoded({ extended: true }));
app.use(cors({}));
// JSON string => JS
app.use(express.json());
app.use(express.static(path.resolve("public")));
app.use(express.static(path.resolve("images")));



// app.use(userRoutes);

import { routes } from "./routes";


const API_VERSION = "/api";
app.use(API_VERSION, routes);

const PORT = 8080;
app.listen(PORT, () => {
  logger.debug(`Listening to http://localhost:${PORT}`);
});
