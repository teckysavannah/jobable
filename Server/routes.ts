import express from "express";
import { knex } from "./knexConfig";

import { UserController } from "./controllers/userController";
import { UserService } from "./services/userService";

export const userService = new UserService(knex);
export const userController = new UserController(userService);

export const routes = express.Router();

import { jobRoutes } from "./routers/jobRoutes";
import { userRoutes } from "./routers/userRoutes";
import { webUserRoutes } from "./routers/webUserRoutes";
import { employerRoutes } from "./routers/employerRoutes";
import { adminRoutes } from "./routers/adminRoutes";



routes.use("/users", userRoutes);
routes.use("/jobs", jobRoutes);
routes.use("/webUser", webUserRoutes);
routes.use("/employer", employerRoutes);
routes.use("/admin", adminRoutes);



