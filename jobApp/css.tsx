import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    greyText: {
        fontSize: 14,
        color: "#898989",
        marginTop: 10,
        marginLeft: 8,
        width: "90%"
    },
    greySubText: {
        fontSize: 14,
        color: "#898989",
        marginTop: 10,
        marginLeft: 4,
        width: "90%"
    },
    boxIcon: {
        fontSize: 14,
        color: "#898989",
        marginTop: 11,
        marginLeft: 12
    },

    greyTextPost: {
        fontSize: 14,
        color: "#666",
        marginTop: 20,
        marginLeft: 20
    },
    greyTextDetail: {
        fontSize: 14,
        color: "#444",
        marginVertical: 10,
        marginHorizontal: 20,
        lineHeight: 22
    },
    boxTitlePost: {
        fontSize: 28,
        marginTop: 5,
        marginLeft: 20
    },
    boxTitle: {
        fontSize: 28,
        marginTop: 10,
        marginLeft: 12
    },
    bookmarkedBoxTitle: {
        fontSize: 18,
        marginTop: 10,
        marginLeft: 12,
        width: "60%"
    },
    boxSubTitle: {
        fontSize: 17,
        marginTop: 10,
        marginLeft: 20
    },
    formInput: {
        width: "65%",
        marginLeft: "31%",
        marginTop: -30
    },
    label: {
        width: "30%",
        height: 20,
        marginTop: 0,
        marginLeft: "5%",
        fontSize: 13,

    },
    formSubmit: {
        backgroundColor: "#70c14d",
        width: "80%",
        borderRadius: 30,
        marginHorizontal: "10%"
    },
    bookmarkIcon: {

        fontSize: 32,
        opacity: 0.6,
        color: '#ddd',
        marginTop: 1,
        marginLeft: 2
    },
    bookmarkIconClicked: {

        fontSize: 32,
        opacity: 1,
        color: '#e7e7e7',
        marginTop: 1,
        marginLeft: 2
    },
    postBookmarkIcon: {

        fontSize: 32,
        opacity: 1,
        color: '#222',
        marginTop: 1,
        marginLeft: 2
    },
    postBookmarkIconClick: {

        fontSize: 32,
        opacity: 1,
        color: '#d6af5a',
        marginTop: 1,
        marginLeft: 2
    },
    bookmarkBtn: {
        position: "absolute",
        top: 12,
        right: 12,
        height: 42,
        width: 42,
        backgroundColor: "#00000055",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 21

    },
    postBookmarkBtn: {
        width: "28%",
        height: 60,
        backgroundColor: "#fff",
        borderStyle: "solid",
        borderColor: "#aaa",
        borderWidth: 1,
        marginLeft: "4%",

        borderRadius: 30,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",

    },
    postBookmarkBtnClicked: {
        width: "28%",
        height: 60,
        backgroundColor: "#fff",
        borderStyle: "solid",
        borderColor: "#d6af5a",
        borderWidth: 1,
        marginLeft: "4%",

        borderRadius: 30,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",

    },
    bookmarkBtnClicked: {
        position: "absolute",
        top: 12,
        right: 12,
        height: 42,
        width: 42,
        backgroundColor: "#d6af5a",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 21

    },
    menuBarIcon: {
        fontSize: 26,
        color: "#898989",
        marginTop: 6

    },
    loginOrRegBtn: {
        backgroundColor: "#70c14d",
        width: "80%",
        height: 60,
        borderRadius: 30,
        marginHorizontal: "10%",
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginOrRegBtnCreate: {
        backgroundColor: "#fff",
        width: "80%",
        height: 60,
        borderRadius: 30,
        borderWidth: 3,
        borderColor: "#70c14d",
        marginHorizontal: "10%",
        justifyContent: 'center',
        alignItems: 'center'
    },
    barContainer: {
        flexDirection: 'row',
        marginTop: 20,
        marginHorizontal: '7%',
        justifyContent: 'space-between',
        alignItems: 'center'

    },
    barText: {
        width: 84,

    },
    barTextLeft: {
        width: 84,
        textAlign: 'right'
    },
    barStyle: {
        fontSize: 16,
        color: "#444",
        marginVertical: 20,
        marginHorizontal: '3%',
        lineHeight: 22,
        letterSpacing: -2,
    }


});