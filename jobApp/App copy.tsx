import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { ScrollView, Button, Text, View, Image, FlatList, StyleSheet, TouchableOpacity, Platform, Alert, RefreshControl } from 'react-native'
// import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
// import CheckCircleFillIcon from "react-native-bootstrap-icons/icons/check-circle-fill";
import { FaAddressBook, FaRegAddressBook } from 'react-icons/fa';
import { faAddressBook } from '@fortawesome/free-solid-svg-icons';
// import { FaBeer } from "@react-icons/all-files/fa/FaBeer";
// import { Icon } from 'react-native-elements'
import Icon from 'react-native-vector-icons/MaterialIcons';




import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';
import { Form, FormItem, Picker } from 'react-native-form-component'
import { launchImageLibrary } from 'react-native-image-picker';
import { Provider, useDispatch, useSelector } from 'react-redux'
import { store, RootState } from './store'
import { updateJob, updateBookmarked, userLoigin, userLogout, updateUserDataForm, initBookmarked, initHistory } from './actions'
import axios from 'axios'
import { JobData, UserFormData } from './type'

import SplashScreen from 'react-native-splash-screen'

const server = 'https://server.wcatw.xyz'
// const server = 'http://localhost:8080'
const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
// async function loaddata() {

// }

type postData = {
    id: number,
    poster: string,
    companyName: string,
    title: string,
    address: string,
    lat: number,
    lng: number,
    salary: string,
    details: string
}

const testDataArr: postData[] = [
    {
        id: 1,
        poster: "https://motto-jp.com/media/wp-content/uploads/2021/04/AdobeStock_142590859.jpeg",
        companyName: "ABC Community",
        title: "Happy Working",
        address: "16 Shing Yip St, Kwun Tong",
        lat: 22.31016045091338,
        lng: 114.22587909561412,
        salary: "11k",
        details: "契訶夫在不經意間這樣說過，人應當一切都美，外貌、衣著、靈魂、思想。想必各位已經看出了其中的端倪。就我個人來說，年收600萬對我的意義，不能不說非常重大。若沒有年收600萬的存在，那麼後果可想而知。若能夠洞悉年收600萬各種層面的含義，勢必能讓思維再提高一個層級。"

    },
    {
        id: 2,
        poster: "https://jpninfo.com/wp-content/uploads/sites/2/2017/05/japanese-job-hunters.jpg",
        companyName: "Facebook",
        title: "No workingexperiance required!",
        address: "Cyberport 2, Cyberport, 100 Cyberport Road",
        lat: 22.262652637891364,
        lng: 114.13084026862852,
        salary: "48k",
        details: "回過神才發現，思考年收600萬的存在意義，已讓我廢寢忘食。培根講過一段深奧的話，金錢像肥田料，如不散佈是沒有多大用處的。這段話非常有意思。那麼，巴爾扎克說過一句發人省思的話，晚秋季節還能找到春天和夏天錯過的鮮花嗎？ "
    },
    {
        id: 3,
        poster: "https://s3-ap-southeast-1.amazonaws.com/japanese-jobs-production-wordpress/wp-content/uploads/sites/29/2017/10/10125439/20.2.jpg",
        companyName: "Uber",
        title: "Let's apply!!!",
        address: "Smart Space 2 Cyberport 2 100 Cyberport Road HK",
        lat: 22.259712410036038,
        lng: 114.13176829996097,
        salary: "22k",
        details: "問題的關鍵看似不明確，但想必在諸位心中已有了明確的答案。如果仔細思考年收600萬，會發現其中蘊含的深遠意義。老舊的想法已經過時了。鄒韜奮說過一句經典的名言，最主要的是所選的朋友須正派，即品性端正的人。這影響了我的價值觀。"
    },
    {
        id: 4,
        poster: "https://www.tsunagulocal.com/wp-content/uploads/2020/06/%E3%82%B5%E3%83%A0%E3%83%8D%E3%82%A4%E3%83%AB-11.jpg",
        companyName: "PCCW",
        title: "No OT!",
        address: "27/F, PCCW Tower, Tower A, Dorset House, Taikoo Place, 979 King's Road, Quarry Bay, Hong Kong",
        lat: 22.28725633204046,
        lng: 114.21220451280834,
        salary: "18k",
        details: "        帶著這些問題，我們一起來審視年收600萬。這種事實對本人來說意義重大，相信對這個世界也是有一定意義的。若沒有年收600萬的存在，那麼後果可想而知。話雖如此，每個人都不得不面對這些問題。在面對這種問題時，務必詳細考慮年收600萬的各種可能。"
    },
    {
        id: 5,
        poster: "https://s3-ap-southeast-1.amazonaws.com/japanese-jobs-production-wordpress/wp-content/uploads/sites/29/2017/11/21190543/35-1024x682.jpg",
        companyName: "Google",
        title: "Smart Casual!!",
        address: "Level 25, Tower 2, Times Square, 1 Matheson St",
        lat: 22.278339616731788,
        lng: 114.1820384956135,
        salary: "36k",
        details: "        鄒韜奮說過一句經典的名言，最主要的是所選的朋友須正派，即品性端正的人。這段話讓我的心境提高了一個層次。我們不妨可以這樣來想: 儘管如此，別人往往卻不這麼想。年收600萬因何而發生？我們一般認為，抓住了問題的關鍵，其他一切則會迎刃而解。"
    },
]

function boxImg(imageurl: string, idx: number) {
    return (
        <Image style={{ width: 400, height: "55%", resizeMode: 'cover', marginEnd: 6 }} source={{ uri: imageurl }} key={idx} />
    )
}

function box(elem: JobData, idx: number, navi: any, cb: any, bookmark: [], token: string, testnum: number) {
    let bookmarkedArr = [...bookmark]
    // let data = {
    //     headers : {
    //         Authorization: `Bearer ${token}`
    //     },

    //     jobsId:elem.id

    // }
    let jobsId: number = elem.job_id
    return (
        <View style={{
            width: "94%",
            height: 400,
            backgroundColor: "#fafafa",
            marginTop: 30,
            marginLeft: "3%",
            borderRadius: 16,
            borderStyle: 'solid',
            borderWidth: 2,
            borderColor: "#fff",
            overflow: "hidden",

        }} key={elem.job_id} >

            <ScrollView nestedScrollEnabled={true} horizontal={true} showsHorizontalScrollIndicator={false}>
                <View style={{ height: 420, width: (elem.imageurl.length * 406) - 6, flexDirection: 'row' }}>
                    {elem.imageurl.map((e: string, idx: number) => boxImg(e, idx))}

                </View>
                {/* <Image style={{ width: "100%", height: "55%", resizeMode: 'cover' }} source={{ uri: elem.imageurl[0] }} /> */}

            </ScrollView>
            <TouchableOpacity style={bookmarkedArr[idx] ? styles.bookmarkBtnClicked : styles.bookmarkBtn} onPress={async () => {

                if (bookmarkedArr[idx]) {
                    bookmarkedArr[idx] = false
                } else {
                    bookmarkedArr[idx] = true
                }

                cb(updateBookmarked(bookmarkedArr))


                try {
                    const res = await fetch(`${server}/api/users/favourite`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        },
                        body: JSON.stringify({
                            jobsId: jobsId
                        })

                    })



                } catch (error) {

                    if (error?.response.status == 401) {
                        console.warn('bookmark error')
                    }

                }







            }}>

                <Icon name={bookmarkedArr[idx] ? 'bookmark' : 'bookmark-border'}
                    style={bookmarkedArr[idx] ? styles.bookmarkIconClicked : styles.bookmarkIcon}
                />
            </TouchableOpacity>

            <TouchableOpacity style={{ position: 'absolute', top: 240 }} onPress={async () => {

                try {
                    const res = await fetch(`${server}/api/users/history`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        },
                        body: JSON.stringify({
                            jobsId: jobsId
                        })

                    })




                } catch (error) {

                    if (error?.response.status == 401) {
                        console.warn('bookmark error')
                    }

                }

                cb(initHistory(elem.job_id))
                navi.navigate('Details', {
                    itemId: elem.job_id
                })
            }}>

                <View style={{ flexDirection: "row" }}>
                    <Icon name="work" style={styles.boxIcon} />
                    <Text style={styles.greyText}>{elem.comapany_name}</Text>
                </View>
                <View style={{ flexDirection: "row" }}>

                    <Text style={styles.boxTitle}>{elem.title}</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                    <Icon name="location-on" style={styles.boxIcon} />
                    <Text style={styles.greySubText}>{elem.address}</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                    <Icon name="attach-money" style={styles.boxIcon} />
                    <Text style={styles.greySubText}>{elem.salary_project}</Text>
                </View>
            </TouchableOpacity>




        </View>
    )
}
function bookmarkBox(elem: any, idx: number, navi: any, cb: any, bookmark: [], token: string) {
    let bookmarkedArr = [...bookmark]
    let jobsId: number = elem.job_id
    return (
        <TouchableOpacity style={{
            width: "94%",
            height: 300,
            backgroundColor: "#fdfdfd",
            marginTop: 30,
            marginLeft: "3%",
            borderRadius: 16,

            overflow: "hidden",
        }} key={elem.job_id} onPress={() => {
            navi.navigate('Details', {
                itemId: elem.job_id
            })
        }}>
            <TouchableOpacity style={{ height: 40, flexDirection: "row", justifyContent: "space-between" }} onPress={async () => {

                bookmarkedArr[idx] = false


                cb(updateBookmarked(bookmarkedArr))


                try {
                    const res = await fetch(`${server}/api/users/favourite`, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        },
                        body: JSON.stringify({
                            jobsId: jobsId
                        })

                    })



                } catch (error) {

                    if (error?.response.status == 401) {
                        console.warn('bookmark error')
                    }

                }
            }}>
                <Text></Text>
                <Text style={{ marginTop: 15, marginRight: 15, }}>✖️</Text>
            </TouchableOpacity>
            <View style={{ flexDirection: "row", height: 80, width: "100%", }}>

                <Image style={{ width: "33%", height: "100%", resizeMode: 'cover', marginLeft: 12, borderRadius: 7 }} source={{ uri: elem.imageurl[0] }} />
                <Text style={styles.bookmarkedBoxTitle}>{elem.title}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="work" style={styles.boxIcon} />
                <Text style={styles.greyText}>{elem.comapany_name}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="location-on" style={styles.boxIcon} />
                <Text style={styles.greySubText}>{elem.address}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="attach-money" style={styles.boxIcon} />
                <Text style={styles.greySubText}>{elem.salary_project}</Text>
            </View>

            <View style={{ height: 30, borderStyle: "solid", borderBottomColor: "#ccc", borderBottomWidth: 1, width: "90%", marginLeft: "5%" }}></View>
            <TouchableOpacity style={{
                width: "80%",
                height: 30,
                backgroundColor: "#72c91a",
                marginTop: 15,
                marginLeft: "10%",
                borderRadius: 30,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
            }}
                onPress={() => {
                    navi.navigate('Apply Page', {
                        itemId: elem.job_id
                    })
                }}
            >
                <Text style={{
                    fontSize: 16,
                    color: "#fff",
                    fontWeight: "500",
                }}>Apply</Text>
            </TouchableOpacity>

        </TouchableOpacity>
    )
}

function historyBox(elem: JobData, navi: any, token: string) {
    let jobsId: number = elem.job_id
    return (
        <TouchableOpacity style={{
            width: "94%",
            height: 300,
            backgroundColor: "#fdfdfd",
            marginTop: 30,
            marginLeft: "3%",
            borderRadius: 16,

            overflow: "hidden",
        }} key={elem.job_id} onPress={() => {
            navi.navigate('Details', {
                itemId: elem.job_id
            })
        }}>
            <View style={{ height: 40, flexDirection: "row", justifyContent: "space-between" }}>
                <Text></Text>

            </View>
            <View style={{ flexDirection: "row", height: 80, width: "100%", }}>

                <Image style={{ width: "33%", height: "100%", resizeMode: 'cover', marginLeft: 12, borderRadius: 7 }} source={{ uri: elem.imageurl[0] }} />
                <Text style={styles.bookmarkedBoxTitle}>{elem.title}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="work" style={styles.boxIcon} />
                <Text style={styles.greyText}>{elem.comapany_name}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="location-on" style={styles.boxIcon} />
                <Text style={styles.greySubText}>{elem.address}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="attach-money" style={styles.boxIcon} />
                <Text style={styles.greySubText}>{elem.salary_project}</Text>
            </View>
            <View style={{ height: 30, borderStyle: "solid", borderBottomColor: "#ccc", borderBottomWidth: 1, width: "90%", marginLeft: "5%" }}></View>
            <TouchableOpacity style={{
                width: "80%",
                height: 30,
                backgroundColor: "#72c91a",
                marginTop: 15,
                marginLeft: "10%",
                borderRadius: 30,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
            }}
                onPress={() => {
                    navi.navigate('Apply Page', {
                        itemId: elem.job_id
                    })
                }}>
                <Text style={{
                    fontSize: 16,
                    color: "#fff",
                    fontWeight: "500",
                }}>Apply</Text>
            </TouchableOpacity>

        </TouchableOpacity>
    )
}

function appliedBox(elem: JobData, navi: any) {
    return (
        <TouchableOpacity style={{
            width: "94%",
            height: 250,
            backgroundColor: "#fdfdfd",
            marginTop: 30,
            marginLeft: "3%",
            borderRadius: 16,

            overflow: "hidden",
        }} key={elem.job_id} onPress={() => {
            navi.navigate('Details', {
                itemId: elem.job_id
            })
        }}>
            <View style={{ height: 40, flexDirection: "row", justifyContent: "space-between" }}>
                <Text></Text>

            </View>
            <View style={{ flexDirection: "row", height: 80, width: "100%", }}>

                <Image style={{ width: "33%", height: "100%", resizeMode: 'cover', marginLeft: 12, borderRadius: 7 }} source={{ uri: elem.imageurl[0] }} />
                <Text style={styles.bookmarkedBoxTitle}>{elem.title}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="work" style={styles.boxIcon} />
                <Text style={styles.greyText}>{elem.comapany_name}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="location-on" style={styles.boxIcon} />
                <Text style={styles.greySubText}>{elem.address}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="attach-money" style={styles.boxIcon} />
                <Text style={styles.greySubText}>{elem.salary_project}</Text>
            </View>



        </TouchableOpacity>
    )
}

function barIcon() {
    return (
        <Text style={{ color: '#d93f6a', fontSize: 20, position: 'absolute' }}>✿</Text>
    )
}

function barGenerator5(num: number) {
    let circle = "○"

    return (
        <Text style={styles.barStyle}>
            {num == 0 ? barIcon() : circle}---{num == 1 ? barIcon() : circle}---{num == 2 ? barIcon() : circle}---{num == 3 ? barIcon() : circle}---{num == 4 ? barIcon() : circle}
        </Text>
    )
}
function barGenerator3(num: number) {
    let circle = "○"


    return (
        <Text style={styles.barStyle}>
            {num == 0 ? barIcon() : circle}--------{num == 1 ? barIcon() : circle}--------{num == 2 ? barIcon() : circle}
        </Text>
    )
}
function ageBarGenerator(num: number) {

    let circle = "○"


    let binaryStr = Number(num).toString(2)

    let arr = binaryStr.split('')

    while (arr.length < 5) {
        arr.unshift("0")
    }


    return (


        <Text style={styles.barStyle}>
            {arr[0] == "1" ? barIcon() : circle}---{arr[1] == "1" ? barIcon() : circle}---{arr[2] == "1" ? barIcon() : circle}---{arr[3] == "1" ? barIcon() : circle}---{arr[4] == "1" ? barIcon() : circle}
        </Text>


    )
}



function post(elem: any, idx: number, cb: any, bookmark: [], navi, finalToken: string) {
    let jobsId: number = elem.job_id
    return (
        <View key={elem.job_id}>

            <Image style={{ width: "100%", height: 300, resizeMode: 'cover' }} source={{ uri: elem.imageurl[0] }} />
            <Text style={styles.greyTextPost}>{elem['comapany_name']}</Text>
            <Text style={styles.boxTitlePost}>{elem.title}</Text>
            <Text style={styles.greyTextDetail}>{elem.descriptions}</Text>
            <View style={{ flexDirection: "row", marginTop: 10 }}>

                <TouchableOpacity
                    style={{
                        width: "60%",
                        height: 60,
                        backgroundColor: "#95b844",
                        marginLeft: "4%",

                        borderRadius: 30,
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                    onPress={() => {
                        navi.navigate('Apply Page', {
                            itemId: elem.job_id
                        })
                    }}
                >
                    <Text style={{
                        fontSize: 25,
                        color: "#fff",
                        fontWeight: "700",
                    }}>Apply</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={bookmark[idx] ? styles.postBookmarkBtnClicked : styles.postBookmarkBtn}
                    onPress={async () => {
                        let bookmarkArr = [...bookmark]
                        if (bookmarkArr[idx]) {
                            bookmarkArr[idx] = false
                        } else {
                            bookmarkArr[idx] = true
                        }
                        cb({
                            type: 'updateBookmarked',
                            data: bookmarkArr
                        })

                        try {
                            const res = await fetch(`${server}/api/users/favourite`, {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': `Bearer ${finalToken}`
                                },
                                body: JSON.stringify({
                                    jobsId: jobsId
                                })

                            })



                        } catch (error) {

                            if (error?.response.status == 401) {
                                console.warn('bookmark error')
                            }

                        }
                    }}>
                    <Text style={{
                        fontSize: 25,
                        color: "#555",
                        fontWeight: "700",
                    }}>
                        <Icon name={bookmark[idx] ? 'bookmark' : 'bookmark-border'}
                            style={bookmark[idx] ? styles.postBookmarkIconClick : styles.postBookmarkIcon}
                        />
                    </Text>
                </TouchableOpacity>
            </View>

            <View style={{
                width: "90%",
                height: 20,
                borderBottomWidth: 1,
                borderColor: "#aaa",
                margin: "5%"
            }}></View>

            <Text style={styles.boxTitlePost}>Basic Info</Text>
            <View style={{ height: 20 }}></View>
            <Text style={styles.boxSubTitle}>Job Highlight</Text>
            <Text style={styles.greyTextDetail}>{elem.job_duties}</Text>
            <Text style={styles.boxSubTitle}>Address</Text>
            <Text style={styles.greyTextDetail}>{elem.address}</Text>
            <MapView
                style={{
                    width: "94%",
                    height: 300,
                    marginHorizontal: "3%",
                    borderRadius: 10,
                    marginBottom: 50


                }}
                initialRegion={{
                    latitude: parseInt(elem.lng),
                    longitude: parseInt(elem.lat),
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}
            >
                <Marker

                    coordinate={{ latitude: elem.lat, longitude: elem.lng }}

                />
            </MapView>

            <Text style={styles.boxSubTitle}>Salary</Text>
            <Text style={styles.greyTextDetail}>{elem.salary_project}</Text>
            <Text style={styles.boxSubTitle}>Working Hours</Text>
            <Text style={styles.greyTextDetail}>❖  {elem.working_hours} hours / day</Text>

            <Text style={styles.greyTextDetail}>❖  {elem.days_per_week} days / week</Text>

            <Text style={styles.greyTextDetail}>❖  {elem.annual_leave} days annual</Text>
            <Text style={styles.greyTextDetail}>❖  Contract Period : {elem.working_period} months</Text>

            <View style={{
                width: "90%",
                height: 20,
                borderBottomWidth: 1,
                borderColor: "#aaa",
                margin: "5%"
            }}></View>

            <Text style={styles.boxTitlePost}>Workplace Environment</Text>
            <View style={{ height: 20 }}></View>
            <View>

                <View style={styles.barContainer}>
                    <Text style={styles.barTextLeft}>Younger</Text>
                    <Text style={styles.barStyle}>{ageBarGenerator(elem.age_range)}</Text>
                    <Text style={styles.barText}>Older</Text>
                </View>
                <Text style={{ textAlign: 'center', fontSize: 12 }}>10s   20s   30s   40s   50s</Text>
                <View style={styles.barContainer}>
                    <Text style={styles.barTextLeft}>More male</Text>
                    <Text style={styles.barStyle}>{barGenerator5(elem.gender_range)}</Text>
                    <Text style={styles.barText}>More female</Text>
                </View>
                <View style={styles.barContainer}>
                    <Text style={styles.barTextLeft}>Independent</Text>
                    <Text style={styles.barStyle}>{barGenerator5(elem.teamwork_range)}</Text>
                    <Text style={styles.barText}>Group-working</Text>
                </View>
                <View style={styles.barContainer}>
                    <Text style={styles.barTextLeft}>Flexible workplace</Text>
                    <Text style={styles.barStyle}>{barGenerator3(elem.wfh_range)}</Text>
                    <Text style={styles.barText}>Office</Text>
                </View>
                <View style={styles.barContainer}>
                    <Text style={styles.barTextLeft}>Desk work</Text>
                    <Text style={styles.barStyle}>{barGenerator3(elem.client_range)}</Text>
                    <Text style={styles.barText}>Client side</Text>
                </View>
                <View style={styles.barContainer}>
                    <Text style={styles.barTextLeft}>Experience less required</Text>
                    <Text style={styles.barStyle}>{barGenerator3(elem.experience_range)}</Text>
                    <Text style={styles.barText}>Experience required</Text>
                </View>


            </View>
            <View style={{
                width: "90%",
                height: 20,
                borderBottomWidth: 1,
                borderColor: "#aaa",
                margin: "5%"
            }}></View>

            <Text style={styles.boxSubTitle}>Experience Requirement</Text>
            <Text style={styles.greyTextDetail}>{elem.exp_requirement}</Text>
            <Text style={styles.boxSubTitle}>Benefits</Text>
            <Text style={styles.greyTextDetail}>{elem.benefits}</Text>
            <Text style={styles.boxSubTitle}>Ways to Apply</Text>
            <Text style={styles.greyTextDetail}>{elem.apply_method}</Text>
            <Text style={styles.boxSubTitle}>Application Due Date</Text>
            <Text style={styles.greyTextDetail}>{elem.deadline.split('T')[0]}</Text>
            <Text style={styles.boxSubTitle}>Company Website</Text>
            <Text style={styles.greyTextDetail}>{elem.url}</Text>

            <View style={{ height: 100 }}></View>



        </View>
    )
}



function ApplyScreen({ route, navigation }) {
    const { itemId } = route.params
    const formData = useSelector((state: RootState) => state.userFormData)
    const isLoggedIn = useSelector((state: RootState) => state.isLoggedIn)
    const token = useSelector((state: RootState) => state.userToken)


    const dispatch = useDispatch()
    const [lastName, setLastName] = useState(formData.lastName)
    const [firstName, setFirstName] = useState(formData.firstName)
    const [birth, setBirth] = useState(formData.birth)
    const genderNum = ['Others', 'Male', 'Female']
    const [gender, setGender] = useState(genderNum.indexOf(formData.gender))
    const [tel, setTel] = useState(formData.tel)
    const [email, setEmail] = useState(formData.email)
    const [address, setAddress] = useState(formData.address)
    const currentOccupationArr = ['', 'Student', 'Freelance', 'Part-Time', 'Full-Time', 'Unemployed', 'Others']
    const [currentOccupation, setCurrentOccupation] = useState<string>(formData.currentJob)
    const [aboutMe, setAboutMe] = useState(formData.aboutMe)
    const freshGradNum = [null, true, false]
    const [workExp, setWorkExp] = useState(freshGradNum.indexOf(formData.workExp))
    const educationArr = ['', 'High School', 'Diploma', 'Higher-Diploma', 'Degree', 'Master or abrove', 'Others']
    const [education, setEducation] = useState<string>(formData.education)
    const [schoolName, setSchoolName] = useState(formData.schoolName)
    const [major, setMajor] = useState(formData.major)
    const [gradYear, setGradYear] = useState(formData.gradYear)
    const contactTimeNum = ['', 'Whole Day', 'Morning', 'Afternoon']
    const [contactTime, setContactTime] = useState(contactTimeNum.indexOf(formData.contactTime))


    const getUserProfile = async () => {
        try {
            const response = await fetch(
                `${server}/api/users/userProfile`,
                {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },

                }
            );
            const json = await response.json();
            if (json.result){

                dispatch(updateUserDataForm({
                    lastName:json.result.last_name,
                    firstName:json.result.first_name,
                    birth:json.result.birthday,
                    gender: json.result.gender,
                    tel:json.result.mobile,
                    email:json.result.email,
                    address:json.result.address,
                    currentJob: json.result.occupation,
                    aboutMe:json.result.descriptions,
                    workExp: json.result.work_exp,
                    education:json.result.education_level,
                    schoolName:json.result.school_name,
                    major:json.result.program_name,
                    gradYear:json.result.grad_year,
                    contactTime: json.result.contact_time
                    
                }))
                // console.warn(json)
                setLastName(json.result.last_name)
                setFirstName(json.result.first_name)
                setBirth(json.result.birthday)
                setGender(genderNum.indexOf(json.result.gender))
                setTel(json.result.mobile)
                setEmail(json.result.email)
                setAddress(json.result.address)
                setCurrentOccupation(json.result.occupation)
                setAboutMe(json.result.descriptions)
                setWorkExp(freshGradNum.indexOf(json.result.work_exp))
                setEducation(json.result.education_level)
                setSchoolName(json.result.school_name)
                setMajor(json.result.program_name)
                setGradYear(json.result.grad_year)
                setContactTime(contactTimeNum.indexOf(json.result.contact_time))
            }

        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        getUserProfile();

    }, [formData]);




    const maleColorList = ["#fff", "#210000", "#fff"]
    const femaleColorList = ["#fff", "#fff", "#210000"]
    const maleFontColorList = ["#888", "#210000", "#888"]
    const femaleFontColorList = ["#888", "#888", "#210000"]

    const amColorList = ["#fff", "#210000", "#fff", "#fff"]
    const pmColorList = ["#fff", "#fff", "#210000", "#fff"]
    const allDayColorList = ["#fff", "#fff", "#fff", "#210000"]
    const amFontColorList = ["#888", "#210000", "#888", "#888"]
    const pmFontColorList = ["#888", "#888", "#210000", "#888"]
    const allDayFontColorList = ["#888", "#888", "#888", "#210000"]


    return (
        <ScrollView>

            <View style={{ height: 40 }}></View>
            <Form
                onButtonPress={async() => {
                    dispatch(updateUserDataForm({
                        lastName,
                        firstName,
                        birth,
                        gender: genderNum[gender],
                        tel,
                        email,
                        address,
                        currentJob: currentOccupation,
                        aboutMe,
                        workExp: freshGradNum[workExp],
                        education,
                        schoolName,
                        major,
                        gradYear,
                        contactTime: contactTimeNum[contactTime]

                    }))
                    if (isLoggedIn) {
                        try {
                            const response = await fetch(
                                `${server}/api/users/userProfile`,
                                {
                                    method: 'POST',
                                    headers: {
                                        'Content-Type': 'application/json',
                                        'Authorization': `Bearer ${token}`,
                                        
                                    },
                                    body:JSON.stringify({
                                            
                                        last_name:lastName,
                                        first_name:firstName,
                                        birthday:birth,
                                        gender:genderNum[gender],
                                        mobile:tel,
                                        email:email,
                                        address:address,
                                        occupation:currentOccupation,
                                        descriptions:aboutMe,
                                        work_exp:freshGradNum[workExp],
                                        education_level:education,
                                        school_name:schoolName,
                                        program_name:major,
                                        grad_year:gradYear,
                                        contact_time:contactTimeNum[contactTime],
                                    })
                
                                }   
                            );
                            
                            const response2 = await fetch(
                                `${server}/api/jobs/appliedJob`,
                                {
                                    method: 'POST',
                                    headers: {
                                        'Content-Type': 'application/json',
                                        'Authorization': `Bearer ${token}`,
                                        
                                    },
                                    body:JSON.stringify({
                                            
                                        jobsId:itemId
                                    })
                
                                }   
                            );



                            const json = await response2.json();
                            
                            console.warn(json)
                
                        } catch (error) {
                            console.error(error);
                        }

                        Alert.alert(
                            "Success",
                            "Apply success!",
                            [

                                { text: "OK", onPress: () => navigation.navigate('Menubar') }
                            ]
                        );
                    } else {
                        navigation.navigate('LoginOrReg')
                    }
                }

                }

                buttonStyle={styles.formSubmit}
                buttonText="Apply"
            >
                <FormItem
                    label="Last Name"
                    isRequired
                    value={lastName}
                    onChangeText={(lastName) => setLastName(lastName)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />
                <FormItem
                    label="First Name"
                    isRequired
                    value={firstName}
                    onChangeText={(firstName) => setFirstName(firstName)}
                    labelStyle={styles.label}

                    style={styles.formInput}
                />
                <FormItem
                    label="Birth"
                    isRequired
                    value={birth}
                    onChangeText={(birth) => setBirth(birth)}
                    labelStyle={styles.label}
                    placeholder='dd/mm/yyyy'
                    style={styles.formInput}
                />

                <View style={{
                    height: 100,
                    flexDirection: "row"
                }}>
                    <Text style={{
                        width: "20%",
                        height: 80,
                        marginTop: 15,
                        marginLeft: "5%",
                        fontWeight: "700"
                    }}>Gender</Text>

                    <TouchableOpacity
                        onPress={() => setGender(() => gender == 1 ? 0 : 1)}
                        style={{
                            width: "20%",
                            height: 50,
                            backgroundColor: "#fff",
                            borderColor: maleColorList[gender],
                            borderStyle: "solid",
                            borderWidth: 1,
                            marginLeft: "10%",
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <Text style={{ color: maleFontColorList[gender], fontSize: 15 }}>Male</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => setGender(() => gender == 2 ? 0 : 2)}
                        style={{
                            width: "20%",
                            height: 50,
                            backgroundColor: "#fff",
                            borderColor: femaleColorList[gender],
                            borderStyle: "solid",
                            borderWidth: 1,
                            marginLeft: "10%",
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <Text style={{ color: femaleFontColorList[gender], fontSize: 15 }}>Female</Text>
                    </TouchableOpacity>
                </View>

                <FormItem
                    label="Tel"
                    isRequired
                    value={tel}
                    onChangeText={(tel) => setTel(tel)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />
                <FormItem
                    label="Email"
                    isRequired
                    value={email}
                    onChangeText={(email) => setEmail(email)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />
                <FormItem
                    label="Address"
                    isRequired
                    value={address}
                    onChangeText={(address) => setAddress(address)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />
                <Picker
                    items={[
                        { label: 'Student', value: 1 },
                        { label: 'Freelance', value: 2 },
                        { label: 'Part-Time', value: 3 },
                        { label: 'Full-Time', value: 4 },
                        { label: 'Unemployed', value: 5 },
                        { label: 'Others', value: 6 },
                    ]}
                    label="Current Job"
                    selectedValue={currentOccupationArr.indexOf(currentOccupation)}
                    onSelection={(item) => setCurrentOccupation(item["label"])}
                    buttonStyle={styles.formInput}
                    labelStyle={styles.label}
                    placeholder='Please select'
                />
                <FormItem
                    label="About Me"
                    isRequired
                    value={aboutMe}
                    onChangeText={(aboutMe) => setAboutMe(aboutMe)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />

                <View style={{
                    height: 100,
                    flexDirection: "row"
                }}>
                    <Text style={{
                        width: "20%",
                        height: 80,
                        marginTop: 15,
                        marginLeft: "5%",
                        fontWeight: "700",
                        fontSize: 13
                    }}>Work Experience</Text>

                    <TouchableOpacity
                        onPress={() => setWorkExp(() => workExp == 1 ? 0 : 1)}
                        style={{
                            width: "20%",
                            height: 50,
                            backgroundColor: "#fff",
                            borderColor: maleColorList[workExp],
                            borderStyle: "solid",
                            borderWidth: 1,
                            marginLeft: "10%",
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <Text style={{ color: maleFontColorList[workExp], fontSize: 15 }}>Yes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => setWorkExp(() => workExp == 2 ? 0 : 2)}
                        style={{
                            width: "20%",
                            height: 50,
                            backgroundColor: "#fff",
                            borderColor: femaleColorList[workExp],
                            borderStyle: "solid",
                            borderWidth: 1,
                            marginLeft: "10%",
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <Text style={{ color: femaleFontColorList[workExp], fontSize: 15 }}>No</Text>
                    </TouchableOpacity>
                </View>

                <Picker
                    items={[
                        { label: 'High School', value: 1 },
                        { label: 'Diploma', value: 2 },
                        { label: 'Higher-Diploma', value: 3 },
                        { label: 'Degree', value: 4 },
                        { label: 'Master or abrove', value: 5 },
                        { label: 'Others', value: 6 },
                    ]}
                    label="Education"
                    selectedValue={educationArr.indexOf(education)}
                    onSelection={(item) => setEducation(item["label"])}
                    buttonStyle={styles.formInput}
                    labelStyle={styles.label}
                    placeholder='Please select'
                />
                <FormItem
                    label="School Name"
                    isRequired
                    value={schoolName}
                    onChangeText={(schoolName) => setSchoolName(schoolName)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />
                <FormItem
                    label="Major"
                    isRequired
                    value={major}
                    onChangeText={(major) => setMajor(major)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />
                <FormItem
                    label="Grad Year"
                    isRequired
                    value={gradYear}
                    onChangeText={(gradYear) => setGradYear(gradYear)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />
                <View style={{
                    height: 100,
                    flexDirection: "row"
                }}>
                    <Text style={{
                        width: "20%",
                        height: 80,
                        marginTop: 15,
                        marginLeft: "5%",
                        fontWeight: "700"
                    }}>Contact Time</Text>

                    <TouchableOpacity
                        onPress={() => setContactTime(() => contactTime == 1 ? 0 : 1)}
                        style={{
                            width: "18%",
                            height: 50,
                            backgroundColor: "#fff",
                            borderColor: amColorList[contactTime],
                            borderStyle: "solid",
                            borderWidth: 1,
                            marginLeft: "5%",
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <Text style={{ color: amFontColorList[contactTime], fontSize: 15 }}>a.m.</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => setContactTime(() => contactTime == 2 ? 0 : 2)}
                        style={{
                            width: "18%",
                            height: 50,
                            backgroundColor: "#fff",
                            borderColor: pmColorList[contactTime],
                            borderStyle: "solid",
                            borderWidth: 1,
                            marginLeft: "5%",
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <Text style={{ color: pmFontColorList[contactTime], fontSize: 15 }}>p.m.</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => setContactTime(() => contactTime == 3 ? 0 : 3)}
                        style={{
                            width: "18%",
                            height: 50,
                            backgroundColor: "#fff",
                            borderColor: allDayColorList[contactTime],
                            borderStyle: "solid",
                            borderWidth: 1,
                            marginLeft: "5%",
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <Text style={{ color: allDayFontColorList[contactTime], fontSize: 15 }}>All Day</Text>
                    </TouchableOpacity>
                </View>


            </Form>
        </ScrollView>
    )
}


const logo = {
    uri: 'https://www.tsunagulocal.com/wp-content/uploads/2020/06/%E3%82%B5%E3%83%A0%E3%83%8D%E3%82%A4%E3%83%AB-11.jpg',
    width: 64,
    height: 64
};


const wait = (timeout: any) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
}

function HomeScreen({ navigation }) {

    // const [data, setData] = useState([]);
    // const [bookmarked, setBookmarked] = useState([])
    const [refreshing, setRefreshing] = React.useState(false);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getJobsFromApiAsync()
        wait(2000).then(() => setRefreshing(false));
    }, []);

    const dispatch = useDispatch()

    const data = useSelector((state: RootState) => state.postdata)
    const bookmarked = useSelector((state: RootState) => state.bookmarkedByUser)
    const token = useSelector((state: RootState) => state.userToken)

    const [finalToken, setFinalToken] = useState(token)

    useEffect(() => {
        setFinalToken(token)
    }, [token])

    const testnum = Math.random() * 10

    const getJobsFromApiAsync = async () => {
        try {
            const response = await fetch(
                `${server}/api/jobs/allJobs`,
                {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },

                }
            );
            const json = await response.json();
            dispatch(updateJob(json));
            dispatch(initBookmarked(json))


        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        getJobsFromApiAsync();

    }, [token]);


    return (


        <ScrollView
            style={
                { backgroundColor: "#fff" }
            }
            contentContainerStyle={{
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.25,
                shadowRadius: 6.84,


            }}
            
            refreshControl={
                <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            }
        >
            {/* <Text>
                Try editing me! 🎉
            </Text>
            <Button
                title="go to details?"
                onPress={() => navigation.navigate('Details')}
            /> */}


            {/* <TouchableOpacity onPress={()=>console.warn(data[0].bookmarked)}>
                <Text>hihi</Text>
            </TouchableOpacity> */}



            {data.map((elem, idx) => {   //alt  testDataArr
                return box(elem, idx, navigation, dispatch, bookmarked, finalToken, testnum)
            })}

            <View style={{ height: 34 }}></View>
        </ScrollView>

    )
}

function LoginScreen({ navigation }) {
    const [email, setEmail] = useState("")
    const [pw, setPw] = useState("")
    const [message, setMessage] = useState("")
    const dispatch = useDispatch()
    let data = {
        username: email,
        password: pw
    }
    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ height: 40 }}></View>
            <Form
                onButtonPress={async () => {

                    try {
                        const res = await axios.post(`${server}/api/users/login`, data)
                        dispatch(userLoigin(res.data.token))
                        Alert.alert(
                            "Success",
                            "Login success!",
                            [

                                { text: "OK", onPress: () => navigation.navigate('Menubar') }
                            ]
                        );
                    } catch (error) {

                        if (error?.response.status == 401) {
                            setMessage('Invaild Username/Password')
                        }

                    }


                }}
                buttonStyle={styles.formSubmit}
                buttonText="Login"
            >
                <FormItem
                    label="Email"
                    isRequired
                    value={email}
                    onChangeText={(email) => setEmail(email)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                    autoCapitalize='none'
                />
                <FormItem
                    label="Password"
                    isRequired
                    value={pw}
                    onChangeText={(pw) => setPw(pw)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                    secureTextEntry={true}
                />
                <Text style={{
                    color: '#f00',
                    marginLeft: 40
                }}>{message}</Text>
            </Form>



        </ScrollView>

    )
}

function RegisterScreen({ navigation }) {
    const [email, setEmail] = useState("")
    const [pw, setPw] = useState("")
    const [pw2, setPw2] = useState("")
    const [message, setMessage] = useState("")
    const dispatch = useDispatch()
    let data = {
        username: email,
        password: pw
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ height: 40 }}></View>


            <Form
                onButtonPress={async () => {
                    if (pw != pw2) {
                        setMessage('Please enter same password twice')
                    } else {

                        try {
                            const res = await axios.post(`${server}/api/users/register`, data)
                            dispatch(userLoigin(res.data.token))
                            Alert.alert(
                                "Success",
                                "Register success!",
                                [

                                    { text: "OK", onPress: () => navigation.navigate('Menubar') }
                                ]
                            );
                        } catch (error) {
                            if (error?.response.status == 400) {
                                setMessage('Email has been registered')
                            }

                        }
                    }


                }}
                buttonStyle={styles.formSubmit}
                buttonText="Register"
            >
                <FormItem
                    label="Email"
                    isRequired
                    value={email}
                    onChangeText={(email) => setEmail(email)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                    autoCapitalize='none'
                />
                <FormItem
                    label="Password"
                    isRequired
                    value={pw}
                    onChangeText={(pw) => setPw(pw)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                    placeholder='At least 6 charcters'
                    secureTextEntry={true}
                    customValidation={() => {
                        return {
                            status: pw.length > 5,
                            message: 'Please input at least 6 charcters'
                        }
                    }}
                />

                <FormItem
                    label="Password"
                    isRequired
                    value={pw2}
                    onChangeText={(pw2) => setPw2(pw2)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                    placeholder='Please enter same password again'
                    secureTextEntry={true}
                    customValidation={() => {
                        return {
                            status: pw == pw2,
                            message: 'Please input same passwoed twice'
                        }
                    }}
                />
                <Text style={{
                    color: '#f00',
                    marginLeft: 40
                }}>{message}</Text>
            </Form>


        </ScrollView>

    )
}

function LoginOrRegScreen({ navigation }) {
    return (
        <View >
            <View style={{
                height: 250, justifyContent: 'center',
                alignItems: 'center', flexDirection: 'row'
            }}>

                <Icon name='mail-outline' style={{
                    fontSize: 120,
                    color: '#4e4e4e'
                }}></Icon>
                <Icon name='multiple-stop' style={{
                    fontSize: 50,
                    color: '#4e4e4e'
                }}></Icon>
                <Icon name='lock-open' style={{
                    fontSize: 60,
                    color: '#4e4e4e'
                }}></Icon>




            </View>

            <View style={{ justifyContent: "center", alignItems: 'center' }}>

                <TouchableOpacity
                    style={styles.loginOrRegBtn}
                    onPress={() => {
                        navigation.navigate('Login')
                    }}
                >
                    <Text style={{
                        fontSize: 24,
                        color: '#fff',
                        fontWeight: '600'
                    }}>Login by Email</Text>
                </TouchableOpacity>
                <View style={{ height: 40 }}></View>
                <TouchableOpacity
                    style={styles.loginOrRegBtnCreate}
                    onPress={() => {
                        navigation.navigate('Register')
                    }}
                >
                    <Text style={{
                        fontSize: 24,
                        color: '#70c14d',
                        fontWeight: '600'
                    }}>Create new account</Text>
                </TouchableOpacity>

            </View>
        </View>
    )
}

function DetailsScreen({ route, navigation }) {
    const { itemId } = route.params
    // const postData = testDataArr.filter((elem) => elem.id == itemId);
    const data = useSelector((state: RootState) => state.postdata)
    const bookmarked = useSelector((state: RootState) => state.bookmarkedByUser)
    const dispatch = useDispatch()
    const idx = data.indexOf(data.filter((elem) => elem.job_id == itemId)[0])
    // const postData = data.filter((elem) => elem.id == itemId);
    // console.warn(idx)
    const token = useSelector((state: RootState) => state.userToken)

    const [finalToken, setFinalToken] = useState(token)

    useEffect(() => {
        setFinalToken(token)
    }, [token])

    return (
        <ScrollView >

            {data.filter((elem) => elem.job_id == itemId).map((elem) => {
                return post(elem, idx, dispatch, bookmarked, navigation, finalToken)
            })}

        </ScrollView>
    )
}



function BookmarkScreen({ navigation }) {
    const data = useSelector((state: RootState) => state.postdata)
    const bookmarked = useSelector((state: RootState) => state.bookmarkedByUser)
    const [bookmarkeddata, setBookmarkeddata] = useState(data.filter((e, idx) => bookmarked[idx]))
    const token = useSelector((state: RootState) => state.userToken)

    const [finalToken, setFinalToken] = useState(token)

    useEffect(() => {
        setFinalToken(token)
    }, [token])

    useEffect(() => {
        setBookmarkeddata(data.filter((e, idx) => bookmarked[idx]))
    }, [bookmarked])

    const dispatch = useDispatch()

    return (


        <ScrollView
            style={
                { backgroundColor: "#fff" }
            }
            contentContainerStyle={{
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.25,
                shadowRadius: 6.84,


            }}>
            <Text style={styles.boxSubTitle}>{bookmarkeddata.length > 1 ? "Bookmarked Jobs" : "Bookmarked Job"} : {bookmarkeddata.length}</Text>
            {/* {data.map((elem) => {
                return bookmarkBox(elem, navigation)
            })} */}
            {data.map((elem, idx) => {
                if (bookmarked[idx]) {

                    return bookmarkBox(elem, idx, navigation, dispatch, bookmarked, finalToken)
                }
            })}

        </ScrollView>

    )
}

function HistoryScreen({ navigation }) {
    const data = useSelector((state: RootState) => state.postdata)
    const isLoggedIn = useSelector((state: RootState) => state.isLoggedIn)
    const token = useSelector((state: RootState) => state.userToken)
    const historyIdArr = useSelector((state: RootState) => state.history)

    const [finalToken, setFinalToken] = useState(token)

    const [historyId, setHistoryId] = useState([])


    // const dispatch = useDispatch()
    useEffect(() => {
        setFinalToken(token)
    }, [token])

    const getHistory = async () => {
        if (isLoggedIn) {

            try {
                const response = await fetch(
                    `${server}/api/users/history`,
                    {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        },

                    }
                );
                const json = await response.json();

                setHistoryId(json.history.map((e: any) => e.jobs_id))



            } catch (error) {
                console.error(error);
            }
        }
    };

    useEffect(() => {
        getHistory();

    }, [historyIdArr]);




    return (


        <ScrollView
            style={
                { backgroundColor: "#fff" }
            }
            contentContainerStyle={{
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.25,
                shadowRadius: 6.84,


            }}>


            {/* {data.filter((e) => historyId.includes(e.id)).map((elem) => {
                return historyBox(elem, navigation, finalToken)
            })} */}
            {historyId.map((e) => {
                return data.filter((elem) => elem.job_id == e)[0]
            }).map((elem) => {
                return historyBox(elem, navigation, finalToken)
            })}



        </ScrollView>

    )
}

function AppliedScreen({ navigation }) {
    const data = useSelector((state: RootState) => state.postdata)
    const token = useSelector((state: RootState) => state.userToken)
    const userprofile = useSelector((state: RootState) => state.userFormData)
    const [appliedJobs, setAppliedJobs] = useState([])

    const getUserProfile = async () => {
        try {
            const response = await fetch(
                `${server}/api/jobs/getAppliedJob`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },

                }
            );
            const json = await response.json();

            setAppliedJobs(json.result.map((e:any)=>e.job_id))
            
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        getUserProfile();

    }, [userprofile]);

    return (


        <ScrollView
            style={
                { backgroundColor: "#fff" }
            }
            contentContainerStyle={{
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.25,
                shadowRadius: 6.84,


            }}>
            <Text style={styles.boxSubTitle}>{data.length > 1 ? "Applied Jobs" : "Applied Job"} : {appliedJobs.length}</Text>
            

            {/* {data.filter((e)=>appliedJobs.includes(e.job_id)).map((elem)=>{
                return appliedBox(elem, navigation)
            })} */}
            {appliedJobs.map((e)=>{
                return appliedBox(data.find((elem)=>elem.job_id==e)!!, navigation)
                
            })}

        </ScrollView>

    )
}

function ProfileScreen({ route, navigation }) {
    const formData = useSelector((state: RootState) => state.userFormData)
    const isLoggedIn = useSelector((state: RootState) => state.isLoggedIn)
    const token = useSelector((state: RootState) => state.userToken)

    useEffect(() => {
        setLastName(formData.lastName)
        setFirstName(formData.firstName)
        setBirth(formData.birth)
        setGender(genderNum.indexOf(formData.gender))
        setTel(formData.tel)
        setEmail(formData.email)
        setAddress(formData.address)
        setCurrentOccupation(formData.currentJob)
        setAboutMe(formData.aboutMe)
        setWorkExp(freshGradNum.indexOf(formData.workExp))
        setEducation(formData.education)
        setSchoolName(formData.schoolName)
        setMajor(formData.major)
        setGradYear(formData.gradYear)
        setContactTime(contactTimeNum.indexOf(formData.contactTime))
    }, [formData])


    const [lastName, setLastName] = useState(formData.lastName)
    const [firstName, setFirstName] = useState(formData.firstName)
    const [birth, setBirth] = useState(formData.birth)
    const genderNum = ['Others', 'Male', 'Female']
    const [gender, setGender] = useState(genderNum.indexOf(formData.gender))
    const [tel, setTel] = useState(formData.tel)
    const [email, setEmail] = useState(formData.email)
    const [address, setAddress] = useState(formData.address)
    const currentOccupationArr = ['', 'Student', 'Freelance', 'Part-Time', 'Full-Time', 'Unemployed', 'Others']
    const [currentOccupation, setCurrentOccupation] = useState<string>(formData.currentJob)
    const [aboutMe, setAboutMe] = useState(formData.aboutMe)
    const freshGradNum = [null, true, false]
    const [workExp, setWorkExp] = useState(freshGradNum.indexOf(formData.workExp))
    const educationArr = ['', 'High School', 'Diploma', 'Higher-Diploma', 'Degree', 'Master or abrove', 'Others']
    const [education, setEducation] = useState<string>(formData.education)
    const [schoolName, setSchoolName] = useState(formData.schoolName)
    const [major, setMajor] = useState(formData.major)
    const [gradYear, setGradYear] = useState(formData.gradYear)
    const contactTimeNum = ['', 'Whole Day', 'Morning', 'Afternoon']
    const [contactTime, setContactTime] = useState(contactTimeNum.indexOf(formData.contactTime))


    const dispatch = useDispatch()

    const getUserProfile = async () => {
        try {
            const response = await fetch(
                `${server}/api/users/userProfile`,
                {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },

                }
            );
            const json = await response.json();

            if(json.result){

                dispatch(updateUserDataForm({
                    lastName:json.result.last_name,
                    firstName:json.result.first_name,
                    birth:json.result.birthday,
                    gender: json.result.gender,
                    tel:json.result.mobile,
                    email:json.result.email,
                    address:json.result.address,
                    currentJob: json.result.occupation,
                    aboutMe:json.result.descriptions,
                    workExp: json.result.work_exp,
                    education:json.result.education_level,
                    schoolName:json.result.school_name,
                    major:json.result.program_name,
                    gradYear:json.result.grad_year,
                    contactTime: json.result.contact_time
                    
                }))
                // console.warn(json)
                setLastName(json.result.last_name)
                setFirstName(json.result.first_name)
                setBirth(json.result.birthday)
                setGender(genderNum.indexOf(json.result.gender))
                setTel(json.result.mobile)
                setEmail(json.result.email)
                setAddress(json.result.address)
                setCurrentOccupation(json.result.occupation)
                setAboutMe(json.result.descriptions)
                setWorkExp(freshGradNum.indexOf(json.result.work_exp))
                setEducation(json.result.education_level)
                setSchoolName(json.result.school_name)
                setMajor(json.result.program_name)
                setGradYear(json.result.grad_year)
                setContactTime(contactTimeNum.indexOf(json.result.contact_time))
            }

        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        if(isLoggedIn){

            getUserProfile();
        }

    }, [token]);



    const maleColorList = ["#fff", "#210000", "#fff"]
    const femaleColorList = ["#fff", "#fff", "#210000"]
    const maleFontColorList = ["#888", "#210000", "#888"]
    const femaleFontColorList = ["#888", "#888", "#210000"]

    const amColorList = ["#fff", "#210000", "#fff", "#fff"]
    const pmColorList = ["#fff", "#fff", "#210000", "#fff"]
    const allDayColorList = ["#fff", "#fff", "#fff", "#210000"]
    const amFontColorList = ["#888", "#210000", "#888", "#888"]
    const pmFontColorList = ["#888", "#888", "#210000", "#888"]
    const allDayFontColorList = ["#888", "#888", "#888", "#210000"]
    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ height: 40 }}></View>
            <Form
                onButtonPress={() => {
                    dispatch(updateUserDataForm({
                        lastName,
                        firstName,
                        birth,
                        gender: genderNum[gender],
                        tel,
                        email,
                        address,
                        currentJob: currentOccupation,
                        aboutMe,
                        workExp: freshGradNum[workExp],
                        education,
                        schoolName,
                        major,
                        gradYear,
                        contactTime: contactTimeNum[contactTime]

                    }))
                    if (isLoggedIn) {

                        Alert.alert(
                            "Success",
                            "Update success!",
                            [

                                { text: "OK", onPress: () => navigation.navigate('Jobilly') }
                            ]
                        );
                    } else {
                        navigation.navigate('LoginOrReg')
                    }
                }}


                buttonStyle={styles.formSubmit}
                buttonText="Update Profile"
            >
                <FormItem
                    label="Last Name"
                    isRequired
                    value={lastName}
                    onChangeText={(lastName) => setLastName(lastName)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />
                <FormItem
                    label="First Name"
                    isRequired
                    value={firstName}
                    onChangeText={(firstName) => setFirstName(firstName)}
                    labelStyle={styles.label}

                    style={styles.formInput}
                />
                <FormItem
                    label="Birth"
                    isRequired
                    value={birth}
                    onChangeText={(birth) => setBirth(birth)}
                    labelStyle={styles.label}
                    placeholder='dd/mm/yyyy'
                    style={styles.formInput}
                />

                <View style={{
                    height: 100,
                    flexDirection: "row"
                }}>
                    <Text style={{
                        width: "20%",
                        height: 80,
                        marginTop: 15,
                        marginLeft: "5%",
                        fontWeight: "700"
                    }}>Gender</Text>

                    <TouchableOpacity
                        onPress={() => setGender(() => gender == 1 ? 0 : 1)}
                        style={{
                            width: "20%",
                            height: 50,
                            backgroundColor: "#fff",
                            borderColor: maleColorList[gender],
                            borderStyle: "solid",
                            borderWidth: 1,
                            marginLeft: "10%",
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <Text style={{ color: maleFontColorList[gender], fontSize: 15 }}>Male</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => setGender(() => gender == 2 ? 0 : 2)}
                        style={{
                            width: "20%",
                            height: 50,
                            backgroundColor: "#fff",
                            borderColor: femaleColorList[gender],
                            borderStyle: "solid",
                            borderWidth: 1,
                            marginLeft: "10%",
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <Text style={{ color: femaleFontColorList[gender], fontSize: 15 }}>Female</Text>
                    </TouchableOpacity>
                </View>

                <FormItem
                    label="Tel"
                    isRequired
                    value={tel}
                    onChangeText={(tel) => setTel(tel)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />
                <FormItem
                    label="Email"
                    isRequired
                    value={email}
                    onChangeText={(email) => setEmail(email)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />
                <FormItem
                    label="Address"
                    isRequired
                    value={address}
                    onChangeText={(address) => setAddress(address)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />
                <Picker
                    items={[
                        { label: 'Student', value: 1 },
                        { label: 'Freelance', value: 2 },
                        { label: 'Part-Time', value: 3 },
                        { label: 'Full-Time', value: 4 },
                        { label: 'Unemployed', value: 5 },
                        { label: 'Others', value: 6 },
                    ]}
                    label="Current Job"
                    selectedValue={currentOccupationArr.indexOf(currentOccupation)}
                    onSelection={(item) => setCurrentOccupation(item["label"])}
                    buttonStyle={styles.formInput}
                    labelStyle={styles.label}
                    placeholder='Please select'
                />
                <FormItem
                    label="About Me"
                    isRequired
                    value={aboutMe}
                    onChangeText={(aboutMe) => setAboutMe(aboutMe)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />

                <View style={{
                    height: 100,
                    flexDirection: "row"
                }}>
                    <Text style={{
                        width: "20%",
                        height: 80,
                        marginTop: 15,
                        marginLeft: "5%",
                        fontWeight: "700",
                        fontSize: 13
                    }}>Work Experience</Text>

                    <TouchableOpacity
                        onPress={() => setWorkExp(() => workExp == 1 ? 0 : 1)}
                        style={{
                            width: "20%",
                            height: 50,
                            backgroundColor: "#fff",
                            borderColor: maleColorList[workExp],
                            borderStyle: "solid",
                            borderWidth: 1,
                            marginLeft: "10%",
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <Text style={{ color: maleFontColorList[workExp], fontSize: 15 }}>Yes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => setWorkExp(() => workExp == 2 ? 0 : 2)}
                        style={{
                            width: "20%",
                            height: 50,
                            backgroundColor: "#fff",
                            borderColor: femaleColorList[workExp],
                            borderStyle: "solid",
                            borderWidth: 1,
                            marginLeft: "10%",
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <Text style={{ color: femaleFontColorList[workExp], fontSize: 15 }}>No</Text>
                    </TouchableOpacity>
                </View>

                <Picker
                    items={[
                        { label: 'High School', value: 1 },
                        { label: 'Diploma', value: 2 },
                        { label: 'Higher-Diploma', value: 3 },
                        { label: 'Degree', value: 4 },
                        { label: 'Master or abrove', value: 5 },
                        { label: 'Others', value: 6 },
                    ]}
                    label="Education"
                    selectedValue={educationArr.indexOf(education)}
                    onSelection={(item) => setEducation(item["label"])}
                    buttonStyle={styles.formInput}
                    labelStyle={styles.label}
                    placeholder='Please select'
                />
                <FormItem
                    label="School Name"
                    isRequired
                    value={schoolName}
                    onChangeText={(schoolName) => setSchoolName(schoolName)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />
                <FormItem
                    label="Major"
                    isRequired
                    value={major}
                    onChangeText={(major) => setMajor(major)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />
                <FormItem
                    label="Grad Year"
                    isRequired
                    value={gradYear}
                    onChangeText={(gradYear) => setGradYear(gradYear)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                />
                <View style={{
                    height: 100,
                    flexDirection: "row"
                }}>
                    <Text style={{
                        width: "20%",
                        height: 80,
                        marginTop: 15,
                        marginLeft: "5%",
                        fontWeight: "700"
                    }}>Contact Time</Text>

                    <TouchableOpacity
                        onPress={() => setContactTime(() => contactTime == 1 ? 0 : 1)}
                        style={{
                            width: "18%",
                            height: 50,
                            backgroundColor: "#fff",
                            borderColor: amColorList[contactTime],
                            borderStyle: "solid",
                            borderWidth: 1,
                            marginLeft: "5%",
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <Text style={{ color: amFontColorList[contactTime], fontSize: 15 }}>a.m.</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => setContactTime(() => contactTime == 2 ? 0 : 2)}
                        style={{
                            width: "18%",
                            height: 50,
                            backgroundColor: "#fff",
                            borderColor: pmColorList[contactTime],
                            borderStyle: "solid",
                            borderWidth: 1,
                            marginLeft: "5%",
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <Text style={{ color: pmFontColorList[contactTime], fontSize: 15 }}>p.m.</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => setContactTime(() => contactTime == 3 ? 0 : 3)}
                        style={{
                            width: "18%",
                            height: 50,
                            backgroundColor: "#fff",
                            borderColor: allDayColorList[contactTime],
                            borderStyle: "solid",
                            borderWidth: 1,
                            marginLeft: "5%",
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <Text style={{ color: allDayFontColorList[contactTime], fontSize: 15 }}>All Day</Text>
                    </TouchableOpacity>
                </View>


            </Form>
        </ScrollView>
    )
}

const Menubar = ({ navigation }) => {
    const isLoggedIn = useSelector((state: RootState) => state.isLoggedIn)
    const dispatch = useDispatch()
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'Jobilly') {
                        iconName = focused
                            ? 'home'
                            : 'home';
                    } else if (route.name === 'Bookmark') {
                        iconName = 'bookmarks'
                    } else if (route.name === 'History') {
                        iconName = 'history'
                    } else if (route.name === 'Applied') {
                        iconName = 'history-edu'
                    } else if (route.name === 'Profile') {
                        iconName = 'emoji-people'
                    }

                    // You can return any component that you like here!
                    return <Icon name={iconName} style={styles.menuBarIcon} />
                },
                tabBarActiveTintColor: 'tomato',
                tabBarInactiveTintColor: 'gray',
            })}>
            <Tab.Screen
                name="Jobilly"
                component={HomeScreen}
                options={
                    {
                        title: 'Jobilly',
                        headerStyle: {
                            backgroundColor: '#1e5ece',
                        },
                        headerTintColor: '#eee',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },
                        headerRight: () => (
                            <TouchableOpacity
                                style={{
                                    width: 30,
                                    height: 30
                                }}
                                onPress={() => {
                                    if (isLoggedIn) {
                                        dispatch(userLogout())
                                    } else {

                                        navigation.navigate('LoginOrReg')
                                    }
                                }}
                            >

                                {/* <Icon name={isLoggedIn ? 'logout' : 'login'} style={{ fontSize: 24, color: "#eee" }} /> */}
                                <Text style={{
                                    position: 'absolute',
                                    right: 20,
                                    top: 5,
                                    width: 50,
                                    color: '#fff'
                                }}>{isLoggedIn ? 'Logout' : 'Login'}</Text>
                            </TouchableOpacity>
                        )
                    }
                } />
            <Tab.Screen
                name="History"
                component={isLoggedIn ? HistoryScreen : LoginOrRegScreen}
                options={
                    {
                        title: 'History',
                        headerStyle: {
                            backgroundColor: '#1e5ece',
                        },
                        headerTintColor: '#eee',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },

                    }
                } />
            <Tab.Screen
                name="Bookmark"
                component={isLoggedIn ? BookmarkScreen : LoginOrRegScreen}
                options={
                    {
                        title: 'Bookmark',
                        headerStyle: {
                            backgroundColor: '#1e5ece',
                        },
                        headerTintColor: '#eee',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },

                    }
                } />

            <Tab.Screen
                name="Applied"
                component={isLoggedIn ? AppliedScreen : LoginOrRegScreen}
                options={
                    {
                        title: 'Applied',
                        headerStyle: {
                            backgroundColor: '#1e5ece',
                        },
                        headerTintColor: '#eee',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },

                    }
                } />
            <Tab.Screen
                name="Profile"
                component={ProfileScreen}
                options={
                    {
                        title: 'Profile',
                        headerStyle: {
                            backgroundColor: '#fff',
                        },
                        headerTintColor: '#222',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },

                    }
                } />
        </Tab.Navigator>

    )
}






const App = () => {
    useEffect(() => {
        SplashScreen.hide()
    }, [])
    return (
        <Provider store={store}>

            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen
                        name="Menubar"
                        component={Menubar}
                        options={{ headerShown: false }}
                    />


                    <Stack.Screen
                        name="Details"
                        component={DetailsScreen}
                        options={
                            {
                                title: 'Jobilly',
                                headerStyle: {
                                    backgroundColor: '#fff',
                                },
                                headerTintColor: '#000',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },

                            }
                        }
                    />
                    <Stack.Screen
                        name="LoginOrReg"
                        component={LoginOrRegScreen}
                        options={
                            {
                                title: 'Login/Register',
                                headerStyle: {
                                    backgroundColor: '#fff',
                                },
                                headerTintColor: '#000',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },

                            }
                        }
                    />
                    <Stack.Screen
                        name="Login"
                        component={LoginScreen}
                        options={
                            {
                                title: 'Login',
                                headerStyle: {
                                    backgroundColor: '#fff',
                                },
                                headerTintColor: '#000',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },

                            }
                        }
                    />
                    <Stack.Screen
                        name="Register"
                        component={RegisterScreen}
                        options={
                            {
                                title: 'Register',
                                headerStyle: {
                                    backgroundColor: '#fff',
                                },
                                headerTintColor: '#000',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },

                            }
                        }
                    />
                    <Stack.Screen
                        name="Apply Page"
                        component={ApplyScreen}
                        options={
                            {
                                title: 'Apply',
                                headerStyle: {
                                    backgroundColor: '#fff',
                                },
                                headerTintColor: '#000',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },

                            }
                        }
                    />




                </Stack.Navigator>

            </NavigationContainer>
        </Provider>

    )
}
const styles = StyleSheet.create({
    greyText: {
        fontSize: 14,
        color: "#898989",
        marginTop: 10,
        marginLeft: 8,
        width: "90%"
    },
    greySubText: {
        fontSize: 14,
        color: "#898989",
        marginTop: 10,
        marginLeft: 4,
        width: "90%"
    },
    boxIcon: {
        fontSize: 14,
        color: "#898989",
        marginTop: 11,
        marginLeft: 12
    },

    greyTextPost: {
        fontSize: 14,
        color: "#666",
        marginTop: 20,
        marginLeft: 20
    },
    greyTextDetail: {
        fontSize: 14,
        color: "#444",
        marginVertical: 10,
        marginHorizontal: 20,
        lineHeight: 22
    },
    boxTitlePost: {
        fontSize: 28,
        marginTop: 5,
        marginLeft: 20
    },
    boxTitle: {
        fontSize: 28,
        marginTop: 10,
        marginLeft: 12
    },
    bookmarkedBoxTitle: {
        fontSize: 18,
        marginTop: 10,
        marginLeft: 12,
        width: "60%"
    },
    boxSubTitle: {
        fontSize: 17,
        marginTop: 10,
        marginLeft: 20
    },
    formInput: {
        width: "65%",
        marginLeft: "31%",
        marginTop: -30
    },
    label: {
        width: "30%",
        height: 20,
        marginTop: 0,
        marginLeft: "5%",
        fontSize: 13
    },
    formSubmit: {
        backgroundColor: "#70c14d",
        width: "80%",
        borderRadius: 30,
        marginHorizontal: "10%"
    },
    bookmarkIcon: {

        fontSize: 32,
        opacity: 0.6,
        color: '#ddd',
        marginTop: 1,
        marginLeft: 2
    },
    bookmarkIconClicked: {

        fontSize: 32,
        opacity: 1,
        color: '#e7e7e7',
        marginTop: 1,
        marginLeft: 2
    },
    postBookmarkIcon: {

        fontSize: 32,
        opacity: 1,
        color: '#222',
        marginTop: 1,
        marginLeft: 2
    },
    postBookmarkIconClick: {

        fontSize: 32,
        opacity: 1,
        color: '#d6af5a',
        marginTop: 1,
        marginLeft: 2
    },
    bookmarkBtn: {
        position: "absolute",
        top: 12,
        right: 12,
        height: 42,
        width: 42,
        backgroundColor: "#00000055",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 21

    },
    postBookmarkBtn: {
        width: "28%",
        height: 60,
        backgroundColor: "#fff",
        borderStyle: "solid",
        borderColor: "#aaa",
        borderWidth: 1,
        marginLeft: "4%",

        borderRadius: 30,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",

    },
    postBookmarkBtnClicked: {
        width: "28%",
        height: 60,
        backgroundColor: "#fff",
        borderStyle: "solid",
        borderColor: "#d6af5a",
        borderWidth: 1,
        marginLeft: "4%",

        borderRadius: 30,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",

    },
    bookmarkBtnClicked: {
        position: "absolute",
        top: 12,
        right: 12,
        height: 42,
        width: 42,
        backgroundColor: "#d6af5a",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 21

    },
    menuBarIcon: {
        fontSize: 26,
        color: "#898989",
        marginTop: 6

    },
    loginOrRegBtn: {
        backgroundColor: "#70c14d",
        width: "80%",
        height: 60,
        borderRadius: 30,
        marginHorizontal: "10%",
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginOrRegBtnCreate: {
        backgroundColor: "#fff",
        width: "80%",
        height: 60,
        borderRadius: 30,
        borderWidth: 3,
        borderColor: "#70c14d",
        marginHorizontal: "10%",
        justifyContent: 'center',
        alignItems: 'center'
    },
    barContainer: {
        flexDirection: 'row',
        marginTop: 20,
        marginHorizontal: '7%',
        justifyContent: 'space-between',
        alignItems: 'center'

    },
    barText: {
        width: 84,

    },
    barTextLeft: {
        width: 84,
        textAlign: 'right'
    },
    barStyle: {
        fontSize: 16,
        color: "#444",
        marginVertical: 20,
        marginHorizontal: '3%',
        lineHeight: 22,
        letterSpacing: -2,
    }


});

export default App;



