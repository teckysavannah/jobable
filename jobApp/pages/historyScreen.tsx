import React, { useEffect, useState } from "react";
import { Alert, Image, RefreshControl, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { initBookmarked, initHistory, updateAppliedJobs, updateBookmarked, updateJob, userLoigin } from "../actions";
import { server } from "../App";
import { styles } from "../css";
import { RootState } from "../store";
import { JobData } from "../type";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Form, FormItem } from "react-native-form-component";
import axios from "axios";

function HistoryScreen({ navigation }) {
    const data = useSelector((state: RootState) => state.postdata)
    const isLoggedIn = useSelector((state: RootState) => state.isLoggedIn)
    const token = useSelector((state: RootState) => state.userToken)
    const historyIdArr = useSelector((state: RootState) => state.history)
    const appliedJobs = useSelector((state: RootState) => state.appliedJobs)

    const [finalToken, setFinalToken] = useState(token)

    const [historyId, setHistoryId] = useState([])


    // const dispatch = useDispatch()
    useEffect(() => {
        setFinalToken(token)
    }, [token])

    const getHistory = async () => {
        if (isLoggedIn) {

            try {
                const response = await fetch(
                    `${server}/api/users/history`,
                    {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        },

                    }
                );
                const json = await response.json();

                setHistoryId(json.history.map((e: any) => e.jobs_id))



            } catch (error) {
                console.error(error);
            }
        }
    };

    useEffect(() => {
        getHistory();

    }, [historyIdArr]);




    return (


        <ScrollView
            style={
                { backgroundColor: "#fff" }
            }
            contentContainerStyle={{
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.25,
                shadowRadius: 6.84,


            }}>


            {/* {data.filter((e) => historyId.includes(e.id)).map((elem) => {
                return historyBox(elem, navigation, finalToken)
            })} */}
            {historyId.map((e) => {
                return data.filter((elem) => elem.job_id == e)[0]
            }).map((elem) => {
                return historyBox(elem, navigation, finalToken, appliedJobs)
            })}



        </ScrollView>

    )
}

function historyBox(elem: JobData, navi: any, token: string, appliedJobs: any[]) {
    let jobsId: number = elem.job_id
    return (
        <TouchableOpacity style={{
            width: "94%",
            height: 300,
            backgroundColor: "#fdfdfd",
            marginTop: 30,
            marginLeft: "3%",
            borderRadius: 16,

            overflow: "hidden",
        }} key={elem.job_id} onPress={() => {
            navi.navigate('Details', {
                itemId: elem.job_id
            })
        }}>
            <View style={{ height: 40, flexDirection: "row", justifyContent: "space-between" }}>
                <Text></Text>

            </View>
            <View style={{ flexDirection: "row", height: 80, width: "100%", }}>

                <Image style={{ width: "33%", height: "100%", resizeMode: 'cover', marginLeft: 12, borderRadius: 7 }} source={{ uri: elem.imageurl[0] }} />
                <Text style={styles.bookmarkedBoxTitle}>{elem.title}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="work" style={styles.boxIcon} />
                <Text style={styles.greyText}>{elem.comapany_name}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="location-on" style={styles.boxIcon} />
                <Text style={styles.greySubText}>{elem.address}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="attach-money" style={styles.boxIcon} />
                <Text style={styles.greySubText}>{elem.salary_project}</Text>
            </View>
            <View style={{ height: 30, borderStyle: "solid", borderBottomColor: "#ccc", borderBottomWidth: 1, width: "90%", marginLeft: "5%" }}></View>
            {!appliedJobs.includes(jobsId) && <TouchableOpacity style={{
                width: "80%",
                height: 30,
                backgroundColor: "#72c91a",
                marginTop: 15,
                marginLeft: "10%",
                borderRadius: 30,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
            }}
                onPress={() => {
                    navi.navigate('Apply Page', {
                        itemId: elem.job_id
                    })
                }}>
                <Text style={{
                    fontSize: 16,
                    color: "#fff",
                    fontWeight: "500",
                }}>Apply</Text>
            </TouchableOpacity>}
            {appliedJobs.includes(jobsId) && <View style={{
                width: "80%",
                height: 30,
                backgroundColor: "#777",
                marginTop: 15,
                marginLeft: "10%",
                borderRadius: 30,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
            }}
            >
                <Text style={{
                    fontSize: 16,
                    color: "#fff",
                    fontWeight: "500",
                }}>Applied</Text>
            </View>}

        </TouchableOpacity>
    )
}

export default HistoryScreen