import React, { useEffect, useState } from "react";
import { Image, RefreshControl, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { initBookmarked, initHistory, updateAppliedJobs, updateBookmarked, updateJob } from "../actions";
import { server } from "../App";
import { styles } from "../css";
import { RootState } from "../store";
import { JobData } from "../type";
import Icon from 'react-native-vector-icons/MaterialIcons';


function HomeScreen({ navigation }) {

    // const [data, setData] = useState([]);
    // const [bookmarked, setBookmarked] = useState([])
    const token = useSelector((state: RootState) => state.userToken)
    const [refreshing, setRefreshing] = React.useState(false);

    const wait = (timeout: any) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getJobsFromApiAsync()
        getAppliedJobs()
        wait(2000).then(() => setRefreshing(false));
    }, [token]);

    const dispatch = useDispatch()

    const data = useSelector((state: RootState) => state.postdata)
    const bookmarked = useSelector((state: RootState) => state.bookmarkedByUser)
    const appliedJobs = useSelector((state: RootState) => state.appliedJobs)


    const [finalToken, setFinalToken] = useState(token)

    useEffect(() => {
        setFinalToken(token)
    }, [token])

    const testnum = Math.random() * 10

    const getJobsFromApiAsync = async () => {
        try {
            // console.warn(token)
            const response = await fetch(
                `${server}/api/jobs/allJobs`,
                {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },

                }
            );
            const json = await response.json();
            dispatch(updateJob(json));
            dispatch(initBookmarked(json))


        } catch (error) {
            console.error(error);
        }
    };
    const getAppliedJobs = async () => {
        if (token != '') {

            try {
                const response = await fetch(
                    `${server}/api/jobs/getAppliedJob`,
                    {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        },

                    }
                );
                const json = await response.json();

                // setAppliedJobs(json.result.map((e: any) => e.job_id))
                dispatch(updateAppliedJobs(json.result.map((e: any) => e.job_id)))
            } catch (error) {
                console.error(error);
            }
        }
    };


    useEffect(() => {
        getJobsFromApiAsync();
        getAppliedJobs();

    }, [token]);


    return (


        <ScrollView
            style={
                { backgroundColor: "#fff" }
            }
            contentContainerStyle={{
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.25,
                shadowRadius: 6.84,


            }}

            refreshControl={
                <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            }
        >
            {/* <Text>
                Try editing me! 🎉
            </Text>
            <Button
                title="go to details?"
                onPress={() => navigation.navigate('Details')}
            /> */}


            {/* <TouchableOpacity onPress={()=>console.warn(data[0].bookmarked)}>
                <Text>hihi</Text>
            </TouchableOpacity> */}



            {data.map((elem, idx) => {   //alt  testDataArr
                return box(elem, idx, navigation, dispatch, bookmarked, finalToken, testnum)
            })}

            <View style={{ height: 34 }}></View>
        </ScrollView>

    )
}


function box(elem: JobData, idx: number, navi: any, cb: any, bookmark: [], token: string, testnum: number) {
    let bookmarkedArr:any = [...bookmark]
    // let data = {
    //     headers : {
    //         Authorization: `Bearer ${token}`
    //     },

    //     jobsId:elem.id

    // }
    let jobsId: number = elem.job_id
    return (
        <View style={{
            width: "94%",
            height: 400,
            backgroundColor: "#fafafa",
            marginTop: 30,
            marginLeft: "3%",
            borderRadius: 16,
            borderStyle: 'solid',
            borderWidth: 2,
            borderColor: "#fff",
            overflow: "hidden",

        }} key={elem.job_id} >

            <ScrollView nestedScrollEnabled={true} horizontal={true} showsHorizontalScrollIndicator={false}>
                <View style={{ height: 420, width: (elem.imageurl.length * 406) - 6, flexDirection: 'row' }}>
                    {elem.imageurl.map((e: string, idx: number) => boxImg(e, idx))}

                </View>
                {/* <Image style={{ width: "100%", height: "55%", resizeMode: 'cover' }} source={{ uri: elem.imageurl[0] }} /> */}

            </ScrollView>
            <TouchableOpacity style={bookmarkedArr[idx] ? styles.bookmarkBtnClicked : styles.bookmarkBtn} onPress={async () => {

                if (bookmarkedArr[idx]) {
                    bookmarkedArr[idx] = false
                } else {
                    bookmarkedArr[idx] = true
                }

                cb(updateBookmarked(bookmarkedArr))


                try {
                    const res = await fetch(`${server}/api/users/favourite`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        },
                        body: JSON.stringify({
                            jobsId: jobsId
                        })

                    })



                } catch (error) {

                    if (error?.response.status == 401) {
                        console.warn('bookmark error')
                    }

                }







            }}>

                <Icon name={bookmarkedArr[idx] ? 'bookmark' : 'bookmark-border'}
                    style={bookmarkedArr[idx] ? styles.bookmarkIconClicked : styles.bookmarkIcon}
                />
            </TouchableOpacity>

            <TouchableOpacity style={{ position: 'absolute', top: 240 }} onPress={async () => {

                try {
                    const res = await fetch(`${server}/api/users/history`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        },
                        body: JSON.stringify({
                            jobsId: jobsId
                        })

                    })




                } catch (error) {

                    if (error?.response.status == 401) {
                        console.warn('bookmark error')
                    }

                }

                cb(initHistory(elem.job_id))
                navi.navigate('Details', {
                    itemId: elem.job_id
                })
            }}>

                <View style={{ flexDirection: "row" }}>
                    <Icon name="work" style={styles.boxIcon} />
                    <Text style={styles.greyText}>{elem.comapany_name}</Text>
                </View>
                <View style={{ flexDirection: "row" }}>

                    <Text style={styles.boxTitle}>{elem.title}</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                    <Icon name="location-on" style={styles.boxIcon} />
                    <Text style={styles.greySubText}>{elem.address}</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                    <Icon name="attach-money" style={styles.boxIcon} />
                    <Text style={styles.greySubText}>{elem.salary_project}</Text>
                </View>
            </TouchableOpacity>




        </View>
    )
}

function boxImg(imageurl: string, idx: number) {
    return (
        <Image style={{ width: 400, height: "55%", resizeMode: 'cover', marginEnd: 6 }} source={{ uri: imageurl }} key={idx} />
    )
}

export default HomeScreen