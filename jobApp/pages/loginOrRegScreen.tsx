import React, { useEffect, useState } from "react";
import { Image, RefreshControl, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { initBookmarked, initHistory, updateAppliedJobs, updateBookmarked, updateJob } from "../actions";
import { server } from "../App";
import { styles } from "../css";
import { RootState } from "../store";
import { JobData } from "../type";
import Icon from 'react-native-vector-icons/MaterialIcons';

function LoginOrRegScreen({ navigation }) {
    return (
        <View >
            <View style={{
                height: 250, justifyContent: 'center',
                alignItems: 'center', flexDirection: 'row'
            }}>

                <Icon name='mail-outline' style={{
                    fontSize: 120,
                    color: '#4e4e4e'
                }}></Icon>
                <Icon name='multiple-stop' style={{
                    fontSize: 50,
                    color: '#4e4e4e'
                }}></Icon>
                <Icon name='lock-open' style={{
                    fontSize: 60,
                    color: '#4e4e4e'
                }}></Icon>




            </View>

            <View style={{ justifyContent: "center", alignItems: 'center' }}>

                <TouchableOpacity
                    style={styles.loginOrRegBtn}
                    onPress={() => {
                        navigation.navigate('Login')
                    }}
                >
                    <Text style={{
                        fontSize: 24,
                        color: '#fff',
                        fontWeight: '600'
                    }}>Login by Email</Text>
                </TouchableOpacity>
                <View style={{ height: 40 }}></View>
                <TouchableOpacity
                    style={styles.loginOrRegBtnCreate}
                    onPress={() => {
                        navigation.navigate('Register')
                    }}
                >
                    <Text style={{
                        fontSize: 24,
                        color: '#70c14d',
                        fontWeight: '600'
                    }}>Create new account</Text>
                </TouchableOpacity>

            </View>
        </View>
    )
}


export default LoginOrRegScreen