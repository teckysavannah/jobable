import React, { useEffect, useState } from "react";
import { Alert, Image, RefreshControl, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { initBookmarked, initHistory, updateAppliedJobs, updateBookmarked, updateJob, userLoigin } from "../actions";
import { server } from "../App";
import { styles } from "../css";
import { RootState } from "../store";
import { JobData } from "../type";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Form, FormItem } from "react-native-form-component";
import axios from "axios";

function AppliedScreen({ navigation }) {
    const data = useSelector((state: RootState) => state.postdata)
    const token = useSelector((state: RootState) => state.userToken)
    const userprofile = useSelector((state: RootState) => state.userFormData)
    // const [appliedJobs, setAppliedJobs] = useState([])
    const appliedJobs = useSelector((state: RootState) => state.appliedJobs)

    const [refreshing, setRefreshing] = React.useState(false);

    const dispatch = useDispatch()

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getUserProfile()
        wait(2000).then(() => setRefreshing(false));
    }, []);

    const getUserProfile = async () => {
        try {
            const response = await fetch(
                `${server}/api/jobs/getAppliedJob`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },

                }
            );
            const json = await response.json();

            // setAppliedJobs(json.result.map((e: any) => e.job_id))
            dispatch(updateAppliedJobs(json.result.map((e: any) => e.job_id)))
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        getUserProfile();

    }, [userprofile]);

    return (


        <ScrollView
            style={
                { backgroundColor: "#fff" }
            }
            contentContainerStyle={{
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.25,
                shadowRadius: 6.84,


            }}
            refreshControl={
                <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            }
        >
            <Text style={styles.boxSubTitle}>{data.length > 1 ? "Applied Jobs" : "Applied Job"} : {appliedJobs.length}</Text>


            {/* {data.filter((e)=>appliedJobs.includes(e.job_id)).map((elem)=>{
                return appliedBox(elem, navigation)
            })} */}
            {appliedJobs.map((e) => {
                return appliedBox(data.find((elem) => elem.job_id == e)!!, navigation, dispatch, token)

            })}

        </ScrollView>

    )
}


function appliedBox(elem: JobData, navi: any, cb: any, token: string) {
    return (
        <TouchableOpacity style={{
            width: "94%",
            height: 250,
            backgroundColor: "#fdfdfd",
            marginTop: 30,
            marginLeft: "3%",
            borderRadius: 16,

            overflow: "hidden",
        }} key={elem.job_id} onPress={async () => {

            try {
                const res = await fetch(`${server}/api/users/history`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify({
                        jobsId: elem.job_id
                    })

                })




            } catch (error) {

                if (error?.response.status == 401) {
                    console.warn('bookmark error')
                }

            }

            cb(initHistory(elem.job_id))


            navi.navigate('Details', {
                itemId: elem.job_id
            })
        }}>
            <View style={{ height: 40, flexDirection: "row", justifyContent: "space-between" }}>
                <Text></Text>

            </View>
            <View style={{ flexDirection: "row", height: 80, width: "100%", }}>

                <Image style={{ width: "33%", height: "100%", resizeMode: 'cover', marginLeft: 12, borderRadius: 7 }} source={{ uri: elem.imageurl[0] }} />
                <Text style={styles.bookmarkedBoxTitle}>{elem.title}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="work" style={styles.boxIcon} />
                <Text style={styles.greyText}>{elem.comapany_name}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="location-on" style={styles.boxIcon} />
                <Text style={styles.greySubText}>{elem.address}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="attach-money" style={styles.boxIcon} />
                <Text style={styles.greySubText}>{elem.salary_project}</Text>
            </View>



        </TouchableOpacity>
    )
}

export default AppliedScreen