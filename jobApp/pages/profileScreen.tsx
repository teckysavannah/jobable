import React, { useEffect, useState } from "react";
import { Alert, Image, RefreshControl, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { initBookmarked, initHistory, updateAppliedJobs, updateBookmarked, updateJob, updateUserDataForm, userLoigin } from "../actions";
import { server } from "../App";
import { styles } from "../css";
import { RootState } from "../store";
import { JobData } from "../type";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Form, FormItem, Picker } from "react-native-form-component";
import axios from "axios";

function ProfileScreen({ navigation }) {

    const formData = useSelector((state: RootState) => state.userFormData)
    const isLoggedIn = useSelector((state: RootState) => state.isLoggedIn)
    const token = useSelector((state: RootState) => state.userToken)


    const dispatch = useDispatch()

    const genderNum = ['Others', 'Male', 'Female']

    const currentOccupationArr = ['', 'Student', 'Freelance', 'Part-Time', 'Full-Time', 'Unemployed', 'Others']

    const freshGradNum = [null, true, false]

    const educationArr = ['', 'High School', 'Diploma', 'Higher-Diploma', 'Degree', 'Master or abrove', 'Others']

    const contactTimeNum = ['', 'Morning', 'Afternoon', 'Whole Day']


    const getUserProfile = async () => {
        try {
            const response = await fetch(
                `${server}/api/users/userProfile`,
                {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },

                }
            );
            const json = await response.json();
            if (json.result) {

                dispatch(updateUserDataForm({
                    lastName: json.result.last_name,
                    firstName: json.result.first_name,
                    birth: json.result.birthday,
                    gender: json.result.gender,
                    tel: json.result.mobile,
                    email: json.result.email,
                    address: json.result.address,
                    currentJob: json.result.occupation,
                    aboutMe: json.result.descriptions,
                    workExp: json.result.work_exp,
                    education: json.result.education_level,
                    schoolName: json.result.school_name,
                    major: json.result.program_name,
                    gradYear: json.result.grad_year,
                    contactTime: json.result.contact_time

                }))

            }

        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        getUserProfile();

    }, []);




    const maleColorList = ["#fff", "#210000", "#fff"]
    const femaleColorList = ["#fff", "#fff", "#210000"]
    const maleFontColorList = ["#888", "#210000", "#888"]
    const femaleFontColorList = ["#888", "#888", "#210000"]

    const amColorList = ["#fff", "#210000", "#fff", "#fff"]
    const pmColorList = ["#fff", "#fff", "#210000", "#fff"]
    const allDayColorList = ["#fff", "#fff", "#fff", "#210000"]
    const amFontColorList = ["#888", "#210000", "#888", "#888"]
    const pmFontColorList = ["#888", "#888", "#210000", "#888"]
    const allDayFontColorList = ["#888", "#888", "#888", "#210000"]


    return (
        <ScrollView >

            <View style={{ height: 40 }}></View>
            <View style={{ flexGrow: 0.4 }}>
                <Form
                    onButtonPress={async () => {

                        if (isLoggedIn) {
                            try {
                                const response = await fetch(
                                    `${server}/api/users/userProfile`,
                                    {
                                        method: 'POST',
                                        headers: {
                                            'Content-Type': 'application/json',
                                            'Authorization': `Bearer ${token}`,

                                        },
                                        body: JSON.stringify({
                                            userProfileDetails: {


                                                last_name: formData.lastName,
                                                first_name: formData.firstName,
                                                birthday:formData.birth ,
                                                gender: formData.gender,
                                                mobile: formData.tel,
                                                email: formData.email,
                                                address: formData.address,
                                                occupation: formData.currentJob,
                                                descriptions: formData.aboutMe,
                                                work_exp: formData.workExp,
                                                education_level: formData.education,
                                                school_name: formData.schoolName,
                                                program_name: formData.major,
                                                grad_year: formData.gradYear,
                                                contact_time: formData.contactTime,

                                            }
                                        })

                                    }
                                );



                            } catch (error) {
                                console.error(error);
                            }

                            Alert.alert(
                                "Success",
                                "Update profile successfully!",
                                [

                                    { text: "OK", onPress: () => navigation.navigate('Jobilly') }
                                ]
                            );
                        } else {
                            navigation.navigate('LoginOrReg')
                        }
                    }

                    }

                    buttonStyle={styles.formSubmit}
                    buttonText="Update Profile"

                >
                    <FormItem
                        label="Last Name"
                        isRequired
                        value={formData.lastName}
                        onChangeText={(lastName) => dispatch(updateUserDataForm({ ...formData, lastName }))}
                        labelStyle={styles.label}
                        style={styles.formInput}
                    />
                    <FormItem
                        label="First Name"
                        isRequired
                        value={formData.firstName}
                        onChangeText={(firstName) => dispatch(updateUserDataForm({ ...formData, firstName }))}
                        labelStyle={styles.label}

                        style={styles.formInput}
                    />
                    <FormItem
                        label="Birth"
                        isRequired
                        value={formData.birth}
                        onChangeText={(birth) => dispatch(updateUserDataForm({ ...formData, birth }))}
                        labelStyle={styles.label}
                        placeholderTextColor={'#666'}
                        placeholder='dd/mm/yyyy'
                        style={styles.formInput}
                    />

                    <View style={{
                        height: 100,
                        flexDirection: "row"
                    }}>
                        <Text style={{
                            width: "20%",
                            height: 80,
                            marginTop: 15,
                            marginLeft: "5%",
                            fontWeight: "700"
                        }}>Gender</Text>

                        <TouchableOpacity
                            onPress={() => dispatch(updateUserDataForm({ ...formData, gender: formData.gender == 'Male' ? 'Others' : 'Male' }))}
                            style={{
                                width: "20%",
                                height: 50,
                                backgroundColor: "#fff",
                                borderColor: maleColorList[genderNum.indexOf(formData.gender)],
                                borderStyle: "solid",
                                borderWidth: 1,
                                marginLeft: "10%",
                                borderRadius: 25,
                                alignItems: "center",
                                justifyContent: "center"
                            }}
                        >
                            <Text style={{ color: maleFontColorList[genderNum.indexOf(formData.gender)], fontSize: 15 }}>Male</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => dispatch(updateUserDataForm({ ...formData, gender: formData.gender == 'Female' ? 'Others' : 'Female' }))}
                            style={{
                                width: "20%",
                                height: 50,
                                backgroundColor: "#fff",
                                borderColor: femaleColorList[genderNum.indexOf(formData.gender)],
                                borderStyle: "solid",
                                borderWidth: 1,
                                marginLeft: "10%",
                                borderRadius: 25,
                                alignItems: "center",
                                justifyContent: "center"
                            }}
                        >
                            <Text style={{ color: femaleFontColorList[genderNum.indexOf(formData.gender)], fontSize: 15 }}>Female</Text>
                        </TouchableOpacity>
                    </View>

                    <FormItem
                        label="Tel"
                        isRequired
                        value={formData.tel}
                        onChangeText={(tel) => dispatch(updateUserDataForm({ ...formData, tel }))}
                        labelStyle={styles.label}
                        style={styles.formInput}
                    />
                    <FormItem
                        label="Email"
                        isRequired
                        value={formData.email}
                        onChangeText={(email) => dispatch(updateUserDataForm({ ...formData, email }))}
                        labelStyle={styles.label}
                        style={styles.formInput}
                    />
                    <FormItem
                        label="Address"
                        isRequired
                        value={formData.address}
                        onChangeText={(address) => dispatch(updateUserDataForm({ ...formData, address }))}
                        labelStyle={styles.label}
                        style={styles.formInput}
                    />
                    <Picker
                        items={[
                            { label: 'Student', value: 1 },
                            { label: 'Freelance', value: 2 },
                            { label: 'Part-Time', value: 3 },
                            { label: 'Full-Time', value: 4 },
                            { label: 'Unemployed', value: 5 },
                            { label: 'Others', value: 6 },
                        ]}
                        label="Current Job"
                        selectedValue={currentOccupationArr.indexOf(formData.currentJob)}
                        onSelection={(item) => dispatch(updateUserDataForm({ ...formData, currentJob: item["label"] }))}
                        buttonStyle={styles.formInput}
                        labelStyle={styles.label}
                        placeholder='Please select'
                    />
                    <FormItem
                        label="About Me"
                        isRequired
                        value={formData.aboutMe}
                        onChangeText={(aboutMe) => dispatch(updateUserDataForm({ ...formData, aboutMe }))}
                        labelStyle={styles.label}
                        style={styles.formInput}
                    />

                    <View style={{
                        height: 100,
                        flexDirection: "row"
                    }}>
                        <Text style={{
                            width: "20%",
                            height: 80,
                            marginTop: 15,
                            marginLeft: "5%",
                            fontWeight: "700",
                            fontSize: 13
                        }}>Work Experience</Text>

                        <TouchableOpacity
                            onPress={() => dispatch(updateUserDataForm({ ...formData, workExp: formData.workExp == true ? null : true }))}
                            style={{
                                width: "20%",
                                height: 50,
                                backgroundColor: "#fff",
                                borderColor: maleColorList[freshGradNum.indexOf(formData.workExp)],
                                borderStyle: "solid",
                                borderWidth: 1,
                                marginLeft: "10%",
                                borderRadius: 25,
                                alignItems: "center",
                                justifyContent: "center"
                            }}
                        >
                            <Text style={{ color: maleFontColorList[freshGradNum.indexOf(formData.workExp)], fontSize: 15 }}>Yes</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => dispatch(updateUserDataForm({ ...formData, workExp: formData.workExp == false ? null : false }))}
                            style={{
                                width: "20%",
                                height: 50,
                                backgroundColor: "#fff",
                                borderColor: femaleColorList[freshGradNum.indexOf(formData.workExp)],
                                borderStyle: "solid",
                                borderWidth: 1,
                                marginLeft: "10%",
                                borderRadius: 25,
                                alignItems: "center",
                                justifyContent: "center"
                            }}
                        >
                            <Text style={{ color: femaleFontColorList[freshGradNum.indexOf(formData.workExp)], fontSize: 15 }}>No</Text>
                        </TouchableOpacity>
                    </View>

                    <Picker
                        items={[
                            { label: 'High School', value: 1 },
                            { label: 'Diploma', value: 2 },
                            { label: 'Higher-Diploma', value: 3 },
                            { label: 'Degree', value: 4 },
                            { label: 'Master or abrove', value: 5 },
                            { label: 'Others', value: 6 },
                        ]}
                        label="Education"
                        selectedValue={educationArr.indexOf(formData.education)}
                        onSelection={(item) => dispatch(updateUserDataForm({ ...formData, education: item["label"] }))}
                        buttonStyle={styles.formInput}
                        labelStyle={styles.label}
                        placeholder='Please select'
                    />
                    <FormItem
                        label="School Name"
                        isRequired
                        value={formData.schoolName}
                        onChangeText={(schoolName) => dispatch(updateUserDataForm({ ...formData, schoolName }))}
                        labelStyle={styles.label}
                        style={styles.formInput}
                    />
                    <FormItem
                        label="Major"
                        isRequired
                        value={formData.major}
                        onChangeText={(major) => dispatch(updateUserDataForm({ ...formData, major }))}
                        labelStyle={styles.label}
                        style={styles.formInput}
                    />
                    <FormItem
                        label="Grad Year"
                        isRequired
                        value={formData.gradYear}
                        onChangeText={(gradYear) => dispatch(updateUserDataForm({ ...formData, gradYear }))}
                        labelStyle={styles.label}
                        style={styles.formInput}
                    />
                    <View style={{
                        height: 100,
                        flexDirection: "row"
                    }}>
                        <Text style={{
                            width: "20%",
                            height: 80,
                            marginTop: 15,
                            marginLeft: "5%",
                            fontWeight: "700"
                        }}>Contact Time</Text>

                        <TouchableOpacity
                            onPress={() => dispatch(updateUserDataForm({ ...formData, contactTime: formData.contactTime == 'Morning' ? '' : 'Morning' }))}
                            style={{
                                width: "18%",
                                height: 50,
                                backgroundColor: "#fff",
                                borderColor: amColorList[contactTimeNum.indexOf(formData.contactTime)],
                                borderStyle: "solid",
                                borderWidth: 1,
                                marginLeft: "5%",
                                borderRadius: 25,
                                alignItems: "center",
                                justifyContent: "center"
                            }}
                        >
                            <Text style={{ color: amFontColorList[contactTimeNum.indexOf(formData.contactTime)], fontSize: 15 }}>a.m.</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => dispatch(updateUserDataForm({ ...formData, contactTime: formData.contactTime == 'Afternoon' ? '' : 'Afternoon' }))}
                            style={{
                                width: "18%",
                                height: 50,
                                backgroundColor: "#fff",
                                borderColor: pmColorList[contactTimeNum.indexOf(formData.contactTime)],
                                borderStyle: "solid",
                                borderWidth: 1,
                                marginLeft: "5%",
                                borderRadius: 25,
                                alignItems: "center",
                                justifyContent: "center"
                            }}
                        >
                            <Text style={{ color: pmFontColorList[contactTimeNum.indexOf(formData.contactTime)], fontSize: 15 }}>p.m.</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => dispatch(updateUserDataForm({ ...formData, contactTime: formData.contactTime == 'Whole Day' ? '' : 'Whole Day' }))}
                            style={{
                                width: "18%",
                                height: 50,
                                backgroundColor: "#fff",
                                borderColor: allDayColorList[contactTimeNum.indexOf(formData.contactTime)],
                                borderStyle: "solid",
                                borderWidth: 1,
                                marginLeft: "5%",
                                borderRadius: 25,
                                alignItems: "center",
                                justifyContent: "center"
                            }}
                        >
                            <Text style={{ color: allDayFontColorList[contactTimeNum.indexOf(formData.contactTime)], fontSize: 15 }}>All Day</Text>
                        </TouchableOpacity>
                    </View>


                </Form>
            </View>
        </ScrollView>
    )
}

export default ProfileScreen