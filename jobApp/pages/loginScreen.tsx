import React, { useEffect, useState } from "react";
import { Alert, Image, RefreshControl, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { initBookmarked, initHistory, updateAppliedJobs, updateBookmarked, updateJob, userLoigin } from "../actions";
import { server } from "../App";
import { styles } from "../css";
import { RootState } from "../store";
import { JobData } from "../type";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Form, FormItem } from "react-native-form-component";
import axios from "axios";


function LoginScreen({ navigation }) {
    const [email, setEmail] = useState("")
    const [pw, setPw] = useState("")
    const [message, setMessage] = useState("")
    const dispatch = useDispatch()
    let data = {
        username: email,
        password: pw
    }
    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ height: 40 }}></View>
            <Form
                onButtonPress={async () => {

                    try {
                        const res = await axios.post(`${server}/api/users/login`, data)
                        dispatch(userLoigin(res.data.token))
                        Alert.alert(
                            "Success",
                            "Login successfully!",
                            [

                                { text: "OK", onPress: () => navigation.navigate('Jobilly') }
                            ]
                        );
                    } catch (error) {

                        if (error?.response.status == 401) {
                            setMessage('Invaild Username/Password')
                        }

                    }


                }}
                buttonStyle={styles.formSubmit}
                buttonText="Login"
            >
                <FormItem
                    label="Email"
                    isRequired
                    value={email}
                    onChangeText={(email) => setEmail(email)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                    autoCapitalize='none'
                />
                <FormItem
                    label="Password"
                    isRequired
                    value={pw}
                    onChangeText={(pw) => setPw(pw)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                    secureTextEntry={true}
                />
                <Text style={{
                    color: '#f00',
                    marginLeft: 40
                }}>{message}</Text>
            </Form>



        </ScrollView>

    )
}

export default LoginScreen