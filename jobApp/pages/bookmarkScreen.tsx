import React, { useEffect, useState } from "react";
import { Alert, Image, RefreshControl, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { initBookmarked, initHistory, updateAppliedJobs, updateBookmarked, updateJob, userLoigin } from "../actions";
import { server } from "../App";
import { styles } from "../css";
import { RootState } from "../store";
import { JobData } from "../type";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Form, FormItem } from "react-native-form-component";
import axios from "axios";


function BookmarkScreen({ navigation }) {
    const data = useSelector((state: RootState) => state.postdata)
    const bookmarked = useSelector((state: RootState) => state.bookmarkedByUser)
    const appliedJobs = useSelector((state: RootState) => state.appliedJobs)
    const [bookmarkeddata, setBookmarkeddata] = useState(data.filter((e, idx) => bookmarked[idx]))
    const token = useSelector((state: RootState) => state.userToken)

    const [finalToken, setFinalToken] = useState(token)

    useEffect(() => {
        setFinalToken(token)
    }, [token])

    useEffect(() => {
        setBookmarkeddata(data.filter((e, idx) => bookmarked[idx]))
    }, [bookmarked])

    const dispatch = useDispatch()

    return (


        <ScrollView
            style={
                { backgroundColor: "#fff" }
            }
            contentContainerStyle={{
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.25,
                shadowRadius: 6.84,


            }}>
            <Text style={styles.boxSubTitle}>{bookmarkeddata.length > 1 ? "Bookmarked Jobs" : "Bookmarked Job"} : {bookmarkeddata.length}</Text>
            {/* {data.map((elem) => {
                return bookmarkBox(elem, navigation)
            })} */}
            {data.map((elem, idx) => {
                if (bookmarked[idx]) {

                    return bookmarkBox(elem, idx, navigation, dispatch, bookmarked, finalToken,appliedJobs)
                }
            })}

        </ScrollView>

    )
}

function bookmarkBox(elem: any, idx: number, navi: any, cb: any, bookmark: [], token: string,appliedJobs:any[]) {
    let bookmarkedArr = [...bookmark]
    let jobsId: number = elem.job_id
    return (
        <TouchableOpacity style={{
            width: "94%",
            height: 300,
            backgroundColor: "#fdfdfd",
            marginTop: 30,
            marginLeft: "3%",
            borderRadius: 16,

            overflow: "hidden",
        }} key={elem.job_id} onPress={async () => {
            try {
                const res = await fetch(`${server}/api/users/history`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify({
                        jobsId: jobsId
                    })

                })




            } catch (error) {

                if (error?.response.status == 401) {
                    console.warn('bookmark error')
                }

            }

            cb(initHistory(elem.job_id))


            navi.navigate('Details', {
                itemId: elem.job_id
            })
        }}>
            <TouchableOpacity style={{ height: 40, flexDirection: "row", justifyContent: "space-between" }} onPress={async () => {

                bookmarkedArr[idx] = false


                cb(updateBookmarked(bookmarkedArr))


                try {
                    const res = await fetch(`${server}/api/users/favourite`, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        },
                        body: JSON.stringify({
                            jobsId: jobsId
                        })

                    })



                } catch (error) {

                    if (error?.response.status == 401) {
                        console.warn('bookmark error')
                    }

                }
            }}>
                <Text></Text>
                <Text style={{ marginTop: 15, marginRight: 15, }}>✖️</Text>
            </TouchableOpacity>
            <View style={{ flexDirection: "row", height: 80, width: "100%", }}>

                <Image style={{ width: "33%", height: "100%", resizeMode: 'cover', marginLeft: 12, borderRadius: 7 }} source={{ uri: elem.imageurl[0] }} />
                <Text style={styles.bookmarkedBoxTitle}>{elem.title}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="work" style={styles.boxIcon} />
                <Text style={styles.greyText}>{elem.comapany_name}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="location-on" style={styles.boxIcon} />
                <Text style={styles.greySubText}>{elem.address}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
                <Icon name="attach-money" style={styles.boxIcon} />
                <Text style={styles.greySubText}>{elem.salary_project}</Text>
            </View>

            <View style={{ height: 30, borderStyle: "solid", borderBottomColor: "#ccc", borderBottomWidth: 1, width: "90%", marginLeft: "5%" }}></View>
            {!appliedJobs.includes(jobsId) && <TouchableOpacity style={{
                width: "80%",
                height: 30,
                backgroundColor: "#72c91a",
                marginTop: 15,
                marginLeft: "10%",
                borderRadius: 30,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
            }}
                onPress={() => {
                    navi.navigate('Apply Page', {
                        itemId: elem.job_id
                    })
                }}
            >
                <Text style={{
                    fontSize: 16,
                    color: "#fff",
                    fontWeight: "500",
                }}>Apply</Text>
            </TouchableOpacity>}
            {appliedJobs.includes(jobsId) && <View style={{
                width: "80%",
                height: 30,
                backgroundColor: "#777",
                marginTop: 15,
                marginLeft: "10%",
                borderRadius: 30,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
            }}
                
            >
                <Text style={{
                    fontSize: 16,
                    color: "#fff",
                    fontWeight: "500",
                }}>Applied</Text>
            </View>}

        </TouchableOpacity>
    )
}


export default BookmarkScreen