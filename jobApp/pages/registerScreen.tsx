import React, { useEffect, useState } from "react";
import { Alert, Image, RefreshControl, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { initBookmarked, initHistory, updateAppliedJobs, updateBookmarked, updateJob, userLoigin } from "../actions";
import { server } from "../App";
import { styles } from "../css";
import { RootState } from "../store";
import { JobData } from "../type";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Form, FormItem } from "react-native-form-component";
import axios from "axios";

function RegisterScreen({ navigation }) {
    const [email, setEmail] = useState("")
    const [pw, setPw] = useState("")
    const [pw2, setPw2] = useState("")
    const [message, setMessage] = useState("")
    const dispatch = useDispatch()
    let data = {
        username: email,
        password: pw
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ height: 40 }}></View>


            <Form
                onButtonPress={async () => {
                    if (pw != pw2) {
                        setMessage('Please enter same password twice')
                    } else {

                        try {
                            const res = await axios.post(`${server}/api/users/register`, data)
                            dispatch(userLoigin(res.data.token))
                            Alert.alert(
                                "Success",
                                "Registered successfully!",
                                [

                                    { text: "OK", onPress: () => navigation.navigate('Menubar') }
                                ]
                            );
                        } catch (error) {
                            if (error?.response.status == 400) {
                                setMessage('Email has been registered')
                            }

                        }
                    }


                }}
                buttonStyle={styles.formSubmit}
                buttonText="Register"
            >
                <FormItem
                    label="Email"
                    isRequired
                    value={email}
                    onChangeText={(email) => setEmail(email)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                    autoCapitalize='none'
                />
                <FormItem
                    label="Password"
                    isRequired
                    value={pw}
                    onChangeText={(pw) => setPw(pw)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                    placeholder='At least 6 charcters'
                    placeholderTextColor={'#666'}
                    secureTextEntry={true}
                    customValidation={() => {
                        return {
                            status: pw.length > 5,
                            message: 'Please input at least 6 charcters'
                        }
                    }}
                />

                <FormItem
                    label="Re-enter PW"
                    isRequired
                    value={pw2}
                    onChangeText={(pw2) => setPw2(pw2)}
                    labelStyle={styles.label}
                    style={styles.formInput}
                    placeholder='Please enter same password again'
                    placeholderTextColor={'#666'}
                    secureTextEntry={true}
                    customValidation={() => {
                        return {
                            status: pw == pw2,
                            message: 'Please input same password twice'
                        }
                    }}
                />
                <Text style={{
                    color: '#f00',
                    marginLeft: 40
                }}>{message}</Text>
            </Form>


        </ScrollView>

    )
}

export default RegisterScreen