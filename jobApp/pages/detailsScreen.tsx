import React, { useEffect, useState } from "react";
import { Image, RefreshControl, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { initBookmarked, initHistory, updateAppliedJobs, updateBookmarked, updateJob } from "../actions";
import { server } from "../App";
import { styles } from "../css";
import { RootState } from "../store";
import { JobData } from "../type";
import Icon from 'react-native-vector-icons/MaterialIcons';
import MapView, { Marker } from "react-native-maps";

function DetailsScreen({ route, navigation }) {
    const { itemId } = route.params
    // const postData = testDataArr.filter((elem) => elem.id == itemId);
    const data = useSelector((state: RootState) => state.postdata)
    const bookmarked = useSelector((state: RootState) => state.bookmarkedByUser)
    const appliedJobs = useSelector((state: RootState) => state.appliedJobs)
    const dispatch = useDispatch()
    const idx = data.indexOf(data.filter((elem) => elem.job_id == itemId)[0])
    // const postData = data.filter((elem) => elem.id == itemId);
    // console.warn(idx)
    const token = useSelector((state: RootState) => state.userToken)

    const [finalToken, setFinalToken] = useState(token)

    useEffect(() => {
        setFinalToken(token)
    }, [token])

    return (
        <ScrollView >

            {data.filter((elem) => elem.job_id == itemId).map((elem) => {
                return post(elem, idx, dispatch, bookmarked, navigation, finalToken, appliedJobs)
            })}

        </ScrollView>
    )
}

function post(elem: any, idx: number, cb: any, bookmark: [], navi, finalToken: string, appliedJobs: any[]) {
    let jobsId: number = elem.job_id
    return (
        <View key={elem.job_id}>

            <Image style={{ width: "100%", height: 300, resizeMode: 'cover' }} source={{ uri: elem.imageurl[0] }} />
            <Text style={styles.greyTextPost}>{elem['comapany_name']}</Text>
            <Text style={styles.boxTitlePost}>{elem.title}</Text>
            <Text style={styles.greyTextDetail}>{elem.descriptions}</Text>
            <View style={{ flexDirection: "row", marginTop: 10 }}>

                {!appliedJobs.includes(jobsId) && <TouchableOpacity
                    style={{
                        width: "60%",
                        height: 60,
                        backgroundColor: "#95b844",
                        marginLeft: "4%",

                        borderRadius: 30,
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                    onPress={() => {
                        navi.navigate('Apply Page', {
                            itemId: elem.job_id
                        })
                    }}
                >
                    <Text style={{
                        fontSize: 25,
                        color: "#fff",
                        fontWeight: "700",
                    }}>Apply</Text>
                </TouchableOpacity>}
                {appliedJobs.includes(jobsId) && <View
                    style={{
                        width: "60%",
                        height: 60,
                        backgroundColor: "#777",
                        marginLeft: "4%",

                        borderRadius: 30,
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                    }}

                >
                    <Text style={{
                        fontSize: 25,
                        color: "#fff",
                        fontWeight: "700",
                    }}>Applied</Text>
                </View>}
                <TouchableOpacity
                    style={bookmark[idx] ? styles.postBookmarkBtnClicked : styles.postBookmarkBtn}
                    onPress={async () => {
                        let bookmarkArr = [...bookmark]
                        if (bookmarkArr[idx]) {
                            bookmarkArr[idx] = false
                        } else {
                            bookmarkArr[idx] = true
                        }
                        cb({
                            type: 'updateBookmarked',
                            data: bookmarkArr
                        })

                        try {
                            const res = await fetch(`${server}/api/users/favourite`, {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': `Bearer ${finalToken}`
                                },
                                body: JSON.stringify({
                                    jobsId: jobsId
                                })

                            })



                        } catch (error) {

                            if (error?.response.status == 401) {
                                console.warn('bookmark error')
                            }

                        }
                    }}>
                    <Text style={{
                        fontSize: 25,
                        color: "#555",
                        fontWeight: "700",
                    }}>
                        <Icon name={bookmark[idx] ? 'bookmark' : 'bookmark-border'}
                            style={bookmark[idx] ? styles.postBookmarkIconClick : styles.postBookmarkIcon}
                        />
                    </Text>
                </TouchableOpacity>
            </View>

            <View style={{
                width: "90%",
                height: 20,
                borderBottomWidth: 1,
                borderColor: "#aaa",
                margin: "5%"
            }}></View>

            <Text style={styles.boxTitlePost}>Basic Info</Text>
            <View style={{ height: 20 }}></View>
            <Text style={styles.boxSubTitle}>Job Highlight</Text>
            <Text style={styles.greyTextDetail}>{elem.job_duties}</Text>
            <Text style={styles.boxSubTitle}>Address</Text>
            <Text style={styles.greyTextDetail}>{elem.address}</Text>
            <MapView
                style={{
                    width: "94%",
                    height: 300,
                    marginHorizontal: "3%",
                    borderRadius: 10,
                    marginBottom: 50


                }}
                initialRegion={{
                    latitude: parseInt(elem.lng),
                    longitude: parseInt(elem.lat),
                    latitudeDelta: 0.1,
                    longitudeDelta: 0.1,
                }}
            >
                <Marker

                    coordinate={{
                        latitude: elem.lat,
                        longitude: elem.lng,

                    }}

                />
            </MapView>

            <Text style={styles.boxSubTitle}>Salary</Text>
            <Text style={styles.greyTextDetail}>{elem.salary_project}</Text>
            <Text style={styles.boxSubTitle}>Working Hours</Text>
            <Text style={styles.greyTextDetail}>❖  {elem.working_hours} hours / day</Text>

            <Text style={styles.greyTextDetail}>❖  {elem.days_per_week} days / week</Text>

            <Text style={styles.greyTextDetail}>❖  {elem.annual_leave} days annual</Text>
            <Text style={styles.greyTextDetail}>❖  Contract Period : {elem.working_period} months</Text>

            <View style={{
                width: "90%",
                height: 20,
                borderBottomWidth: 1,
                borderColor: "#aaa",
                margin: "5%"
            }}></View>

            <Text style={styles.boxTitlePost}>Workplace Environment</Text>
            <View style={{ height: 20 }}></View>
            <View>

                <View style={styles.barContainer}>
                    <Text style={styles.barTextLeft}>Younger</Text>
                    <Text style={styles.barStyle}>{ageBarGenerator(elem.age_range)}</Text>
                    <Text style={styles.barText}>Older</Text>
                </View>
                <Text style={{ textAlign: 'center', fontSize: 12 }}>10s   20s   30s   40s   50s</Text>
                <View style={styles.barContainer}>
                    <Text style={styles.barTextLeft}>More male</Text>
                    <Text style={styles.barStyle}>{barGenerator5(elem.gender_range)}</Text>
                    <Text style={styles.barText}>More female</Text>
                </View>
                <View style={styles.barContainer}>
                    <Text style={styles.barTextLeft}>Independent</Text>
                    <Text style={styles.barStyle}>{barGenerator5(elem.teamwork_range)}</Text>
                    <Text style={styles.barText}>Group-working</Text>
                </View>
                <View style={styles.barContainer}>
                    <Text style={styles.barTextLeft}>Flexible workplace</Text>
                    <Text style={styles.barStyle}>{barGenerator3(elem.wfh_range)}</Text>
                    <Text style={styles.barText}>Office</Text>
                </View>
                <View style={styles.barContainer}>
                    <Text style={styles.barTextLeft}>Desk work</Text>
                    <Text style={styles.barStyle}>{barGenerator3(elem.client_range)}</Text>
                    <Text style={styles.barText}>Client side</Text>
                </View>
                <View style={styles.barContainer}>
                    <Text style={styles.barTextLeft}>Experience less required</Text>
                    <Text style={styles.barStyle}>{barGenerator3(elem.experience_range)}</Text>
                    <Text style={styles.barText}>Experience required</Text>
                </View>


            </View>
            <View style={{
                width: "90%",
                height: 20,
                borderBottomWidth: 1,
                borderColor: "#aaa",
                margin: "5%"
            }}></View>

            <Text style={styles.boxSubTitle}>Experience Requirement</Text>
            <Text style={styles.greyTextDetail}>{elem.exp_requirement}</Text>
            <Text style={styles.boxSubTitle}>Benefits</Text>
            <Text style={styles.greyTextDetail}>{elem.benefits}</Text>
            <Text style={styles.boxSubTitle}>Ways to Apply</Text>
            <Text style={styles.greyTextDetail}>{elem.apply_method}</Text>
            <Text style={styles.boxSubTitle}>Application Due Date</Text>
            <Text style={styles.greyTextDetail}>{elem.deadline.split('T')[0]}</Text>
            <Text style={styles.boxSubTitle}>Company Website</Text>
            <Text style={styles.greyTextDetail}>{elem.url}</Text>

            <View style={{ height: 100 }}></View>



        </View>
    )
}

function barIcon() {
    return (
        <Text style={{ color: '#d93f6a', fontSize: 20, position: 'absolute' }}>✿</Text>
    )
}

function barGenerator5(num: number) {
    let circle = "○"

    return (
        <Text style={styles.barStyle}>
            {num == 0 ? barIcon() : circle}---{num == 1 ? barIcon() : circle}---{num == 2 ? barIcon() : circle}---{num == 3 ? barIcon() : circle}---{num == 4 ? barIcon() : circle}
        </Text>
    )
}
function barGenerator3(num: number) {
    let circle = "○"


    return (
        <Text style={styles.barStyle}>
            {num == 0 ? barIcon() : circle}--------{num == 1 ? barIcon() : circle}--------{num == 2 ? barIcon() : circle}
        </Text>
    )
}
function ageBarGenerator(num: number) {

    let circle = "○"


    let binaryStr = Number(num).toString(2)

    let arr = binaryStr.split('')

    while (arr.length < 5) {
        arr.unshift("0")
    }


    return (


        <Text style={styles.barStyle}>
            {arr[0] == "1" ? barIcon() : circle}---{arr[1] == "1" ? barIcon() : circle}---{arr[2] == "1" ? barIcon() : circle}---{arr[3] == "1" ? barIcon() : circle}---{arr[4] == "1" ? barIcon() : circle}
        </Text>


    )
}

export default DetailsScreen