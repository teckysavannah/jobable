import { ReactReduxContext } from 'react-redux'
import { act } from 'react-test-renderer'
import { createStore } from 'redux'
import { JobPostActions } from './actions'
import { JobData,UserFormData } from './type'


export type RootState = {
    postdata: JobData[],
    appliedJobs:any[],
    bookmarkedByUser: boolean[],
    history:number,
    userToken:string,
    isLoggedIn:boolean,
    userFormData:UserFormData
}

const initalState: RootState = {
    postdata: [],
    appliedJobs:[],
    bookmarkedByUser: [],
    userToken:'',
    isLoggedIn:false,
    userFormData:{
        lastName:'',
        firstName:'',
        birth:'',
        gender:'Others',
        tel:'',
        email:'',
        address:'',
        currentJob:'',
        aboutMe:'',
        workExp:null,
        education:'',
        schoolName:'',
        major:'',
        gradYear:'',
        contactTime:''

    },
    history:0
}


function reducer(state: RootState = initalState, action: JobPostActions):RootState {
    switch (action.type) {
        case 'updateJob':
            return {
                postdata: action.data,
                appliedJobs:state.appliedJobs,
                bookmarkedByUser: state.bookmarkedByUser,
                userToken: state.userToken,
                isLoggedIn: state.isLoggedIn,
                userFormData:state.userFormData,
                history:state.history
            };
        case 'updateAppliedJobs':
            return {
                postdata: state.postdata,
                appliedJobs:action.data,
                bookmarkedByUser: state.bookmarkedByUser,
                userToken: state.userToken,
                isLoggedIn: state.isLoggedIn,
                userFormData:state.userFormData,
                history:state.history
            };
        case 'updateBookmarked':
            return {
                postdata: state.postdata,
                appliedJobs:state.appliedJobs,
                bookmarkedByUser: action.data,
                userToken: state.userToken,
                isLoggedIn: state.isLoggedIn,
                userFormData:state.userFormData,
                history:state.history
            };
        case 'initBookmarked':
            return {
                postdata: state.postdata,
                bookmarkedByUser: action.data,
                appliedJobs:state.appliedJobs,
                userToken: state.userToken,
                isLoggedIn: state.isLoggedIn,
                userFormData:state.userFormData,
                history:state.history
            };
        case 'userLogin':
            return {
                postdata: state.postdata,
                appliedJobs:state.appliedJobs,
                bookmarkedByUser: state.bookmarkedByUser,
                userToken: action.data,
                isLoggedIn:true,
                userFormData:state.userFormData,
                history:state.history
            };
        case 'userLogout':
            return {
                postdata: state.postdata,
                appliedJobs:[],
                bookmarkedByUser: state.bookmarkedByUser,
                userToken: '',
                isLoggedIn:false,
                userFormData:{
                    lastName:'',
                    firstName:'',
                    birth:'',
                    gender:'Others',
                    tel:'',
                    email:'',
                    address:'',
                    currentJob:'',
                    aboutMe:'',
                    workExp:null,
                    education:'',
                    schoolName:'',
                    major:'',
                    gradYear:'',
                    contactTime:''
            
                },
                history:state.history
            };
        case 'updateUserDataForm':
            return {
                postdata: state.postdata,
                appliedJobs:state.appliedJobs,
                bookmarkedByUser: state.bookmarkedByUser,
                userToken: state.userToken,
                isLoggedIn: state.isLoggedIn,
                userFormData:action.data,
                history:state.history
            };
        case 'initHistory':
            return {
                postdata: state.postdata,
                appliedJobs:state.appliedJobs,
                bookmarkedByUser: state.bookmarkedByUser,
                userToken: state.userToken,
                isLoggedIn: state.isLoggedIn,
                userFormData:state.userFormData,
                history:action.data
            };
            
    }
    return state;
}
export const store = createStore(reducer); 

store.subscribe(() => {
    console.log('called when updated')
})

