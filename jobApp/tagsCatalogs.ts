export const tags = {
    // example
    popular:[
        'High Income',
        'Beginners Welcome',
        'Fresh Graduate Welcome',
        'Project-Based',
        'Freelance',
        'Transportation Allowance'
    ],
    environment:[
        'Beginners Welcome',
        'Fresh Graduate Welcome',
        'Middle-Aged Welcome',
        'Senior Welcome',
        'Foreigners Welcome'
    ],
    jobType:[
        'Full-Time',
        'Part-Time',
        'Freelance',
        'Project-Based',
        'Contract',
        'Permanent',
        'Temporary',
        'Internship'
    ],
    benefits: [
        'Double Pay',
        'Dental Insurance',
        'Medical Insurance',
        'Life Insurance',
        'Transportation Allowance',
        'Education Allowance',
        'Housing Allowance',
        'Travel Allowance',
        'Flexible Working Hours',
        'Free Shuttle Bus',
        'Overtime Pay',
        'Performance Bonus',
        'Work From Home'
    ],
    careerLevel: [
        'Entry',
        'Middle',
        'Senior',
        'Top'
    ],
    educationLevel: [
        'Postgraduate',
        'Degree',
        'Non-Degree Tertiary',
        'Matriculated',
        'School Certificate'
    ],
    yearsOfExperience: [
        'No Experience',
        '1-2 Years Experience',
        '3-4 Years Experience',
        'Over 5 Years Experience',
        'Over 10 Years Experience'
    ],
    appearance: [
        'Casual',
        'Smart Casual',
        'Business Casual',
        'Business Attire',
        'Formal Black Tie'
    ]

} 