import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { ScrollView, Button, Text, View, Image, FlatList, StyleSheet, TouchableOpacity, Platform, Alert, RefreshControl } from 'react-native'


import Icon from 'react-native-vector-icons/MaterialIcons';
import { Provider, useDispatch, useSelector } from 'react-redux'
import { store, RootState } from './store'
import { updateJob, updateBookmarked, userLoigin, userLogout, updateUserDataForm, initBookmarked, initHistory, updateAppliedJobs } from './actions'
import axios from 'axios'
import { JobData, UserFormData } from './type'

import SplashScreen from 'react-native-splash-screen'
import { styles } from './css';
import HomeScreen from './pages/homePage';
import DetailsScreen from './pages/detailsScreen';
import LoginOrRegScreen from './pages/loginOrRegScreen';
import LoginScreen from './pages/loginScreen';
import RegisterScreen from './pages/registerScreen';
import ProfileScreen from './pages/profileScreen';
import HistoryScreen from './pages/historyScreen';
import BookmarkScreen from './pages/bookmarkScreen';
import AppliedScreen from './pages/appliedScreen';
import ApplyScreen from './pages/ApplyScreen';

export const server = 'https://server.wcatw.xyz'
// const server = 'http://localhost:8080'
const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();



const Menubar = ({ navigation }) => {
    const isLoggedIn = useSelector((state: RootState) => state.isLoggedIn)
    const dispatch = useDispatch()
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'Jobilly') {
                        iconName = focused
                            ? 'home'
                            : 'home';
                    } else if (route.name === 'Bookmark') {
                        iconName = 'bookmarks'
                    } else if (route.name === 'History') {
                        iconName = 'history'
                    } else if (route.name === 'Applied') {
                        iconName = 'history-edu'
                    } else if (route.name === 'Profile') {
                        iconName = 'emoji-people'
                    }

                    // You can return any component that you like here!
                    return <Icon name={iconName} style={styles.menuBarIcon} />
                },
                tabBarActiveTintColor: 'tomato',
                tabBarInactiveTintColor: 'gray',
            })}>
            <Tab.Screen
                name="Jobilly"
                component={HomeScreen}
                options={
                    {
                        title: 'Jobable',
                        headerStyle: {
                            backgroundColor: '#1e5ece',
                        },
                        headerTintColor: '#eee',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },
                        headerRight: () => (
                            <TouchableOpacity
                                style={{
                                    width: 30,
                                    height: 30
                                }}
                                onPress={() => {
                                    if (isLoggedIn) {
                                        dispatch(userLogout())
                                    } else {

                                        navigation.navigate('LoginOrReg')
                                    }
                                }}
                            >

                                <Text style={{
                                    position: 'absolute',
                                    right: 20,
                                    top: 5,
                                    width: 50,
                                    color: '#fff'
                                }}>{isLoggedIn ? 'Logout' : 'Login'}</Text>
                            </TouchableOpacity>
                        )
                    }
                } />
            <Tab.Screen
                name="History"
                component={isLoggedIn ? HistoryScreen : LoginOrRegScreen}
                options={
                    {
                        title: 'History',
                        headerStyle: {
                            backgroundColor: '#1e5ece',
                        },
                        headerTintColor: '#eee',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },

                    }
                } />
            <Tab.Screen
                name="Bookmark"
                component={isLoggedIn ? BookmarkScreen : LoginOrRegScreen}
                options={
                    {
                        title: 'Bookmark',
                        headerStyle: {
                            backgroundColor: '#1e5ece',
                        },
                        headerTintColor: '#eee',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },

                    }
                } />

            <Tab.Screen
                name="Applied"
                component={isLoggedIn ? AppliedScreen : LoginOrRegScreen}
                options={
                    {
                        title: 'Applied',
                        headerStyle: {
                            backgroundColor: '#1e5ece',
                        },
                        headerTintColor: '#eee',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },

                    }
                } />
            <Tab.Screen
                name="Profile"
                component={isLoggedIn ? ProfileScreen : LoginOrRegScreen}
                options={
                    {
                        title: 'Profile',
                        headerStyle: {
                            backgroundColor: '#fff',
                        },
                        headerTintColor: '#222',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },

                    }
                } />
        </Tab.Navigator>

    )
}


const App = () => {
    useEffect(() => {
        SplashScreen.hide()
    }, [])
    return (
        <Provider store={store}>

            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen
                        name="Menubar"
                        component={Menubar}
                        options={{ headerShown: false }}
                    />


                    <Stack.Screen
                        name="Details"
                        component={DetailsScreen}
                        options={
                            {
                                title: 'Jobable',
                                headerStyle: {
                                    backgroundColor: '#fff',
                                },
                                headerTintColor: '#000',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },

                            }
                        }
                    />
                    <Stack.Screen
                        name="LoginOrReg"
                        component={LoginOrRegScreen}
                        options={
                            {
                                title: 'Login/Register',
                                headerStyle: {
                                    backgroundColor: '#fff',
                                },
                                headerTintColor: '#000',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },

                            }
                        }
                    />
                    <Stack.Screen
                        name="Login"
                        component={LoginScreen}
                        options={
                            {
                                title: 'Login',
                                headerStyle: {
                                    backgroundColor: '#fff',
                                },
                                headerTintColor: '#000',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },

                            }
                        }
                    />
                    <Stack.Screen
                        name="Register"
                        component={RegisterScreen}
                        options={
                            {
                                title: 'Register',
                                headerStyle: {
                                    backgroundColor: '#fff',
                                },
                                headerTintColor: '#000',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },

                            }
                        }
                    />
                    <Stack.Screen
                        name="Apply Page"
                        component={ApplyScreen}
                        options={
                            {
                                title: 'Apply',
                                headerStyle: {
                                    backgroundColor: '#fff',
                                },
                                headerTintColor: '#000',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },

                            }
                        }
                    />

                </Stack.Navigator>

            </NavigationContainer>
        </Provider>

    )
}


export default App;



