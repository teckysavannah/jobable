import { fas } from '@fortawesome/free-solid-svg-icons'
import {JobData ,UserFormData} from './type'


export function updateJob(data:JobData[]){
    return{
        type:'updateJob' as const,
        data
    }
}

export function initBookmarked(data:JobData[]){
    return{
        type:'initBookmarked' as const,
        data: data.map((elem)=>{
            if(elem.bookmarked == null){
                return false
            }
            return true
        })
    }
}
export function updateBookmarked(data:JobData[]){
    return{
        type:'updateBookmarked' as const,
        data: data.map((elem)=>{
            if(elem){
                return true
            }
            return false
        })
    }
}

export function userLoigin(token:string){
    return{
        type:'userLogin' as const,
        data: token,
    }
}
export function userLogout(){
    return{
        type:'userLogout' as const,
    }
}
export function updateUserDataForm(data:UserFormData){
    return{
        type:'updateUserDataForm' as const,
        data
    }
}
export function initHistory(data:number){
    return{
        type:'initHistory' as const,
        data
    }
}
export function updateAppliedJobs(data:any[]){
    return{
        type:'updateAppliedJobs' as const,
        data
    }
}



export type JobPostActions= ReturnType<typeof updateJob> |
                            ReturnType<typeof updateBookmarked> |
                            ReturnType<typeof userLoigin>|
                            ReturnType<typeof userLogout>|
                            ReturnType<typeof initBookmarked>|
                            ReturnType<typeof updateUserDataForm>|
                            ReturnType<typeof initHistory>|
                            ReturnType<typeof updateAppliedJobs>
