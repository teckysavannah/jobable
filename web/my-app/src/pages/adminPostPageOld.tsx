import React from "react";
import Header from "../components/Header";
import SideBar from "../components/sideBar";
import { useForm, SubmitHandler } from "react-hook-form";


function AdminPostPage(){
    const {register,handleSubmit} = useForm()
    return(
        <div>
            <Header/>

            <div className="adminPostContainer">

                <SideBar/>

                <div className="adminPostForm">
                    <div className="title">
                        Job Post Form
                    </div>

                    <form>
                        <div className="inputLabel">imageurl</div>
                        <input {...register("imageurl")} />
                        <div className="inputLabel">company name</div>
                        <input {...register("comapany_name")} />
                        <div className="inputLabel">job title</div>
                        <input {...register("title")} />
                        <div className="inputLabel">descriptions</div>
                        <input {...register("descriptions")} />
                        <div className="inputLabel">job duties</div>
                        <input {...register("job_duties")} />
                        <div className="inputLabel">address</div>
                        <input {...register("address")} />
                        <div className="inputLabel">lat</div>
                        <input {...register("lat")} />
                        <div className="inputLabel">lng</div>
                        <input {...register("lng")} />
                        <div className="inputLabel">salary</div>
                        <input {...register("salary_project")} />
                        <div className="inputLabel">working hours</div>
                        <input {...register("working_hours")} />
                        <div className="inputLabel">working days per week</div>
                        <input {...register("days_per_week")} />
                        <div className="inputLabel">annual leave</div>
                        <input {...register("annual_leave")} />
                        <div className="inputLabel">working period</div>
                        <input {...register("working_period")} />
                        <div className="inputLabel">age distrubution</div>
                        <input {...register("age_range")} />

                        <div className="inputLabel">gender distrubution</div>
                        <select {...register("gender_range")}>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                        <div className="inputLabel">level of collaboration</div>
                        <select {...register("teamwork_range")} >
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                        <div className="inputLabel">place of employment</div>
                        <select {...register("wfh_range")} >
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                        <div className="inputLabel">frequency of meeting client</div>
                        <select {...register("client_range")} >
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                        <div className="inputLabel">level of experience</div>
                        <select {...register("experience_range")} >
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>


                        <div className="inputLabel">experience requirement</div>
                        <input {...register("exp_requirement")} />
                        <div className="inputLabel">benefits</div>
                        <input {...register("benefits")} />
                        <div className="inputLabel">apply methods</div>
                        <input {...register("apply_method")} />
                        <div className="inputLabel">post deadline</div>
                        <input {...register("deadline")} />
                        <div className="inputLabel">company url</div>
                        <input {...register("url")} />


                        <div className="submit-btn" onClick={handleSubmit((d)=>console.log(d))}>
                            submit
                        </div>  
                    </form>

                </div>

            </div>

        </div>
    )
}

export default AdminPostPage;