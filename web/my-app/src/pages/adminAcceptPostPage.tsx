import React, { useEffect } from "react";
import Header from "../components/Header";
import SideBar from "../components/sideBar";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import {
    Button,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    OutlinedInput,
    TextField
  } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/store";
import AdminSideBar from "../components/adminSideBar";
import axios from "axios";
import { server } from "../App";

import { Redirect } from "react-router";
import { initEmployerRequest, updateAdminShowingRequest, userLoigin } from "../redux/actions";



const FCStyle = {
    width: "100%",
    mb: "1rem",  
};

function AdminAcceptPostPage(){
    console.log('hihi')
    const token = useSelector((state:RootState)=>state.userReducer.userToken)
    

    
    const dispatch = useDispatch()
    useEffect(() => {
        console.log('hihihi')
        const token = window.localStorage.getItem('token')
        const role = window.localStorage.getItem('role')
        const isAdmin = role == 'admin' ? true : false
        dispatch(userLoigin(token ? token : '', isAdmin))

        const requsetId = window.localStorage.getItem('requsetId')
        const requsetData = window.localStorage.getItem('requsetData')
        const frequsetData = JSON.parse(requsetData!!)
        console.log('requsetData',requsetData)

        
        dispatch(updateAdminShowingRequest(requsetId? parseInt(requsetId) :0 ))
        dispatch(initEmployerRequest(frequsetData))

    }, [])
    const requsetId = useSelector((state:RootState)=>state.userReducer.adminShowingRequest)
    const requsetData = useSelector((state:RootState)=>state.userReducer.employerRequest)
    
    const data = requsetData.find((e:any)=>e.id==requsetId)

    
    const accept = async()=>{
        if(token != ''){
                
            try {
                
                
                const response = await fetch(
                    `${server}/api/admin/employerFormStatus`,
                    {
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`,
                        },
                        body: JSON.stringify({status:'Processing',formId:requsetId})
    
                    }
                );
                
                alert(
                    "Accepted"
                    
                );
                window.location.pathname = '/adminConsole'
                
            } catch (error) {
    
                console.error(error)
                alert("error")
    
            }
        }
    }
    const reject = async()=>{
        if(token != ''){

            try {
                
    
                const response = await fetch(
                    `${server}/api/admin/employerFormStatus`,
                    {
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`,
                        },
                        body: JSON.stringify({status:'Rejected',formId:requsetId})
    
                    }
                );
                
                alert(
                    "Rejected"
                    
                );
                window.location.pathname = '/adminConsole'
                
            } catch (error) {
    
                console.error(error)
                alert("error")
    
            }
        }
    }

    function sendMail():any {
    window.location.href = "mailto:" + data.email;
    }


      return(
        <div>
            <Header/>

            <div className="adminAcceptPostContainer">

                <AdminSideBar/>

                <div className="acceptRequsetInfoBtnContainer">

                    <div className="acceptRequsetInfo">

                        <div className="titleBox">
                            <div className="companyName">
                                {data.company_name}
                            </div>
                        </div>

                        <div className="titleBox">
                            <div className="jobTitle">
                                {data.job_title}
                            </div>

                            <div className="contentBox">
                                <div className="content">
                                    Post deadline:
                                    {data.due_date}
                                </div>

                                <div className="content">
                                    Contact method: 
                                    {data.contact_method}
                                </div>
                            </div>
                        </div>

                        <div className="content">
                            <div className="content">
                                Salary: 
                                ${data.salary}
                            </div>
                                Working hours: 
                                {data.work_hours} hours/day
                            </div>

                        <div className="companyInfoBox">

                                <div className="companyInfo">
                                    Company Infomation
                                </div>
                                <div className="content">
                                    Tel: 
                                    {data.phone}
                                </div>
                                <div className="content">
                                    Email: 
                                    <a href="#" onClick={sendMail}>{data.email}</a>
                                </div>
                                <div className="content">
                                    Website:
                                    <a href={data.website}>{data.website}</a>
                                </div>
                                <div className="content">
                                    Address: 
                                    <address>
                                        {data.address}
                                    </address>
                                </div>

                        </div>
                        
                    </div>

                    <div className="btnContainer">

                        <button className="acceptBtn" onClick={accept}>
                            Accept
                        </button>
                        <button className="rejectBtn" onClick={reject}>
                            Reject
                        </button>

                    </div>

                </div>

                

            </div>

        </div>
    )
}

export default AdminAcceptPostPage;