import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { server } from "../App";
import AdminSideBar from "../components/adminSideBar";
import Header from "../components/Header";
import SideBar from "../components/sideBar";
import { initEmployerRequest, updateAdminShowingRequest, userLoigin } from "../redux/actions";
import { RootState } from "../redux/store";

function requestList(e: any, i: number, cb: any, showingType: number) {
    // let showingObj = ['/adminAcceptPostPage','/adminAcceptPostPage','/adminPostPage','/adminEditPostPage','/adminAcceptPostPage']
    let showingObj: any = {
        'Waiting': '/adminAcceptPostPage',
        'Processing': '/adminPostPage',
        'Done': '/adminEditPostPage',
        'Rejected': '/adminAcceptPostPage'
    }
    const stateColr:any = {
        'Waiting': '#b71c1c',
        'Processing': '#ff8f00',
        'Done': '#0097a7',
        'Rejected': '#455a64'
    }


    return (


        <Link key={i} to={showingObj[e.form_status]} style={{ color: "#000" }}>
            <div  className="request-list" onClick={() => {
                cb(updateAdminShowingRequest(e.id))
                window.localStorage.setItem('requsetId',e.id)

            }}>
                <div className="title">


                    {e.company_name} - &nbsp;&nbsp;<span style={{ color: "#777" }}>{e.job_title}</span>


                </div>
                <div className="status">
                    <div className="state" style={{ borderColor: stateColr[e.form_status], color: stateColr[e.form_status] }}>

                        {e.form_status}
                    </div>
                    <div className="create-time">

                        {e.created_at.split('T')[0]}
                    </div>
                </div>

            </div>
        </Link>

    )
}

function AdminConsole() {
    const token = useSelector((state: RootState) => state.userReducer.userToken)
    const data = useSelector((state: RootState) => state.userReducer.employerRequest)
    const showingType = useSelector((state: RootState) => state.userReducer.adminShowingRequestType)
    const dispatch = useDispatch()
    const [allRequest, setAllRequest] = useState([])

    useEffect(() => {
        const token = window.localStorage.getItem('token')
        const role = window.localStorage.getItem('role')
        const isAdmin = role == 'admin' ? true : false
        dispatch(userLoigin(token ? token : '', isAdmin))
    }, [])


    const getEmployerReq = async () => {
        if (token != '') {

            try {
                const response = await fetch(
                    `${server}/api/jobs/allEmployerForm`,
                    {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        },

                    }
                );
                const json = await response.json();
                setAllRequest(json.result)
                dispatch(initEmployerRequest( json.result))
                window.localStorage.setItem('requsetData',JSON.stringify(json.result) )
    
    
            } catch (error) {
                console.error(error);
            }
        }

    };

    useEffect(() => {
        getEmployerReq();

    }, [token]);
    let finalData: any = []
    if (showingType == 0) {

        finalData = data
    } else if (showingType == 1) {
        finalData = data.filter((e: any) => e.form_status == "Waiting")
    } else if (showingType == 2) {
        finalData = data.filter((e: any) => e.form_status == "Processing")
    } else if (showingType == 3) {
        finalData = data.filter((e: any) => e.form_status == "Done")
    } else if (showingType == 4) {
        finalData = data.filter((e: any) => e.form_status == "Rejected")
    }
    console.log(data)

    return (
        <div>
            <Header />
            <div className="employer-console-main">
                <AdminSideBar />

                <div className="request-container">
                    {finalData.map((e: any, i: number) => requestList(e, i, dispatch, showingType))}

                </div>
            </div>
        </div>
    )
}

export default AdminConsole;