import React from "react";
import Header from "../components/Header";
import SideBar from "../components/sideBar";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import {
    Button,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    OutlinedInput,
    TextField
} from "@mui/material";
import { useSelector } from "react-redux";
import { RootState } from "../redux/store";
import AdminSideBar from "../components/adminSideBar";
import axios from "axios";
import { server } from "../App";



const FCStyle = {
    width: "100%",
    mb: "1rem",
};

function EmployerApplicantDetailsPage() {

    const token = useSelector((state: RootState) => state.userReducer.userToken)


    const applicantId = useSelector((state: RootState) => state.userReducer.employerShowingApplicantID)
    const applicantData = useSelector((state: RootState) => state.userReducer.applicantsByJob)

    const data = applicantData.find((e: any) => e.users_id == applicantId)


    // console.log(data)

    function sendMail(): any {
        window.location.href = "mailto:" + data.email;
    }


    return (
        <div>
            <Header />

            <div className="adminAcceptPostContainer">

                <SideBar />
                <div className="acceptRequsetInfoBtnContainer">

                    <div className="acceptRequsetContainer">

                    <div className="acceptRequsetInfo">

                        <div className="titleBox">
                            <div className="companyName">
                                {data.first_name} {data.last_name}
                            </div>
                        </div>

                        <div className="titleBox">
                            <div className="jobTitle">
                                {data.occupation}
                            </div>

                            <div className="contentBox">
                                
                                <div className="content">
                                    Contact time:
                                    {data.contact_time}
                                </div>
                                <div className="content">
                                    Tel:
                                    {data.mobile}
                                </div>
                                <div className="content">
                                    Email:
                                    <a href="#" onClick={sendMail}>{data.email}</a>
                                </div>

                                
                            </div>
                        </div>



                        <div className="candidateInfoContainer">

                            <div className="companyInfoBox">
                                <div className="companyInfo">
                                    Candidate Infomation
                                </div>
                                <div className="content">
                                    Gender:
                                    {data.gender}
                                </div>
                                <div className="content">
                                    Address:
                                    <address>
                                        {data.address}
                                    </address>
                                </div>
                                <div className="content">
                                    Birthday:
                                    {data.birthday.split('T')[0]}
                                </div>

                                <div className="content">
                                    Descriptions:
                                    <div>{data.descriptions}</div>

                                </div>
                                <div className="content">
                                Education level:
                                    <div>{data.education_level}</div>

                                </div>
                                <div className="content">
                                School name:
                                    <div>{data.school_name}</div>

                                </div>
                                <div className="content">
                                Program name:
                                    <div>{data.program_name}</div>

                                </div>
                                <div className="content">
                                Grad year:
                                    <div>{data.grad_year}</div>

                                </div>
                                {/* <div className="content">
                                Latest job title:
                                    <div>{data.latest_job_title}</div>

                                </div> */}
                                <div className="content">
                                Having Work Experiences?
                                    <div>{data.work_exp? 'Yes':'No'}</div>

                                </div>
                                
                            </div>


                            </div>

                        </div>

                    </div>
                </div>

            </div>

        </div>
    )
}

export default EmployerApplicantDetailsPage;