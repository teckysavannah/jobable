import React, { useLayoutEffect, useState } from "react";
import Header from "../components/Header";
import axios from "axios";
import { server } from "../App";
import { useDispatch } from "react-redux";
import { userLoigin } from "../redux/actions";
import { Redirect } from "react-router";
// import { useForm, SubmitHandler } from "react-hook-form";


function LoginRegisterPage() {
    // const { register, handleSubmit } = useForm({ mode: 'onBlur' })
    // const { register: reg2, handleSubmit: handle2 } = useForm({ mode: 'onBlur' })
    // const onSubmit = (data: any) => {
    //     alert(JSON.stringify(data));
    // };

    // const onSubmitReg = (data: any) => {
    //     alert(JSON.stringify(data));
    // };
    const dispatch = useDispatch()

    const [username,setUsername] = useState('')
    const [password,setPassword] = useState('')
    const [usernameReg,setUsernameReg] = useState('')
    const [passwordReg,setPasswordReg] = useState('')
    const [passwordReg2,setPasswordReg2] = useState('')

    const submitReg = async(e:any)=>{
        e.preventDefault()
        try {
            console.log('domian',server)
            const response = await fetch(
                `${server}/api/webUser/register`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        
                    },
                    body: JSON.stringify({username:usernameReg,password:passwordReg})

                }
            );
            const res = await response.json()
            // const res = await axios.post(`${server}/api/webUser/register`, {username:usernameReg,password:passwordReg})
            if (res.token && passwordReg==passwordReg2){

                const isAdmin = res.role=='admin'? true:false
                dispatch(userLoigin(res.token,isAdmin))
                window.localStorage.setItem('token',res.token)
                window.localStorage.setItem('role',res.role)
                alert(
                    "Register Success"
                    
                );
            }else if(passwordReg!=passwordReg2){
                alert('Please input same password twice')
            }else{
                alert(res.message)
            }
            
        } catch (error) {

            console.error(error)
            alert("error")

        }

    }
    const submitLogin = async(e:any)=>{
        e.preventDefault()

        try {
            console.log('domian',server)
            const response = await fetch(
                `${server}/api/webUser/login`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        
                    },
                    body: JSON.stringify({username,password})

                }
            );
            const res = await response.json()
            // const res = await axios.post(`${server}/api/webUser/login`, {username,password})
            // console.log(res)
            if(res.token){

                const isAdmin = res.role=='admin'? true:false
                dispatch(userLoigin(res.token,isAdmin))
                window.localStorage.setItem('token',res.token)
                window.localStorage.setItem('role',res.role)
                alert(
                    "Login Success"
                    
                );
            }else{
                alert(
                    "incorrect username/ password"
                    
                );
            }
        } catch (error) {

            console.error(error)
            alert("error")

        }

    }

    function useWindowSize() {
        const [size, setSize] = useState([window.innerWidth, window.innerHeight]);
        useLayoutEffect(() => {
          function updateSize() {
            setSize([window.innerWidth, window.innerHeight]);
          }
          window.addEventListener('resize', updateSize);
          updateSize();
          return () => window.removeEventListener('resize', updateSize);
        }, []);
        return size;
      }

    const [width, height] = useWindowSize()



    return (
        <div>
            <Header />

            <video autoPlay muted loop id="bg-video" className={
                width/height > 16/9 ? "bg-video-w":"bg-video-h"
            }>
                <source src="interview.mp4" type="video/mp4" />
            </video>

            <div id="login-reg-container" >
                <form key={1} onSubmit={submitLogin}>
                    <h3>Login</h3>
                    <input  type="email" name="email1" id="login-email" placeholder="Email" value={username} onChange={(e)=>setUsername(e.target.value)}/>
                    <input  type="password" name="password1" id="login-password" placeholder="Password" value={password} onChange={(e)=>setPassword(e.target.value)}/>
                    <a href="#">Forgot your password?</a>
                    <input type="submit" value="Login" className="submit-btn" id="sub-1" />
                </form>
                <div className="middle-line"></div>
                <form key={2} onSubmit={submitReg}>
                    <h3>Create Account</h3>
                    <input  type="email" name="email2" id="register-email" placeholder="Email" value={usernameReg} onChange={(e)=>setUsernameReg(e.target.value)}/>
                    <input  type="password" name="password2" id="register-password" placeholder="Password" value={passwordReg} onChange={(e)=>setPasswordReg(e.target.value)}/>
                    <input  type="password" name="password3" id="register-password2" placeholder="Re-enter password" value={passwordReg2} onChange={(e)=>setPasswordReg2(e.target.value)}/>
                    <input type="submit" value="Register" className="submit-btn" id="sub-2" />
                </form>
            </div>

        </div>
    )
}

export default LoginRegisterPage;

function userDispatch() {
    throw new Error("Function not implemented.");
}
