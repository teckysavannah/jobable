import React, { useEffect } from "react";
import Header from "../components/Header";
import SideBar from "../components/sideBar";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import {
    Button,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    OutlinedInput,
    TextField
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/store";
import AdminSideBar from "../components/adminSideBar";
import axios from "axios";
import { server } from "../App";
import { userLoigin } from "../redux/actions";



const FCStyle = {
    width: "100%",
    mb: "1rem",
};

function EmployerPostDetailPage() {

    const token = useSelector((state: RootState) => state.userReducer.userToken)
    // const dispatch = useDispatch()
    // useEffect(() => {
    //     const token = window.localStorage.getItem('token')
    //     const role = window.localStorage.getItem('role')
    //     const isAdmin = role == 'admin' ? true : false
    //     dispatch(userLoigin(token ? token : '', isAdmin))
    // }, [])


    const requsetId = useSelector((state: RootState) => state.userReducer.employerShowingRequest)
    const requsetData = useSelector((state: RootState) => state.userReducer.employerRequest)

    const data = requsetData.find((e: any) => e.id == requsetId)




    function sendMail(): any {
        window.location.href = "mailto:" + data.email;
    }


    return (
        <div>
            <Header />

            <div className="adminAcceptPostContainer">

                <SideBar />
                <div className="acceptRequsetInfoBtnContainer">

                    <div className="acceptRequsetContainer">

                    <div className="acceptRequsetInfo">

                        <div className="titleBox">
                            <div className="companyName">
                                {data.company_name}
                            </div>
                        </div>

                        <div className="titleBox">
                            <div className="jobTitle">
                                {data.job_title}
                            </div>

                            <div className="contentBox">
                                <div className="content">
                                    Post deadline:
                                    {data.due_date}
                                </div>

                                <div className="content">
                                    Contact method:
                                    {data.contact_method}
                                </div>
                            </div>
                        </div>

                        <div className="content">
                            <div className="content">
                                Salary:
                                ${data.salary}
                            </div>
                            Working hours:
                            {data.work_hours} hours/day
                        </div>

                        <div className="infoBtnContainer">

                            <div className="companyInfoBox">
                                <div className="companyInfo">
                                    Company Infomation
                                </div>
                                <div className="content">
                                    Tel:
                                    {data.phone}
                                </div>
                                <div className="content">
                                    Email:
                                    <a href="#" onClick={sendMail}>{data.email}</a>
                                </div>
                                <div className="content">
                                    Website:
                                    <a href={data.website}>{data.website}</a>
                                </div>
                                <div className="content">
                                    Address:
                                    <address>
                                        {data.address}
                                    </address>
                                </div>
                            </div>



                        </div>

                        </div>


                    </div>
                </div>

            </div>

        </div>
    )
}

export default EmployerPostDetailPage;