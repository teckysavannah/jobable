import React, { useEffect } from "react";
import Header from "../components/Header";
import SideBar from "../components/sideBar";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import {
    Button,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    OutlinedInput,
    TextField,
    
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/store";
import AdminSideBar from "../components/adminSideBar";
import axios from "axios";
import { server } from "../App";
import { updateEmployerShowingApplicantId, updateJobApplicants, userLoigin } from "../redux/actions";
import { Link } from "react-router-dom";



const FCStyle = {
    width: "100%",
    mb: "1rem",
};

function generateApplicantsList(elem: any,cb:any) {

    return (

        <Link key={elem.users_id} to={'/employerApplicantDetailsPage'} style={{ color: "#000" }}>
            <div className="request-list" onClick={async () => {
                cb(updateEmployerShowingApplicantId(elem.users_id))}}>


                {elem.first_name}&nbsp;{elem.last_name}
            </div>

        </Link>
    )
}


function EmployerDonePostDetailPage() {

    const token = useSelector((state: RootState) => state.userReducer.userToken)


    const requsetId = useSelector((state: RootState) => state.userReducer.employerShowingRequest)
    const requsetJobIdId = useSelector((state: RootState) => state.userReducer.employerShowingRequestJobID)
    const requsetData = useSelector((state: RootState) => state.userReducer.employerRequest)
    const applicants = useSelector((state: RootState) => state.userReducer.applicantsByJob)

    const data = requsetData.find((e: any) => e.id == requsetId)
    const dispatch = useDispatch()

    useEffect(() => {
        const token = window.localStorage.getItem('token')
        const role = window.localStorage.getItem('role')
        const isAdmin = role == 'admin' ? true : false
        dispatch(userLoigin(token ? token : '', isAdmin))
    }, [])

    const getApplicants = async () => {
        if (token != ''){

            try {
                const response = await fetch(
                    `${server}/api/employer/getApplicants`,
                    {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        },
                        body: JSON.stringify({ jobId: requsetJobIdId })
    
                    }
                );
                const json = await response.json();
    
                console.log(json, requsetJobIdId)
    
                dispatch(updateJobApplicants(json.applicantsInfo))
    
    
            } catch (error) {
                console.error(error);
            }
        }

    };

    useEffect(() => {
        getApplicants();

    }, []);


    console.log(applicants)

    function sendMail(): any {
        window.location.href = "mailto:" + data.email;
    }


    return (
        <div>
            <Header />

            <div className="adminAcceptPostContainer">

                <SideBar />

                <div className="acceptRequsetInfoBtnContainer">

                    <div className="acceptRequsetContainer">

                    <div className="acceptRequsetInfo">

                        <div className="titleBox">
                            <div className="companyName">
                                {data.company_name}
                            </div>
                        </div>

                        <div className="titleBox">
                            <div className="jobTitle">
                                {data.job_title}
                            </div>

                            <div className="contentBox">
                                <div className="content">
                                    Post deadline:
                                    {data.due_date}
                                </div>

                                <div className="content">
                                    Contact method:
                                    {data.contact_method}
                                </div>
                            </div>
                        </div>

                        <div className="content">
                            <div className="content">
                                Salary:
                                ${data.salary}
                            </div>
                            Working hours:
                            {data.work_hours} hours/day
                        </div>

                        <div className="infoBtnContainer">

                            <div className="companyInfoBox">
                                <div className="companyInfo">
                                    Company Infomation
                                </div>
                                <div className="content">
                                    Tel:
                                    {data.phone}
                                </div>
                                <div className="content">
                                    Email:
                                    <a href="#" onClick={sendMail}>{data.email}</a>
                                </div>
                                <div className="content">
                                    Website:
                                    <a href={data.website}>{data.website}</a>
                                </div>
                                <div className="content">
                                    Address:
                                    <address>
                                        {data.address}
                                    </address>
                                </div>
                            </div>

                        </div>

                        <div className="applicantContainer">
                            <div className="title">
                                Applicants
                            </div>
                            <div className="applicantList">
                                {applicants.map((e) => generateApplicantsList(e,dispatch))}
                            </div>
                        </div>
                    </div>

                </div>
                </div>



            </div>

        </div>
    )
}

export default EmployerDonePostDetailPage;


