import React, { useEffect } from "react";
import Header from "../components/Header";
import SideBar from "../components/sideBar";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import {
    Button,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    OutlinedInput,
    TextField
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/store";
import AdminSideBar from "../components/adminSideBar";
import { server } from "../App";
import { userLoigin } from "../redux/actions";

import {
    IconLookup,
    IconDefinition,
    findIconDefinition
} from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const imageLookup: IconLookup = { prefix: 'far', iconName: 'image' }
const imageIconDefinition: IconDefinition = findIconDefinition(imageLookup)


const FCStyle = {
    width: "100%",
    mb: "1rem",
};

function AdminPostPage() {
    const token = useSelector((state: RootState) => state.userReducer.userToken)
    const requsetId = useSelector((state: RootState) => state.userReducer.adminShowingRequest)
    const requsetData = useSelector((state: RootState) => state.userReducer.employerRequest)
    const dispatch = useDispatch()
    useEffect(() => {
        const token = window.localStorage.getItem('token')
        const role = window.localStorage.getItem('role')
        const isAdmin = role == 'admin' ? true : false
        dispatch(userLoigin(token ? token : '', isAdmin))
    }, [])

    const { control, handleSubmit } = useForm();
    const formSubmitHandler = async (formData: any) => {
        console.log(formData);
        let ageRangeArr = [0, 0, 0, 0, 0]

        if (formData.age_range.includes('10s')) {
            ageRangeArr[0] = 1
        }
        if (formData.age_range.includes('20s')) {
            ageRangeArr[1] = 1
        }
        if (formData.age_range.includes('30s')) {
            ageRangeArr[2] = 1
        }
        if (formData.age_range.includes('40s')) {
            ageRangeArr[3] = 1
        }
        if (formData.age_range.includes('50s')) {
            ageRangeArr[4] = 1
        }
        const bnumber = parseInt(ageRangeArr.join(''), 2)

        // console.log(bnumber)
        formData.age_range = bnumber
        // formData.deadline = formData.deadline.split('-')
        // formData.deadline[1] ++
        // formData.deadline = formData.deadline.join('-')
        formData.deadline = new Date(formData.deadline)
        console.log(formData.deadline)
        const img = formData.imageurl[0]
        delete formData.imageurl

        const finalFormData = new FormData()
        finalFormData.append('adminFormData', JSON.stringify(formData))
        finalFormData.append('formId', requsetId.toString())
        finalFormData.append('image_filename', img)

        if (token != '') {

            try {
                const response = await fetch(
                    `${server}/api/admin/adminPost`,
                    {
                        method: 'POST',
                        headers: {

                            'Authorization': `Bearer ${token}`,
                        },
                        body: finalFormData

                    }
                );
                const json = await response.json();
                console.log(json)

                const response2 = await fetch(
                    `${server}/api/admin/employerFormStatus`,
                    {
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`,
                        },
                        body: JSON.stringify({ status: 'Done', formId: requsetId })

                    }
                );

                alert(
                    "Create Job Post Success"

                );
                window.location.pathname = '/adminConsole'



            } catch (error) {

                console.error(error)
                alert("error")

            }
        }

    };


    const data = requsetData.find((e: any) => e.id == requsetId)

    console.log(requsetData)

    const ages = ["10s", "20s", "30s", "40s", "50s"];

    function sendMail(): any {
        window.location.href = "mailto:" + data.email;
    }

    return (
        <div>
            <Header />

            <div className="adminPostContainer">

                <AdminSideBar />

                <div className="PostAndFormContainer">

                    <div className="processRequsetContainer">

                        <div className="companyName">
                            {data.company_name}
                        </div>

                        <div className="titleBox">

                            <div className="content">
                                Post deadline:
                                {data.due_date}
                            </div>

                            <div className="content">
                                Contact method:
                                {data.contact_method}
                            </div>

                        </div>



                        <div className="content">
                            <div className="content">
                                Job title:
                                {data.job_title}
                            </div>
                            <div className="content">
                                Salary:
                                ${data.salary}
                            </div>
                            Working hours:
                            {data.work_hours} hours/day
                        </div>

                        <div className="companyInfoBox">
                            <div className="companyInfo">
                                Company Infomation
                            </div>
                            <div className="content">
                                Tel:
                                {data.phone}
                            </div>
                            <div className="content">
                                Email:
                                <a href="#" onClick={sendMail}>{data.email}</a>
                            </div>
                            <div className="content">
                                Website:
                                <a href={data.website}>{data.website}</a>
                            </div>
                            <div className="content">
                                Address:
                                <address>
                                    {data.address}
                                </address>
                            </div>

                        </div>

                    </div>

                    <div className="adminPostForm">

                        <div className="title">
                            Create Job Post Form
                        </div>

                        <div>
                            <form onSubmit={handleSubmit(formSubmitHandler)}>


                                {/* <Controller
                                    name="imageurl"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">Image URL</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Image URL"
                                                
                                                {...field}
                                            />
                                        </FormControl>
                                    )} /> */}
                                <Controller
                                    name="comapany_name"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">company name</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Company Name"
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="title"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">job title</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Job Title"
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="descriptions"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">descriptions</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Descriptions"
                                                multiline
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="job_duties"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">job duties</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Job Duties"
                                                multiline
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="address"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">address</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Address"
                                                multiline
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="lat"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">latitude</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Latitude"
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="lng"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">longitude</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Longitude"
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="salary_project"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">salary</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Salary"
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="working_hours"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">working hours</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Working Hours"
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="days_per_week"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">working days per week</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Working Days Per Week"
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="annual_leave"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">annual leave</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Annual Leave"
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="working_period"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">working period</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Working Period"
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />

                                <Controller
                                    name="age_range"
                                    control={control}
                                    defaultValue={[]}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">age distrubution</InputLabel>
                                            <Select
                                                {...field}
                                                labelId="age_range"
                                                label="Age Distrubution"
                                                multiple
                                            >
                                                {ages.map((age) => (
                                                    <MenuItem value={age} key={age}>
                                                        {age}
                                                    </MenuItem>
                                                ))}
                                            </Select>
                                        </FormControl>
                                    )} />


                                <Controller
                                    name="gender_range"
                                    control={control}
                                    defaultValue={[]}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">gender distribution</InputLabel>
                                            <Select
                                                {...field}
                                                labelId="gender_range"
                                                label="Gender Distribution"
                                            >
                                                <MenuItem value={0}>1</MenuItem>
                                                <MenuItem value={1}>2</MenuItem>
                                                <MenuItem value={2}>3</MenuItem>
                                                <MenuItem value={3}>4</MenuItem>
                                                <MenuItem value={4}>5</MenuItem>
                                            </Select>
                                        </FormControl>
                                    )} />

                                <Controller
                                    name="teamwork_range"
                                    control={control}
                                    defaultValue={[]}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">level of collaboration</InputLabel>
                                            <Select
                                                {...field}
                                                labelId="teamwork_range"
                                                label="Level Of Collaboration"
                                            >
                                                <MenuItem value={0}>1</MenuItem>
                                                <MenuItem value={1}>2</MenuItem>
                                                <MenuItem value={2}>3</MenuItem>
                                                <MenuItem value={3}>4</MenuItem>
                                                <MenuItem value={4}>5</MenuItem>
                                            </Select>
                                        </FormControl>
                                    )} />

                                <Controller
                                    name="wfh_range"
                                    control={control}
                                    defaultValue={[]}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">place of employment</InputLabel>
                                            <Select
                                                {...field}
                                                labelId="wfh_range"
                                                label="Place Of Employment"
                                            >
                                                <MenuItem value={0}>1</MenuItem>
                                                <MenuItem value={1}>2</MenuItem>
                                                <MenuItem value={2}>3</MenuItem>
                                            </Select>
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="client_range"
                                    control={control}
                                    defaultValue={[]}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">frequency of meeting client</InputLabel>
                                            <Select
                                                {...field}
                                                labelId="client_range"
                                                label="Frequency Of Meeting Client"
                                            >
                                                <MenuItem value={0}>1</MenuItem>
                                                <MenuItem value={1}>2</MenuItem>
                                                <MenuItem value={2}>3</MenuItem>
                                            </Select>
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="experience_range"
                                    control={control}
                                    defaultValue={[]}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">level of experience</InputLabel>
                                            <Select
                                                {...field}
                                                labelId="experience_range"
                                                label="Level Of Experience"
                                            >
                                                <MenuItem value={0}>1</MenuItem>
                                                <MenuItem value={1}>2</MenuItem>
                                                <MenuItem value={2}>3</MenuItem>
                                            </Select>
                                        </FormControl>
                                    )} />

                                <Controller
                                    name="exp_requirement"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">experience requirements</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Experience Requirements"
                                                multiline
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="benefits"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">benefits</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Benefits"
                                                multiline
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="apply_method"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">apply method</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Apply Method"
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="deadline"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">post deadline</InputLabel>

                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Post Deadline"
                                                placeholder="yyyy/mm/dd"
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />
                                <Controller
                                    name="url"
                                    control={control}
                                    render={({ field }) => (
                                        <FormControl sx={FCStyle}>
                                            <InputLabel id="inputLabel">company website</InputLabel>
                                            <OutlinedInput
                                                id="component-outlined"
                                                label="Company Website"
                                                {...field}
                                            />
                                        </FormControl>
                                    )} />

                                <Controller
                                    name="imageurl"
                                    control={control}
                                    render={({ field }) => (
                                        <div className="jobPosterContainer">

                                            <label className="chooseImgContainer">
                                                <div className="chooseImgBtn">
                                                    <FontAwesomeIcon className="chooseImgIcon" icon={imageIconDefinition} />
                                                    Choose Job Poster
                                                </div>
                                                <input
                                                    className="chooseInput"
                                                    type="file"
                                                    onChange={e => {
                                                        field.onChange(e.target.files);
                                                    }}
                                                />
                                            </label>
                                        </div>
                                    )} />



                                <FormControl>
                                    <Button
                                        type="submit"
                                        variant="contained"
                                        fullWidth
                                        sx={{ mt: ".75rem", fontWeight: "bold", width: "100%", mb: "3rem" }}
                                    >
                                        create job post
                                    </Button>
                                </FormControl>
                            </form>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    )
}

export default AdminPostPage;