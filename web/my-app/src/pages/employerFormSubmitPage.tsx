import React, { useEffect, useState } from "react";
import Header from "../components/Header";
import SideBar from "../components/sideBar";
import { useForm, SubmitHandler } from "react-hook-form";
import axios from "axios";
import { server } from "../App";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/store";
import { Redirect } from "react-router";
import { userLoigin } from "../redux/actions";

function EmployerFormSubmitPage() {
    const {register,handleSubmit} = useForm()
    const token = useSelector((state:RootState)=>state.userReducer.userToken)
    const dispatch = useDispatch()
    useEffect(() => {
        const token = window.localStorage.getItem('token')
        const role = window.localStorage.getItem('role')
        const isAdmin = role == 'admin' ? true : false
        dispatch(userLoigin(token ? token : '', isAdmin))
    }, [])
    
    
    return (
        <div>
            <Header />
            
            <div className="employerEnquiryForm">
                <div className="title">

                    Enquiry Form
                </div>
                
                <form>
                    <div className="inputLabel">company name</div>
                    <input {...register("company_name")} />
                    <div className="inputLabel">company address</div>
                    <input {...register("address")} />
                    <div className="inputLabel">job title</div>
                    <input {...register("job_title")} />
                    <div className="inputLabel">salary</div>
                    <input {...register("salary")} />
                    <div className="inputLabel">work hours</div>
                    <input {...register("work_hours")} placeholder="i.e. 8a.m. -6p.m."/>
                    <div className="inputLabel">contact number</div>
                    <input {...register("phone")} />
                    <div className="inputLabel">e-mail</div>
                    <input {...register("email")} />
                    <div className="inputLabel">contact methed</div>
                    <select {...register("contact_method")}  >
                     <option value="Phone">Phone</option>
                     <option value="Email">Email</option>
                     </select>
                    <div className="inputLabel">application deadline</div>
                    <input {...register("due_date")} />
                    <div className="inputLabel">company website</div>
                    <input {...register("website")} />
                    <div className="submit-btn" onClick={
                        handleSubmit(async(d)=>{
                            console.log('hihi')
                            if(token != ''){

                                try {
                                    const response = await fetch(
                                        `${server}/api/employer/employerForm`,
                                        {
                                            method: 'POST',
                                            headers: {
                                                'Content-Type': 'application/json',
                                                'Authorization': `Bearer ${token}`,
                                            },
                                            body: JSON.stringify({employerFormData:d})
                        
                                        }
                                    );
                                    const json = await response.json();
                                    
                                    alert(
                                        "Submit form success"
                                        
                                    );
                                    window.location.pathname = '/'
                                    
                                    
                                } catch (error) {
                        
                                    console.error(error)
                                    alert("error")
                        
                                }
                            }
                        })
                    }>
                        submit
                    </div>
                    
                </form>
                <div className="bottom">

                </div>
            </div>

            

            
        </div>
    )
}

export default EmployerFormSubmitPage;