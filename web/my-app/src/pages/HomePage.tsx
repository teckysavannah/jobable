import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import Header from "../components/Header";
import { RootState } from "../redux/store";

function HomePage() {
    const isloggedin = useSelector((state: RootState) => state.userReducer.isLoggedIn)
    const dispatch = useDispatch()
    return (
        <div className="homePage">
            <Header />
            
            <main>
                <div className="banner">
                    <div className="slogan-contianer">
                        <div className="slogan1">
                            hiring whenever you want
                            {/* <div className="thick-line">

                            </div> */}
                        </div>
                        <div className="slogan2">
                            match the right talent with us
                            {/* <div className="thick-line2">

                            </div> */}
                        </div>

                    </div>
                    <img src="webBackground3.jpg" alt="" />
                </div>
                <div className="progress-container">
                    <div className="progress-title">
                        how to post a job?
                    </div>
                    <div className="boxx-container">

                        <div className="boxx">
                            <div className="step-box">STEP 1</div>
                            <div className="box-title">Enquiry</div>
                            <div className="box-content">Please feel free to contact us by "phone" or "web form".</div>
                        </div>
                        <div className="boxx">
                            <div className="step-box">STEP 2</div>
                            <div className="box-title">
                                Meeting
                            </div>
                            <div className="box-content">
                                Our staffs will hear about hiring issues, budget, period, etc. and propose the optimal plan.
                            </div>
                        </div>
                        <div className="boxx">
                            <div className="step-box">STEP 3</div>
                            <div className="box-title">
                                Manuscript creation
                            </div>
                            <div className="box-content">
                                Based on your company's request and recruitment issues, we will create a recruitment manuscript for the desired human resources.
                            </div>
                        </div>
                        <div className="boxx">
                            <div className="step-box">STEP 4</div>
                            <div className="box-title">
                                Confirmation and
                                Publication
                            </div>
                            <div className="box-content">
                                Posting start. You can modify the manuscript as many times as you like depends on the situation. We will analysis the number of accesses and the number of applications.
                            </div>
                        </div>
                        <div className="boxx">
                            <div className="step-box">STEP 5</div>
                            <div className="box-title">Interview / recruitment</div>
                            <div className="box-content">
                                We propose specific methods such as how to follow applicants and make appointment. We will back up until successful recruitment.
                            </div>
                        </div>
                    </div>

                </div>

                <div className="blue-line"></div>
                <div className="qna-container">
                    <div className="progress-title">
                        Q & A
                    </div>
                    <div className="boxx-container">

                        <div className="boxx">
                            <div className="box-title">
                                <div className="step-box">Q.</div>
                                Is there a cost for inquiries and consultations?
                            </div>
                            <div className="box-content">
                                <div className="step-boxA">A.</div>
                                FREE for any inquiries and consultation. You will be charged only after the first publication. If you have any questions, please feel free to contact us.</div>
                        </div>
                        <div className="boxx">
                            <div className="box-title">
                                <div className="step-box">Q.</div>
                                Do I have to write the job post details myself?
                            </div>
                            <div className="box-content">
                                <div className="step-boxA">A.</div>
                                You can choose from two methods: creating your own manuscript or creating a manuscript for your sales staff. Please be assured that the price will not change when you create the manuscript at our company.
                            </div>
                        </div>
                        <div className="boxx">
                            <div className="box-title">
                                <div className="step-box">Q.</div>
                                How much does it cost?
                            </div>
                            <div className="box-content">
                                <div className="step-boxA">A.</div>
                                We will propose the most suitable plan for the customer based on the recruitment requirement and the status of issues. Please feel free to contact us.
                            </div>
                        </div>
                        <div className="boxx">
                            <div className="box-title">
                                <div className="step-box">Q.</div>
                                Is it possible to recruit full-time employees?
                            </div>
                            <div className="box-content">
                                <div className="step-boxA">A.</div>
                                We also have a full-time employee recruitment plan. Please feel free to contact us.
                            </div>
                        </div>

                    </div>

                </div>
                <div className="consult-box">
                    
                    <div className="footer-register-btn">

                        {!isloggedin && <Link to='/register' >Register</Link>}
                        {isloggedin && <Link to='/employerForm' className="register">Start Consulting For Free</Link>}
                    </div>
                    <div className="footer-tel">
                        Tel : 62003844
                    </div>
                </div>
                {/* <footer>
                    <div className="footer-slogan">
                        start consulting <br />for free
                    </div>
                    <div className="footer-register-btn">

                        {!isloggedin && <Link to='/register' >Register</Link>}
                        {isloggedin && <Link to='/employerForm' className="loggedin-consult-btn">Start Consult</Link>}
                    </div>
                    <div className="footer-tel">
                        Tel : 62003844
                    </div>
                </footer> */}
            </main>
        </div>
    );
}

export default HomePage;