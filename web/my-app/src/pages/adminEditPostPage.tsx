import React, { useEffect, useState } from "react";
import Header from "../components/Header";
import SideBar from "../components/sideBar";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import {
    Button,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    OutlinedInput,
    TextField
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/store";
import AdminSideBar from "../components/adminSideBar";
import { server } from "../App";
import { userLoigin } from "../redux/actions";
import {
    IconLookup,
    IconDefinition,
    findIconDefinition
} from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const imageLookup: IconLookup = { prefix: 'far', iconName: 'image' }
const imageIconDefinition: IconDefinition = findIconDefinition(imageLookup)




const FCStyle = {
    width: "100%",
    mb: "1rem",
};

function AdminEditPostPage() {
    const token = useSelector((state: RootState) => state.userReducer.userToken)
    const requsetId = useSelector((state: RootState) => state.userReducer.adminShowingRequest)
    const requsetData = useSelector((state: RootState) => state.userReducer.employerRequest)

    // const [allJobs, setAllJobs] = useState([])
    const [theJob, setTheJob] = useState<any>([{ address: "hihi" }])
    const { handleSubmit, control, setValue } = useForm({ mode: 'onBlur' })
    const dispatch = useDispatch()

    useEffect(() => {
        const token = window.localStorage.getItem('token')
        const role = window.localStorage.getItem('role')
        const isAdmin = role == 'admin' ? true : false
        dispatch(userLoigin(token ? token : '', isAdmin))
    }, [])

    const loadJobs = async () => {

        try {

            const response: any = await fetch(
                `${server}/api/jobs/allJobs`
            );
            const theRespose = await response.json()
            // console.log('response')
            console.log(theRespose)
            // setAllJobs(theRespose)
            setTheJob(theRespose.filter((e: any) => e.employer_forms_id == requsetId))

        } catch (error) {

            console.error(error)
            alert("loaderror")

        }
    }


    useEffect(() => {
        loadJobs()
    }, [])

    console.log('theJob', theJob[0])

    function toAgeRange(num: number) {
        const binaryStr = Number(num).toString(2)
        const Arr = binaryStr.split('')
        while (Arr.length < 5) {
            Arr.unshift('0')
        }
        let optionArr = ['10s', '20s', '30s', '40s', '50s']
        const outputArr = []
        for (let i = 0; i < 5; i++) {
            if (Arr[i] == '1') {
                outputArr.push(optionArr[i])
            }
        }
        return outputArr
    }



    useEffect(() => {
        if (theJob) {
            setValue('imageurl', theJob[0].imageurl);
            setValue('comapany_name', theJob[0].comapany_name);
            setValue('title', theJob[0].title);
            setValue('descriptions', theJob[0].descriptions);
            setValue('job_duties', theJob[0].job_duties);
            setValue('address', theJob[0].address);
            setValue('lat', theJob[0].lat);
            setValue('lng', theJob[0].lng);
            setValue('salary_project', theJob[0].salary_project);
            setValue('working_hours', theJob[0].working_hours);
            setValue('days_per_week', theJob[0].days_per_week);
            setValue('annual_leave', theJob[0].annual_leave);
            setValue('working_period', theJob[0].working_period);
            setValue('age_range', toAgeRange(theJob[0].age_range));
            setValue('gender_range', theJob[0].gender_range);
            console.log('gender_range',theJob[0].gender_range)
            setValue('teamwork_range', theJob[0].teamwork_range);
            setValue('wfh_range', theJob[0].wfh_range);
            console.log('wfh_range',theJob[0].wfh_range)
            setValue('client_range', theJob[0].client_range);
            setValue('experience_range', theJob[0].experience_range);
            setValue('exp_requirement', theJob[0].exp_requirement);
            setValue('benefits', theJob[0].benefits);
            setValue('apply_method', theJob[0].apply_method);
            setValue('deadline', theJob[0].deadline);
            setValue('url', theJob[0].website);
        }
    }, [theJob]);

    // console.log('allJob', requsetId, allJobs)
    // console.log('theJob', requsetId, allJobs.filter((e: any) => e.employer_forms_id == requsetId))



    const formSubmitHandler = async (formData: any) => {
        // console.log(formData);
        let ageRangeArr = [0, 0, 0, 0, 0]

        if (formData.age_range.includes('10s')) {
            ageRangeArr[0] = 1
        }
        if (formData.age_range.includes('20s')) {
            ageRangeArr[1] = 1
        }
        if (formData.age_range.includes('30s')) {
            ageRangeArr[2] = 1
        }
        if (formData.age_range.includes('40s')) {
            ageRangeArr[3] = 1
        }
        if (formData.age_range.includes('50s')) {
            ageRangeArr[4] = 1
        }
        const bnumber = parseInt(ageRangeArr.join(''), 2)

        // console.log(bnumber)
        formData.age_range = bnumber
        // formData.deadline = formData.deadline.split('-')
        // formData.deadline[1] ++
        // formData.deadline = formData.deadline.join('-')
        formData.deadline = new Date(formData.deadline)
        console.log(formData.deadline)
        const img = formData.imageurl[0]
        delete formData.imageurl

        const finalFormData = new FormData()
        finalFormData.append('adminFormData', JSON.stringify(formData))
        finalFormData.append('formId', requsetId.toString())
        finalFormData.append('image_filename', img)

        if (token != '') {

            try {
                const response = await fetch(
                    `${server}/api/admin/adminPost`,
                    {
                        method: 'PUT',
                        headers: {

                            'Authorization': `Bearer ${token}`,
                        },
                        body: finalFormData

                    }
                );
                const json = await response.json();



                alert(
                    "Update Job Post Success"

                );
                window.location.pathname = '/adminConsole'



            } catch (error) {

                console.error(error)
                alert("error")

            }
        }

    };

    // useEffect(()=>{
    //     formSubmitHandler
    // },[theJob])

    const data = requsetData.find((e: any) => e.id == requsetId)

    // console.log(requsetData)

    const ages = ["10s", "20s", "30s", "40s", "50s"];

    function sendMail(): any {
        window.location.href = "mailto:" + data.email;
    }

    return (
        <div>
            <Header />

            <div className="adminPostContainer">

                <AdminSideBar />

                <div className="widthContainer">

                <div className="processRequsetContainer">

                    <div className="companyName">
                        {data.company_name}
                    </div>

                    <div className="titleBox">

                        <div className="content">
                            Post deadline:
                            {data.due_date}
                        </div>

                        <div className="content">
                            Contact method:
                            {data.contact_method}
                        </div>

                    </div>



                    <div className="content">
                        <div className="content">
                            Job title:
                            {data.job_title}
                        </div>
                        <div className="content">
                            Salary:
                            ${data.salary}
                        </div>
                        Working hours:
                        {data.work_hours} hours/day
                    </div>

                    <div className="companyInfoBox">
                        <div className="companyInfo">
                            Company Infomation
                        </div>
                        <div className="content">
                            Tel:
                            {data.phone}
                        </div>
                        <div className="content">
                            Email:
                            <a href="#" onClick={sendMail}>{data.email}</a>
                        </div>
                        <div className="content">
                            Website:
                            <a href={data.website}>{data.website}</a>
                        </div>
                        <div className="content">
                            Address:
                            <address>
                                {data.address}
                            </address>
                        </div>

                    </div>

                </div>

                <div className="adminPostForm">
                    <div className="title">
                        Edit Job Post
                    </div>

                    <div>
                        <form onSubmit={handleSubmit(formSubmitHandler)}>

                            <Controller
                                name="comapany_name"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">company name</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Company Name"
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="title"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">job title</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Job Title"
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="descriptions"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">descriptions</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Descriptions"
                                            multiline
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="job_duties"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">job duties</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Job Duties"
                                            multiline
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="address"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">address</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Address"
                                            multiline
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="lat"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">latitude</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Latitude"
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="lng"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">longitude</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Longitude"
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="salary_project"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">salary</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Salary"
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="working_hours"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">working hours</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Working Hours"
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="days_per_week"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">working days per week</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Working Days Per Week"
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="annual_leave"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">annual leave</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Annual Leave"
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="working_period"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">working period</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Working Period"
                                            {...field}
                                        />
                                    </FormControl>
                                )} />

                            <Controller
                                name="age_range"
                                control={control}
                                defaultValue={[]}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">age distrubution</InputLabel>
                                        <Select
                                            {...field}
                                            labelId="age_range"
                                            label="Age Distrubution"
                                            multiple
                                        >
                                            {ages.map((age) => (
                                                <MenuItem value={age} key={age}>
                                                    {age}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                )} />

                            <Controller
                                name="gender_range"
                                control={control}
                                defaultValue={[]}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">gender distribution</InputLabel>
                                        <Select
                                            {...field}
                                            labelId="gender_range"
                                            label="Gender Distribution"
                                        >
                                            <MenuItem value={0}>1</MenuItem>
                                            <MenuItem value={1}>2</MenuItem>
                                            <MenuItem value={2}>3</MenuItem>
                                            <MenuItem value={3}>4</MenuItem>
                                            <MenuItem value={4}>5</MenuItem>
                                        </Select>
                                    </FormControl>
                                )} />

                            <Controller
                                name="teamwork_range"
                                control={control}
                                defaultValue={[]}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">level of collaboration</InputLabel>
                                        <Select
                                            {...field}
                                            labelId="teamwork_range"
                                            label="Level Of Collaboration"
                                        >
                                            <MenuItem value={0}>1</MenuItem>
                                            <MenuItem value={1}>2</MenuItem>
                                            <MenuItem value={2}>3</MenuItem>
                                            <MenuItem value={3}>4</MenuItem>
                                            <MenuItem value={4}>5</MenuItem>
                                        </Select>
                                    </FormControl>
                                )} />

                            <Controller
                                name="wfh_range"
                                control={control}
                                defaultValue={[]}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">place of employment</InputLabel>
                                        <Select
                                            {...field}
                                            labelId="wfh_range"
                                            label="Place Of Employment"
                                        >
                                            <MenuItem value={0}>1</MenuItem>
                                            <MenuItem value={1}>2</MenuItem>
                                            <MenuItem value={2}>3</MenuItem>
                                        </Select>
                                    </FormControl>
                                )} />

                            <Controller
                                name="client_range"
                                control={control}
                                defaultValue={[]}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">frequency of meeting client</InputLabel>
                                        <Select
                                            {...field}
                                            labelId="client_range"
                                            label="Frequency Of Meeting Client"
                                        >
                                            <MenuItem value={0}>1</MenuItem>
                                            <MenuItem value={1}>2</MenuItem>
                                            <MenuItem value={2}>3</MenuItem>
                                        </Select>
                                    </FormControl>
                                )} />
                            <Controller
                                name="experience_range"
                                control={control}
                                defaultValue={[]}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">level of experience</InputLabel>
                                        <Select
                                            {...field}
                                            labelId="experience_range"
                                            label="Level Of Experience"
                                        >
                                            <MenuItem value={0}>1</MenuItem>
                                            <MenuItem value={1}>2</MenuItem>
                                            <MenuItem value={2}>3</MenuItem>
                                        </Select>
                                    </FormControl>
                                )} />

                            <Controller
                                name="exp_requirement"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">experience requirements</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Experience Requirements"
                                            multiline
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="benefits"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">benefits</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Benefits"
                                            multiline
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="apply_method"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">apply method</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Apply Method"
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="deadline"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">post deadline</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Post Deadline"
                                            placeholder="yyyy/mm/dd"
                                            {...field}
                                        />
                                    </FormControl>
                                )} />
                            <Controller
                                name="url"
                                control={control}
                                render={({ field }) => (
                                    <FormControl sx={FCStyle} focused={true}>
                                        <InputLabel id="inputLabel">company website</InputLabel>
                                        <OutlinedInput
                                            id="component-outlined"
                                            label="Company Website"
                                            {...field}
                                        />
                                    </FormControl>
                                )} />

                            <Controller
                                name="imageurl"
                                control={control}
                                render={({ field }) => (
                                    <div className="jobPosterContainer">

                                        <label className="chooseImgContainer">
                                            <div className="chooseImgBtn">
                                                <FontAwesomeIcon className="chooseImgIcon" icon={imageIconDefinition} />
                                                Choose Job Poster
                                            </div>
                                            <input
                                                className="chooseInput"
                                                type="file"
                                                onChange={e => {
                                                    field.onChange(e.target.files);
                                                }}
                                            />
                                        </label>
                                    </div>
                                )} />

                            <FormControl>
                                <Button
                                    type="submit"
                                    variant="contained"
                                    fullWidth
                                    sx={{ margin: ".75rem,", fontWeight: "bold", width: "100%", mb: "3rem" }}
                                >
                                    Update
                                </Button>
                            </FormControl>
                        </form>
                    </div>

                </div>

                </div>
            
            </div>

        </div>
    )
}

export default AdminEditPostPage;