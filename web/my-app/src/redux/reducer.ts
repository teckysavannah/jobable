import React from "react";
import { WebUserActions } from './actions'

export type UserState = {
    userToken: string,
    isLoggedIn: boolean,
    isAdmin: boolean,
    employerRequest: any[],
    adminShowingRequest: number,
    adminShowingRequestType: number,
    employerShowingRequestType: number,
    employerShowingRequest: number,
    applicantsByJob: any[],
    employerShowingRequestJobID:number,
    employerShowingApplicantID:number,

}

const initalState: UserState = {
    userToken: '',
    isLoggedIn: false,
    isAdmin: false,
    employerRequest: [],
    adminShowingRequest: 0,
    adminShowingRequestType: 0,
    employerShowingRequestType: 0,
    employerShowingRequest: 0,
    applicantsByJob: [],
    employerShowingRequestJobID:0,
    employerShowingApplicantID:0,
}


export const userReducer = (state: UserState = initalState, action: WebUserActions): UserState => {
    switch (action.type) {
        case 'userLogin':
            return {
                userToken: action.data.token,
                isLoggedIn: true,
                isAdmin: action.data.isAdmin,
                employerRequest: state.employerRequest,
                adminShowingRequest: state.adminShowingRequest,
                adminShowingRequestType: state.adminShowingRequestType,
                employerShowingRequestType: state.employerShowingRequestType,
                employerShowingRequest: state.employerShowingRequest,
                applicantsByJob: state.applicantsByJob,
                employerShowingRequestJobID:state.employerShowingRequestJobID,
                employerShowingApplicantID:state.employerShowingApplicantID,
            }
        case 'userLogout':
            return {
                userToken: '',
                isLoggedIn: false,
                isAdmin: false,
                employerRequest: state.employerRequest,
                adminShowingRequest: state.adminShowingRequest,
                adminShowingRequestType: state.adminShowingRequestType,
                employerShowingRequestType: state.employerShowingRequestType,
                employerShowingRequest: state.employerShowingRequest,
                applicantsByJob: state.applicantsByJob,
                employerShowingRequestJobID:state.employerShowingRequestJobID,
                employerShowingApplicantID:state.employerShowingApplicantID,
            }
        case 'initEmployerRequest':
            return {
                userToken: state.userToken,
                isLoggedIn: state.isLoggedIn,
                isAdmin: state.isAdmin,
                employerRequest: action.data,
                adminShowingRequest: state.adminShowingRequest,
                adminShowingRequestType: state.adminShowingRequestType,
                employerShowingRequestType: state.employerShowingRequestType,
                employerShowingRequest: state.employerShowingRequest,
                applicantsByJob: state.applicantsByJob,
                employerShowingRequestJobID:state.employerShowingRequestJobID,
                employerShowingApplicantID:state.employerShowingApplicantID,
            }
        case 'updateAdminShowingRequest':
            return {
                userToken: state.userToken,
                isLoggedIn: state.isLoggedIn,
                isAdmin: state.isAdmin,
                employerRequest: state.employerRequest,
                adminShowingRequest: action.data,
                adminShowingRequestType: state.adminShowingRequestType,
                employerShowingRequestType: state.employerShowingRequestType,
                employerShowingRequest: state.employerShowingRequest,
                applicantsByJob: state.applicantsByJob,
                employerShowingRequestJobID:state.employerShowingRequestJobID,
                employerShowingApplicantID:state.employerShowingApplicantID,
            }
        case 'updateAdminShowingType':
            return {
                userToken: state.userToken,
                isLoggedIn: state.isLoggedIn,
                isAdmin: state.isAdmin,
                employerRequest: state.employerRequest,
                adminShowingRequest: state.adminShowingRequest,
                adminShowingRequestType: action.data,
                employerShowingRequestType: state.employerShowingRequestType,
                employerShowingRequest: state.employerShowingRequest,
                applicantsByJob: state.applicantsByJob,
                employerShowingRequestJobID:state.employerShowingRequestJobID,
                employerShowingApplicantID:state.employerShowingApplicantID,
            }
        case 'updateEmployerShowingType':
            return {
                userToken: state.userToken,
                isLoggedIn: state.isLoggedIn,
                isAdmin: state.isAdmin,
                employerRequest: state.employerRequest,
                adminShowingRequest: state.adminShowingRequest,
                adminShowingRequestType: state.adminShowingRequestType,
                employerShowingRequestType: action.data,
                employerShowingRequest: state.employerShowingRequest,
                applicantsByJob: state.applicantsByJob,
                employerShowingRequestJobID:state.employerShowingRequestJobID,
                employerShowingApplicantID:state.employerShowingApplicantID,
            }
        case 'updateEmployerShowingRequest':
            return {
                userToken: state.userToken,
                isLoggedIn: state.isLoggedIn,
                isAdmin: state.isAdmin,
                employerRequest: state.employerRequest,
                adminShowingRequest: state.adminShowingRequest,
                adminShowingRequestType: state.adminShowingRequestType,
                employerShowingRequestType: state.employerShowingRequestType,
                employerShowingRequest: action.data,
                applicantsByJob: state.applicantsByJob,
                employerShowingRequestJobID:state.employerShowingRequestJobID,
                employerShowingApplicantID:state.employerShowingApplicantID,
            }
        case 'updateJobApplicants':
            return {
                userToken: state.userToken,
                isLoggedIn: state.isLoggedIn,
                isAdmin: state.isAdmin,
                employerRequest: state.employerRequest,
                adminShowingRequest: state.adminShowingRequest,
                adminShowingRequestType: state.adminShowingRequestType,
                employerShowingRequestType: state.employerShowingRequestType,
                employerShowingRequest: state.employerShowingRequest,
                applicantsByJob: action.data,
                employerShowingRequestJobID:state.employerShowingRequestJobID,
                employerShowingApplicantID:state.employerShowingApplicantID,
            }
        case 'updateEmployerShowingRequestJobId':
            return {
                userToken: state.userToken,
                isLoggedIn: state.isLoggedIn,
                isAdmin: state.isAdmin,
                employerRequest: state.employerRequest,
                adminShowingRequest: state.adminShowingRequest,
                adminShowingRequestType: state.adminShowingRequestType,
                employerShowingRequestType: state.employerShowingRequestType,
                employerShowingRequest: state.employerShowingRequest,
                applicantsByJob: state.applicantsByJob,
                employerShowingRequestJobID:action.data,
                employerShowingApplicantID:state.employerShowingApplicantID,
            }
        case 'updateEmployerShowingApplicantId':
            return {
                userToken: state.userToken,
                isLoggedIn: state.isLoggedIn,
                isAdmin: state.isAdmin,
                employerRequest: state.employerRequest,
                adminShowingRequest: state.adminShowingRequest,
                adminShowingRequestType: state.adminShowingRequestType,
                employerShowingRequestType: state.employerShowingRequestType,
                employerShowingRequest: state.employerShowingRequest,
                applicantsByJob: state.applicantsByJob,
                employerShowingRequestJobID:state.employerShowingRequestJobID,
                employerShowingApplicantID:action.data,
            }
    }
    return state;
}