import {applyMiddleware, combineReducers, compose, createStore} from 'redux'
import {
    RouterState,
    connectRouter,
    routerMiddleware,
    CallHistoryMethodAction
  } from 'connected-react-router';
import { createBrowserHistory } from 'history';

import { userReducer, UserState } from './reducer';

export type RootState ={
    router:RouterState,
    userReducer:UserState,

}

declare global{
    /* tslint:disable:interface-name */
    interface Window{
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}



export const history = createBrowserHistory()

// function reducer(state:RootState=initialState,action:any):RootState{
//     switch(action){
        
//     }
//     return state
// }

const rootReducer = combineReducers<RootState>({
    router: connectRouter(history),
    userReducer:userReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export const store = createStore(rootReducer,
    composeEnhancers(

        applyMiddleware(routerMiddleware(history))
    )
)





