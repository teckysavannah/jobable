import React from "react";

export function userLoigin(token:string,isAdmin:boolean){
    return{
        type:'userLogin' as const,
        data: {token,isAdmin},
    }
}
export function userLogout(){
    return{
        type:'userLogout' as const,
    }
}
export function initEmployerRequest(data:[]){
    return{
        type:'initEmployerRequest' as const,
        data
    }
}
export function updateAdminShowingRequest(data:number){
    return{
        type:'updateAdminShowingRequest' as const,
        data
    }
}
export function updateAdminShowingType(data:number){
    return{
        type:'updateAdminShowingType' as const,
        data
    }
}
export function updateEmployerShowingType(data:number){
    return{
        type:'updateEmployerShowingType' as const,
        data
    }
}
export function updateEmployerShowingRequest(data:number){
    return{
        type:'updateEmployerShowingRequest' as const,
        data
    }
}
export function updateJobApplicants(data:any[]){
    return{
        type:'updateJobApplicants' as const,
        data
    }
}
export function updateEmployerShowingRequestJobId(data:number){
    return{
        type:'updateEmployerShowingRequestJobId' as const,
        data
    }
}
export function updateEmployerShowingApplicantId(data:number){
    return{
        type:'updateEmployerShowingApplicantId' as const,
        data
    }
}

export type WebUserActions= ReturnType<typeof userLoigin>|
                            ReturnType<typeof userLogout>|
                            ReturnType<typeof initEmployerRequest>|
                            ReturnType<typeof updateAdminShowingRequest>|
                            ReturnType<typeof updateAdminShowingType>|
                            ReturnType<typeof updateEmployerShowingType>|
                            ReturnType<typeof updateEmployerShowingRequest>|
                            ReturnType<typeof updateJobApplicants>|
                            ReturnType<typeof updateEmployerShowingRequestJobId>|
                            ReturnType<typeof updateEmployerShowingApplicantId>
                            