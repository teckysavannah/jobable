import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"
import { updateEmployerShowingType } from "../redux/actions"
import { RootState } from "../redux/store"

import {
    IconLookup,
    IconDefinition,
    findIconDefinition
} from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'

library.add(far, fas)


const cloneLookup: IconLookup = { prefix: 'fas', iconName: 'clone' }
const cloneIconDefinition: IconDefinition = findIconDefinition(cloneLookup)
const clockLookup: IconLookup = { prefix: 'far', iconName: 'clock' }
const clockIconDefinition: IconDefinition = findIconDefinition(clockLookup)
const checkLookup: IconLookup = { prefix: 'fas', iconName: 'check-circle' }
const checkIconDefinition: IconDefinition = findIconDefinition(checkLookup)

export default function SideBar() {

    const dispatch = useDispatch()
    const showingType = useSelector((state: RootState) => state.userReducer.employerShowingRequestType)
    return (
        <div className="bar-container">
            <div className="spacing"></div>
            

            <Link to={'/employerConsole'}>

                <div className={showingType == 0 ? "option-btn-focus" : "option-btn"} onClick={() => dispatch(updateEmployerShowingType(0))}>
                <FontAwesomeIcon className="chooseImgIcon" icon={cloneIconDefinition} />
                    All
                </div>
            </Link>
            <Link to={'/employerConsole'}>
                <div className={showingType == 1 ? "option-btn-focus" : "option-btn"} onClick={() => dispatch(updateEmployerShowingType(1))}>
                <FontAwesomeIcon className="chooseImgIcon" icon={checkIconDefinition} />
                    Posted
                </div>
            </Link>
            <Link to={'/employerConsole'}>
                <div className={showingType == 2 ? "option-btn-focus" : "option-btn"} onClick={() => dispatch(updateEmployerShowingType(2))}>
                <FontAwesomeIcon className="chooseImgIcon" icon={clockIconDefinition} />
                    Pending
                </div>
            </Link>
        </div>
    )
}