
import { useFormState } from "react-hook-form"
import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"
import { UserState } from "../redux/reducer"
import { userLogout } from "../redux/actions"
import { RootState } from "../redux/store"

export default function Header() {

    const isloggedin = useSelector((state: RootState) => state.userReducer.isLoggedIn)
    const dispatch = useDispatch()
    console.log(isloggedin)
    return (
        <header className="headerr">
            <div className="logo">
                <Link to='/' >Jobable</Link>
            </div>
            <div className="buttons">
                <div className="login-btn">

                    {isloggedin && <Link to='/employerConsole' >Console</Link>}
                </div>
                <div className="register-btn">

                    {!isloggedin && <Link to='/register' >Login / Register</Link>}
                    {isloggedin && <div onClick={() => {
                        dispatch(userLogout())
                        
                        window.localStorage.clear()
                        window.location.pathname = '/'
                    }}>Logout</div>}
                </div>

            </div>
        </header>
    )
}