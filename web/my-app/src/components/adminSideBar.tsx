import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"
import { updateAdminShowingType } from "../redux/actions"
import { RootState } from "../redux/store"
import {
    IconLookup,
    IconDefinition,
    findIconDefinition
} from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'

library.add(far, fas)


const cloneLookup: IconLookup = { prefix: 'fas', iconName: 'clone' }
const cloneIconDefinition: IconDefinition = findIconDefinition(cloneLookup)
const clockLookup: IconLookup = { prefix: 'far', iconName: 'clock' }
const clockIconDefinition: IconDefinition = findIconDefinition(clockLookup)
const spinnerLookup: IconLookup = { prefix: 'fas', iconName: 'spinner' }
const spinnerIconDefinition: IconDefinition = findIconDefinition(spinnerLookup)
const checkLookup: IconLookup = { prefix: 'fas', iconName: 'check-circle' }
const checkIconDefinition: IconDefinition = findIconDefinition(checkLookup)
const timesLookup: IconLookup = { prefix: 'far', iconName: 'times-circle' }
const timesIconDefinition: IconDefinition = findIconDefinition(timesLookup)



export default function AdminSideBar() {
    const dispatch = useDispatch()
    const showingType = useSelector((state: RootState) => state.userReducer.adminShowingRequestType)



    return (
        <div className="bar-container">
            <div className="spacing"></div>
            <Link to={'/adminConsole'}>

                <div className={showingType == 0 ? "option-btn-focus" : "option-btn"} onClick={() => dispatch(updateAdminShowingType(0))}>
                <FontAwesomeIcon className="chooseImgIcon" icon={cloneIconDefinition} />
                    All
                </div>
            </Link>
            <Link to={'/adminConsole'}>
                <div className={showingType == 1 ? "option-btn-focus" : "option-btn"} onClick={() => dispatch(updateAdminShowingType(1))}>
                <FontAwesomeIcon className="chooseImgIcon" icon={clockIconDefinition} />
                    Waiting
                </div>
            </Link>
            <Link to={'/adminConsole'}>
                <div className={showingType == 2 ? "option-btn-focus" : "option-btn"} onClick={() => dispatch(updateAdminShowingType(2))}>
                <FontAwesomeIcon className="chooseImgIcon" icon={spinnerIconDefinition} />
                    Proccessing
                </div>
            </Link>
            <Link to={'/adminConsole'}>
                <div className={showingType == 3 ? "option-btn-focus" : "option-btn"} onClick={() => dispatch(updateAdminShowingType(3))}>
                <FontAwesomeIcon className="chooseImgIcon" icon={checkIconDefinition} />
                    Done
                </div>
            </Link>
            <Link to={'/adminConsole'}>
                <div className={showingType == 4 ? "option-btn-focus" : "option-btn"} onClick={() => dispatch(updateAdminShowingType(4))}>
                <FontAwesomeIcon className="chooseImgIcon" icon={timesIconDefinition} />
                    Rejected
                </div>
            </Link>
        </div>
    )
}