import React, { useEffect } from 'react';
import logo from './logo.svg';
import { library } from '@fortawesome/fontawesome-svg-core'
import { far } from '@fortawesome/free-regular-svg-icons'
import './App.css';
import HomePage from './pages/HomePage';
import EmployerConsole from './pages/employerConsole';
import EmployerFormSubmitPage from './pages/employerFormSubmitPage'
import LoginRegisterPage from './pages/LoginRegisterPage'
import { Route, Link, Switch, Redirect } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from './redux/store';
import AdminPostPage from './pages/adminPostPage';
import AdminConsole from './pages/adminConsole';
import AdminAcceptPostPage from './pages/adminAcceptPostPage';
import AdminEditPostPage from './pages/adminEditPostPage';
import EmployerPostDetailPage from './pages/employerPostDetailPage';
import EmployerDonePostDetailPage from './pages/employerDonePostDetailPage';
import EmployerApplicantDetailsPage from './pages/employerApplicantDetailsPage';
import { userLoigin } from './redux/actions';
// export const server = 'http://localhost:8080'
export const server = 'https://server.wcatw.xyz'

library.add(far)



function App() {
  const isloggedin = useSelector((state: RootState) => state.userReducer.isLoggedIn)
  const isAdmin = useSelector((state: RootState) => state.userReducer.isAdmin)
  const dispatch = useDispatch()
  useEffect(()=>{
    const token = window.localStorage.getItem('token')
    const role = window.localStorage.getItem('role')
    const isAdmin = role=='admin'? true:false
    if( token ){
      
      dispatch(userLoigin(token? token:'',isAdmin))
    }
    document.title = "Jobable";  
  },[])

  return (

    <div>
      <Switch>

        <Route path="/" exact>
          {isAdmin ? <Redirect to="/adminConsole" /> : <HomePage />}
        </Route>
        <Route path="/register" exact>

          {isloggedin ? <Redirect to="/" /> : <LoginRegisterPage />}

        </Route>

        <Route path="/employerForm" exact><EmployerFormSubmitPage /></Route>
        <Route path="/employerConsole" exact>
        {isAdmin ? <Redirect to="/adminConsole" /> : <EmployerConsole />}
          
          </Route>
        <Route path="/adminConsole" exact><AdminConsole /></Route>
        <Route path="/adminPostPage" exact><AdminPostPage /></Route>
        <Route path="/adminAcceptPostPage" exact><AdminAcceptPostPage /></Route>
        <Route path="/adminEditPostPage" exact><AdminEditPostPage /></Route>
        <Route path="/employerPostDetailPage" exact><EmployerPostDetailPage /></Route>
        <Route path="/employerDonePostDetailPage" exact><EmployerDonePostDetailPage /></Route>
        <Route path="/employerApplicantDetailsPage" exact><EmployerApplicantDetailsPage /></Route>

      </Switch>
    </div>



  );
}

export default App;
